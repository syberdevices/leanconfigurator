﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LEANConfigurator
{
    public partial class TestCan : Form
    {
        public TestCan(Status s)
        {
            InitializeComponent();
            status = s;
        }

        Status status;

        public void UpdateFormData(Selftest st)
        {
            labelRpm.Text = st.can_rpm;
            labelTemp.Text = st.can_temp;
            labelFuelLevelLiter.Text = st.can_fuel_l;
            labelFuelLevelPercent.Text = st.can_fuel_p;
            labelOdo.Text = st.can_odo;
            labelIgnition.Text = st.can_ign;
            labelCheckOil.Text = st.can_check_oil;

            labelAlertRpm.Text = st.alert_rpm;
            labelAlertTemp.Text = st.alert_temp_eng;
            labelAlertCheckOil.Text = st.alert_check_oil;
        }
    }
}
