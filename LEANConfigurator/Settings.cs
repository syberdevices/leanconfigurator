﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEANConfigurator
{
    public class SubPeriod
    {
        public string in_active_mode { get; set; }
        public string in_standby_mode { get; set; }
    }

    public class Period
    {
        public SubPeriod data_send { get; set; }
        public SubPeriod data_write { get; set; }
        public SubPeriod fls_read { get; set; }
    }

    public class FlsSensor
    {
        public string address { get; set; }
    }

    public class DS18B20
    {
        public string id { get; set; }
    }

    public class GNSS
    {
        public string filter { get; set; }
        public string freeze_speed { get; set; }
        public string freeze_time { get; set; }
        public string hdop_max { get; set; }
        public string satellites_min { get; set; }
        public string max_speed { get; set; }
        public string speed_avg { get; set; }
    }

    public class APN
    {
        public string address { get; set; }
        public string user { get; set; }
        public string password { get; set; }
    }

    public class MainHost
    {
        public string address { get; set; }
        public string port { get; set; }
        public string wait_response { get; set; }
    }

    /*    class FtpHost
        {
            public string address { get; set; }
            public string login { get; set; }
            public string password { get; set; }
        }
    */
    public class LogHost
    {
        public string address { get; set; }
        public string port { get; set; }
    }

    public class DIN
    {
        public string type { get; set; }
        public string mode { get; set; }
    }

    public class DOUT
    {
        public string type { get; set; }
    }

    public class AIN
    {
        public string threshold { get; set; }
    }

    public class Bluetooth
    {
        public string used { get; set; }
        public string auto_answer { get; set; }
        public string mac { get; set; }
    }

    public class AlertThreshold
    {
        public string max { get; set; }
        public string duration { get; set; }
    }

    public class CanAlert
    {
        public AlertThreshold temp_trh { get; set; }
        public AlertThreshold rpm_trh { get; set; }
        public string on_check_oil { get; set; }
    }

    public class Can
    {
        public string used { get; set; }
        public string used_param { get; set; }
        public SubPeriod period { get; set; }
        public CanAlert alert { get; set; }
        public string gen_pack_on_ign { get; set; }
    }


    public class Settings
    {
        public string password { get; set; }
        public string id { get; set; }
        public string protocol { get; set; }
        public Period period { get; set; }
        public FlsSensor[] fls_sensor { get; set; }
        public DS18B20[] ds18b20 { get; set; }
        public GNSS gnss { get; set; }
        public string min_rssi { get; set; }
        public string untracked_course { get; set; }
        public string untracked_distance { get; set; }
        public string acceleration_threshold { get; set; }
        public APN apn { get; set; }
        public MainHost main_host { get; set; }
//        public FtpHost ftp_host { get; set; }
        public LogHost log_host { get; set; }
        public DIN[] dins { get; set; }
        public DOUT[] douts { get; set; }
        public AIN[] ains { get; set; }
        public string[] white_phone_numbers { get; set; }
        public string[] master_phone_numbers { get; set; }
        public Bluetooth bluetooth { get; set; }
        public string auto_id { get; set; }
        public string log_level { get; set; }
        public Can can { get; set; }
        public string auto_update { get; set; }
        public string settings_version { get; set; }
    }
}
