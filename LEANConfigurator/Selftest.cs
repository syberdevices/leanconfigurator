﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LEANConfigurator
{
    public class LLS
    {
        public string address { get; set; }
        public string level { get; set; }
        public string temp { get; set; }
    }

    public class DS18B20Sta
    {
        public string id { get; set; }
        public string temp { get; set; }
    }

    public class IBatton
    {
        public string id { get; set; }
    }

    public class Selftest
    {
        public string unixtime { get; set; }
        public string fw_version { get; set; }
        public string hw_version { get; set; }
        public string id { get; set; }
        public string gsm_phy { get; set; }
        public string gsm_sim { get; set; }
        public string gsm_gprs { get; set; }
        public string gsm_tcp_conn { get; set; }
        public string gsm_tcp_tx { get; set; }
        public string imei { get; set; }
        public string gsm_rssi { get; set; }
        public string gnss_phy { get; set; }
        public string gnss_fix { get; set; }
        public string gnss_hdop { get; set; }
        public string bat_voltage { get; set; }
        public string bat_charging { get; set; }
        public string power_voltage { get; set; }
        public string accel_phy { get; set; }
        public string accel_motion { get; set; }
        public string spi_flash { get; set; }
        public string din1 { get; set; }
        public string din2 { get; set; }
        public string din3 { get; set; }
        public string din4 { get; set; }
        public string count1 { get; set; }
        public string count2 { get; set; }
        public string count3 { get; set; }
        public string count4 { get; set; }
        public string dout1 { get; set; }
        public string dout2 { get; set; }
        public string ain1 { get; set; }
        public string ain2 { get; set; }
        public LLS[] lls { get; set; }
        public DS18B20Sta[] ds18b20 { get; set; }
        public IBatton[] ibutton { get; set; }
        public string ignition { get; set; }
        public string mode { get; set; }
        public string dev_case { get; set; }
        public string temp_mcu { get; set; }
        public string can_rpm { get; set; }
        public string can_fuel_l { get; set; }
        public string can_fuel_p { get; set; }
        public string can_odo { get; set; }
        public string can_temp { get; set; }
        public string can_ign { get; set; }
        public string can_check_oil { get; set; }
        public string alert_temp_eng { get; set; }
        public string alert_rpm { get; set; }
        public string alert_check_oil { get; set; }
    }
}
