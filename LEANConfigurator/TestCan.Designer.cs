﻿
namespace LEANConfigurator
{
    partial class TestCan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxTemp = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelRpm = new System.Windows.Forms.Label();
            this.labelTemp = new System.Windows.Forms.Label();
            this.labelFuelLevelLiter = new System.Windows.Forms.Label();
            this.labelFuelLevelPercent = new System.Windows.Forms.Label();
            this.labelOdo = new System.Windows.Forms.Label();
            this.labelIgnition = new System.Windows.Forms.Label();
            this.labelCheckOil = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelAlertRpm = new System.Windows.Forms.Label();
            this.labelAlertTemp = new System.Windows.Forms.Label();
            this.labelAlertCheckOil = new System.Windows.Forms.Label();
            this.groupBoxTemp.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTemp
            // 
            this.groupBoxTemp.Controls.Add(this.tableLayoutPanel10);
            this.groupBoxTemp.ForeColor = System.Drawing.Color.Black;
            this.groupBoxTemp.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTemp.Name = "groupBoxTemp";
            this.groupBoxTemp.Size = new System.Drawing.Size(274, 234);
            this.groupBoxTemp.TabIndex = 3;
            this.groupBoxTemp.TabStop = false;
            this.groupBoxTemp.Text = "Данные CAN";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.53191F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.46808F));
            this.tableLayoutPanel10.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label10, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.labelRpm, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.labelTemp, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.labelFuelLevelLiter, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.labelFuelLevelPercent, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.labelOdo, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.labelIgnition, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.labelCheckOil, 1, 6);
            this.tableLayoutPanel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 7;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.29061F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.29062F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.29062F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28204F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28204F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28204F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28204F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(262, 209);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Обороты двигателя";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(3, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Температура двигателя";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(3, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Уровень топлива (литры)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(3, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Уровень топлива (проценты)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(3, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Пробег";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(3, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(165, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Зажигание";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(3, 185);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(165, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Индикатор уровня масла";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelRpm
            // 
            this.labelRpm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRpm.AutoSize = true;
            this.labelRpm.BackColor = System.Drawing.SystemColors.Control;
            this.labelRpm.Location = new System.Drawing.Point(174, 8);
            this.labelRpm.Name = "labelRpm";
            this.labelRpm.Size = new System.Drawing.Size(85, 13);
            this.labelRpm.TabIndex = 0;
            this.labelRpm.Text = "-";
            this.labelRpm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTemp
            // 
            this.labelTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTemp.AutoSize = true;
            this.labelTemp.BackColor = System.Drawing.SystemColors.Control;
            this.labelTemp.Location = new System.Drawing.Point(174, 37);
            this.labelTemp.Name = "labelTemp";
            this.labelTemp.Size = new System.Drawing.Size(85, 13);
            this.labelTemp.TabIndex = 0;
            this.labelTemp.Text = "-";
            this.labelTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFuelLevelLiter
            // 
            this.labelFuelLevelLiter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFuelLevelLiter.AutoSize = true;
            this.labelFuelLevelLiter.BackColor = System.Drawing.SystemColors.Control;
            this.labelFuelLevelLiter.Location = new System.Drawing.Point(174, 66);
            this.labelFuelLevelLiter.Name = "labelFuelLevelLiter";
            this.labelFuelLevelLiter.Size = new System.Drawing.Size(85, 13);
            this.labelFuelLevelLiter.TabIndex = 0;
            this.labelFuelLevelLiter.Text = "-";
            this.labelFuelLevelLiter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelFuelLevelPercent
            // 
            this.labelFuelLevelPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFuelLevelPercent.AutoSize = true;
            this.labelFuelLevelPercent.BackColor = System.Drawing.SystemColors.Control;
            this.labelFuelLevelPercent.Location = new System.Drawing.Point(174, 95);
            this.labelFuelLevelPercent.Name = "labelFuelLevelPercent";
            this.labelFuelLevelPercent.Size = new System.Drawing.Size(85, 13);
            this.labelFuelLevelPercent.TabIndex = 0;
            this.labelFuelLevelPercent.Text = "-";
            this.labelFuelLevelPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelOdo
            // 
            this.labelOdo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOdo.AutoSize = true;
            this.labelOdo.BackColor = System.Drawing.SystemColors.Control;
            this.labelOdo.Location = new System.Drawing.Point(174, 124);
            this.labelOdo.Name = "labelOdo";
            this.labelOdo.Size = new System.Drawing.Size(85, 13);
            this.labelOdo.TabIndex = 0;
            this.labelOdo.Text = "-";
            this.labelOdo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelIgnition
            // 
            this.labelIgnition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIgnition.AutoSize = true;
            this.labelIgnition.BackColor = System.Drawing.SystemColors.Control;
            this.labelIgnition.Location = new System.Drawing.Point(174, 153);
            this.labelIgnition.Name = "labelIgnition";
            this.labelIgnition.Size = new System.Drawing.Size(85, 13);
            this.labelIgnition.TabIndex = 0;
            this.labelIgnition.Text = "-";
            this.labelIgnition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCheckOil
            // 
            this.labelCheckOil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCheckOil.AutoSize = true;
            this.labelCheckOil.BackColor = System.Drawing.SystemColors.Control;
            this.labelCheckOil.Location = new System.Drawing.Point(174, 185);
            this.labelCheckOil.Name = "labelCheckOil";
            this.labelCheckOil.Size = new System.Drawing.Size(85, 13);
            this.labelCheckOil.TabIndex = 0;
            this.labelCheckOil.Text = "-";
            this.labelCheckOil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(292, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(274, 234);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Алерты CAN";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.53191F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.46808F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelAlertRpm, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelAlertTemp, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelAlertCheckOil, 1, 2);
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(262, 90);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Превышение оборотов";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(3, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Превышение температуры";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(3, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Низкий уровень масла";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAlertRpm
            // 
            this.labelAlertRpm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAlertRpm.AutoSize = true;
            this.labelAlertRpm.BackColor = System.Drawing.SystemColors.Control;
            this.labelAlertRpm.Location = new System.Drawing.Point(174, 8);
            this.labelAlertRpm.Name = "labelAlertRpm";
            this.labelAlertRpm.Size = new System.Drawing.Size(85, 13);
            this.labelAlertRpm.TabIndex = 0;
            this.labelAlertRpm.Text = "-";
            this.labelAlertRpm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAlertTemp
            // 
            this.labelAlertTemp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAlertTemp.AutoSize = true;
            this.labelAlertTemp.BackColor = System.Drawing.SystemColors.Control;
            this.labelAlertTemp.Location = new System.Drawing.Point(174, 37);
            this.labelAlertTemp.Name = "labelAlertTemp";
            this.labelAlertTemp.Size = new System.Drawing.Size(85, 13);
            this.labelAlertTemp.TabIndex = 0;
            this.labelAlertTemp.Text = "-";
            this.labelAlertTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAlertCheckOil
            // 
            this.labelAlertCheckOil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAlertCheckOil.AutoSize = true;
            this.labelAlertCheckOil.BackColor = System.Drawing.SystemColors.Control;
            this.labelAlertCheckOil.Location = new System.Drawing.Point(174, 68);
            this.labelAlertCheckOil.Name = "labelAlertCheckOil";
            this.labelAlertCheckOil.Size = new System.Drawing.Size(85, 13);
            this.labelAlertCheckOil.TabIndex = 0;
            this.labelAlertCheckOil.Text = "-";
            this.labelAlertCheckOil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TestCan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 254);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxTemp);
            this.Name = "TestCan";
            this.Text = "Тест CAN";
            this.groupBoxTemp.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTemp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelRpm;
        private System.Windows.Forms.Label labelTemp;
        private System.Windows.Forms.Label labelFuelLevelLiter;
        private System.Windows.Forms.Label labelFuelLevelPercent;
        private System.Windows.Forms.Label labelOdo;
        private System.Windows.Forms.Label labelIgnition;
        private System.Windows.Forms.Label labelCheckOil;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelAlertRpm;
        private System.Windows.Forms.Label labelAlertTemp;
        private System.Windows.Forms.Label labelAlertCheckOil;
    }
}