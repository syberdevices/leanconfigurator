﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace LEANConfigurator
{
    public partial class Status : Form
    {
        public Status()
        {
            InitializeComponent();
        }

        public Status(Configurator f)
        {
            InitializeComponent();
            configurator = f;
            configurator.Enabled = false;
        }

        private Configurator configurator;
        private string imei;
        private DateTime dateTime;
        private TestCan testcan;

        private void OnStatusFromClosed(object sender, FormClosedEventArgs e)
        {
            if (testcan != null && testcan.IsDisposed == false)
            {
                testcan.Close();
            }

            configurator.Enabled = true;
        }

        private void Status_Load(object sender, EventArgs e)
        {
            if (configurator.settings.can.used == "true" && configurator.settings.can.used_param != "0")
            {
                buttonTestCan.Enabled = true;
            }
            else
            {
                buttonTestCan.Enabled = false;
            }
        }

        private void UpdateGroupBoxGsm()
        {
            if (labelGsmPhyStatus.BackColor != Color.LimeGreen ||
                labelGsmSimStatus.BackColor != Color.LimeGreen ||
                labelGsmGprsStatus.BackColor != Color.LimeGreen ||
                labelGsmServerConStatus.BackColor != Color.LimeGreen ||
                labelGsmServerTxStatus.BackColor != Color.LimeGreen ||
                labelGsmRssiStatus.BackColor != Color.LimeGreen)
            {
                groupBoxGsm.ForeColor = Color.Red;
            }
            else
            {
                groupBoxGsm.ForeColor = Color.LimeGreen;
            }
        }

        private void UpdateGroupBoxGnss()
        {
            if (labelGnssPhyStatus.BackColor != Color.LimeGreen ||
                labelGnssFixStatus.BackColor != Color.LimeGreen ||
                labelGnssHdopStatus.BackColor != Color.LimeGreen)
            {
                groupBoxGnss.ForeColor = Color.Red;
            }
            else
            {
                groupBoxGnss.ForeColor = Color.LimeGreen;
            }
        }

        private void UpdateGroupBoxPeriph()
        {
            if (labelAccelStatus.BackColor != Color.LimeGreen ||
                labelMotionStatus.BackColor != Color.LimeGreen ||
                labelSpiFlashStatus.BackColor != Color.LimeGreen ||
                labelCaseStatus.BackColor != Color.LimeGreen ||
                labelMcuTempStatus.BackColor != Color.LimeGreen)
            {
                groupBoxPeriph.ForeColor = Color.Red;
            }
            else
            {
                groupBoxPeriph.ForeColor = Color.LimeGreen;
            }
        }

        private void UpdateGroupBoxPower()
        {
            if (labelBatteryVoltage.BackColor != Color.LimeGreen ||
                labelBatteryCharging.BackColor != Color.LimeGreen ||
                labelPowerVoltage.BackColor != Color.LimeGreen)
            {
                groupBoxPower.ForeColor = Color.Red;
            }
            else
            {
                groupBoxPower.ForeColor = Color.LimeGreen;
            }
        }

        private void UpdateGroupBoxLls()
        {
            if (labelLls1Addr.Text.Length > 0 ||
                labelLls2Addr.Text.Length > 0 ||
                labelLls3Addr.Text.Length > 0 ||
                labelLls4Addr.Text.Length > 0 ||
                labelLls5Addr.Text.Length > 0 ||
                labelLls6Addr.Text.Length > 0 ||
                labelLls7Addr.Text.Length > 0 ||
                labelLls8Addr.Text.Length > 0)
            {
                groupBoxLls.ForeColor = Color.LimeGreen;
            }
            else
            {
                groupBoxLls.ForeColor = Color.Red;
            }
        }

        private void UpdateGroupBoxiButton()
        {
            if (labelIbutton1Id.Text.Length > 0 ||
                labelIbutton3Id.Text.Length > 0 ||
                labelIbutton3Id.Text.Length > 0)
            {
                groupBoxiButton.ForeColor = Color.LimeGreen;
            }
            else
            {
                groupBoxiButton.ForeColor = Color.Red;
            }
        }

        private void UpdateGroupBoxTemp()
        {
            if (labelDs1Id.Text.Length > 0 ||
                labelDs2Id.Text.Length > 0 ||
                labelDs3Id.Text.Length > 0 ||
                labelDs4Id.Text.Length > 0 ||
                labelDs5Id.Text.Length > 0 ||
                labelDs6Id.Text.Length > 0 ||
                labelDs7Id.Text.Length > 0 ||
                labelDs8Id.Text.Length > 0)
            {
                groupBoxTemp.ForeColor = Color.LimeGreen;
            }
            else
            {
                groupBoxTemp.ForeColor = Color.Red;
            }
        }

        private void UpdateStatus(Selftest st)
        {
            imei = st.imei;
            dateTime = DateTimeOffset.FromUnixTimeSeconds(Convert.ToUInt32(st.unixtime)).DateTime.ToLocalTime();

            labelFWVersion.Text = "Версия ПО: " + st.fw_version;
            labelHWVersion.Text = "Версия оборудования: " + st.hw_version;
            labelImei.Text = "IMEI: " + st.imei;
            labelId.Text = "Идентификатор: " + st.id;
            labelMode.Text = "Режим: " + ((st.mode == "standby") ? "В ожидании" : ((st.mode == "active") ? "Активный" : "???"));
            labelUnitTime.Text = "Время прибора: " + dateTime.ToString();
            labelGsmPhyStatus.Text = st.gsm_phy;
            labelGsmSimStatus.Text = st.gsm_sim;
            labelGsmGprsStatus.Text = st.gsm_gprs;
            labelGsmServerConStatus.Text = st.gsm_tcp_conn;
            labelGsmServerTxStatus.Text = st.gsm_tcp_tx;
            labelGsmRssiStatus.Text = st.gsm_rssi;
            labelGnssPhyStatus.Text = st.gnss_phy;
            labelGnssFixStatus.Text = st.gnss_fix;
            labelGnssHdopStatus.Text = st.gnss_hdop;
            labelBatteryVoltage.Text = st.bat_voltage;
            labelBatteryCharging.Text = st.bat_charging;
            labelPowerVoltage.Text = st.power_voltage;
            labelAccelStatus.Text = st.accel_phy;
            labelMotionStatus.Text = st.accel_motion;
            labelSpiFlashStatus.Text = st.spi_flash;
            labelDin1Status.Text = st.din1;
            labelDin2Status.Text = st.din2;
            labelDin3Status.Text = st.din3;
            labelDin4Status.Text = st.din4;
            labelCount1Status.Text = st.count1;
            labelCount2Status.Text = st.count2;
            labelCount3Status.Text = st.count3;
            labelCount4Status.Text = st.count4;
            labelDout1Status.Text = st.dout1;
            labelDout2Status.Text = st.dout2;
            labelAin1Status.Text = st.ain1;
            labelAin2Status.Text = st.ain2;
            labelLls1Addr.Text = st.lls[0].address;
            labelLls1Fuel.Text = st.lls[0].level;
            labelLls1Temp.Text = st.lls[0].temp;
            labelLls2Addr.Text = st.lls[1].address;
            labelLls2Fuel.Text = st.lls[1].level;
            labelLls2Temp.Text = st.lls[1].temp;
            labelLls3Addr.Text = st.lls[2].address;
            labelLls3Fuel.Text = st.lls[2].level;
            labelLls3Temp.Text = st.lls[2].temp;
            labelLls4Addr.Text = st.lls[3].address;
            labelLls4Fuel.Text = st.lls[3].level;
            labelLls4Temp.Text = st.lls[3].temp;
            labelLls5Addr.Text = st.lls[4].address;
            labelLls5Fuel.Text = st.lls[4].level;
            labelLls5Temp.Text = st.lls[4].temp;
            labelLls6Addr.Text = st.lls[5].address;
            labelLls6Fuel.Text = st.lls[5].level;
            labelLls6Temp.Text = st.lls[5].temp;
            labelLls7Addr.Text = st.lls[6].address;
            labelLls7Fuel.Text = st.lls[6].level;
            labelLls7Temp.Text = st.lls[6].temp;
            labelLls8Addr.Text = st.lls[7].address;
            labelLls8Fuel.Text = st.lls[7].level;
            labelLls8Temp.Text = st.lls[7].temp;
            labelDs1Id.Text = st.ds18b20[0].id;
            labelDs1Temp.Text = st.ds18b20[0].temp;
            labelDs2Id.Text = st.ds18b20[1].id;
            labelDs2Temp.Text = st.ds18b20[1].temp;
            labelDs3Id.Text = st.ds18b20[2].id;
            labelDs3Temp.Text = st.ds18b20[2].temp;
            labelDs4Id.Text = st.ds18b20[3].id;
            labelDs4Temp.Text = st.ds18b20[3].temp;
            labelDs5Id.Text = st.ds18b20[4].id;
            labelDs5Temp.Text = st.ds18b20[4].temp;
            labelDs6Id.Text = st.ds18b20[5].id;
            labelDs6Temp.Text = st.ds18b20[5].temp;
            labelDs7Id.Text = st.ds18b20[6].id;
            labelDs7Temp.Text = st.ds18b20[6].temp;
            labelDs8Id.Text = st.ds18b20[7].id;
            labelDs8Temp.Text = st.ds18b20[7].temp;
            labelIbutton1Id.Text = st.ibutton[0].id;
            labelIbutton2Id.Text = st.ibutton[1].id;
            labelIbutton3Id.Text = st.ibutton[2].id;
            //labelIgnition.Text = st.ignition;
            labelCaseStatus.Text = st.dev_case;
            labelMcuTempStatus.Text = st.temp_mcu;

            UpdateGroupBoxGsm();
            UpdateGroupBoxGnss();
            UpdateGroupBoxPeriph();
            UpdateGroupBoxPower();
            UpdateGroupBoxLls();
            UpdateGroupBoxiButton();
            UpdateGroupBoxTemp();

            if (testcan != null && testcan.IsDisposed == false)
            {
                testcan.UpdateFormData(st);
            }
        }

        private void timerStatusRequestCallback(object sender, EventArgs e)
        {
            if (configurator.serialPort.IsOpen)
            {
                configurator.serialPort.Write("b\r");

                string selftestJson = "";

                try
                {
                    configurator.serialPort.ReadTo("-------======== Board Info: ========--------\r\n");

                    while (true)
                    {
                        int _byte = configurator.serialPort.ReadByte();
                        if (_byte == -1)
                        {
                            return;
                        }

                        char inchar = Convert.ToChar(_byte);
                        if (inchar == '\r')
                        {
                            break;
                        }

                        selftestJson += inchar;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }

                try
                {
                    Selftest selftest = JsonConvert.DeserializeObject<Selftest>(selftestJson);
                    UpdateStatus(selftest);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void LabelGsmPhyStatus_TextChanged(object sender, EventArgs e)
        {
            labelGsmPhyStatus.BackColor = labelGsmPhyStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGsmSimStatus_TextChanged(object sender, EventArgs e)
        {
            labelGsmSimStatus.BackColor = labelGsmSimStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGsmGprsStatus_TextChanged(object sender, EventArgs e)
        {
            labelGsmGprsStatus.BackColor = labelGsmGprsStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGsmServerConStatus_TextChanged(object sender, EventArgs e)
        {
            labelGsmServerConStatus.BackColor = labelGsmServerConStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGsmServerTxStatus_TextChanged(object sender, EventArgs e)
        {
            labelGsmServerTxStatus.BackColor = labelGsmServerTxStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGsmRssiStatus_TextChanged(object sender, EventArgs e)
        {
            char[] separator = { ' ', '%', '(', ')' };
            string[] rssi = labelGsmRssiStatus.Text.Split(separator);
            labelGsmRssiStatus.BackColor = int.Parse(rssi[0]) > 40 ? Color.LimeGreen : int.Parse(rssi[0]) > 15 ? Color.Orange : Color.Red;
        }

        private void LabelGnssPhyStatus_TextChanged(object sender, EventArgs e)
        {
            labelGnssPhyStatus.BackColor = labelGnssPhyStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelGnssFixStatus_TextChanged(object sender, EventArgs e)
        {
            if (labelGnssFixStatus.Text == "3D")
            {
                labelGnssFixStatus.BackColor = Color.LimeGreen;
            }
            else if (labelGnssFixStatus.Text == "2D")
            {
                labelGnssFixStatus.BackColor = Color.Orange;
            }
            else
            {
                labelGnssFixStatus.Text = "-";
                labelGnssFixStatus.BackColor = Color.Red;
            }
        }

        private void LabelGnssHdopStatus_TextChanged(object sender, EventArgs e)
        {
            double hdop = double.Parse(labelGnssHdopStatus.Text);
            labelGnssHdopStatus.BackColor = (hdop < 1 && hdop != 0) ? Color.LimeGreen : (hdop < 2 && hdop != 0) ? Color.Orange : Color.Red;
        }

        private void LabelAccelStatus_TextChanged(object sender, EventArgs e)
        {
            labelAccelStatus.BackColor = labelAccelStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelMotionStatus_TextChanged(object sender, EventArgs e)
        {
            labelMotionStatus.BackColor = labelMotionStatus.Text == "Yes" ? Color.LimeGreen : Color.Red;
        }

        private void LabelSpiFlashStatus_TextChanged(object sender, EventArgs e)
        {
            labelSpiFlashStatus.BackColor = labelSpiFlashStatus.Text == "Ok" ? Color.LimeGreen : Color.Red;
        }

        private void LabelCaseStatus_TextChanged(object sender, EventArgs e)
        {
            labelCaseStatus.BackColor = labelCaseStatus.Text == "closed" ? Color.LimeGreen : Color.Red;
        }

        private void LabelMcuTempStatus_TextChanged(object sender, EventArgs e)
        {
            int temp = int.Parse(labelMcuTempStatus.Text);
            labelMcuTempStatus.BackColor = (temp > -20 && temp < 50) ? Color.LimeGreen : (temp > -40 && temp < 80) ? Color.Orange : Color.Red;
        }

        private void LabelPowerVoltage_TextChanged(object sender, EventArgs e)
        {
            double voltage = double.Parse(labelPowerVoltage.Text);
            labelPowerVoltage.BackColor = (voltage > 10 && voltage < 28) ? Color.LimeGreen : (voltage > 7 && voltage < 40) ? Color.Orange : Color.Red;
        }

        private void LabelBatteryVoltage_TextChanged(object sender, EventArgs e)
        {
            double voltage = double.Parse(labelBatteryVoltage.Text);
            labelBatteryVoltage.BackColor = (voltage > 3.6 && voltage < 4.28) ? Color.LimeGreen : (voltage > 3.3 && voltage < 4.32) ? Color.Orange : Color.Red;
        }

        private void LabelBatteryCharging_TextChanged(object sender, EventArgs e)
        {
            labelBatteryCharging.BackColor = labelBatteryCharging.Text == "Yes" ? Color.LimeGreen : Color.Red;
        }

        private void ButtonPrintScreenStatus_Click(object sender, EventArgs e)
        {
            var frm = Form.ActiveForm;
            using (var bmp = new Bitmap(frm.Width, frm.Height))
            {
                frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));

                try
                {
                    if (!Directory.Exists("screenshots"))
                    {
                        Directory.CreateDirectory("screenshots");
                    }
                    bmp.Save(@"screenshots/status-" + imei + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png");

                    MessageBox.Show("Скриншот сохранён", "LEANConfigurator");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
                
            }
        }

        private void LabelImei_Click(object sender, EventArgs e)
        {
            char[] CutChar = { 'I', 'M', 'E', 'I', ':', ' ' };
            string str = labelImei.Text;
            string imei = str.TrimStart(CutChar);

            if (imei.Length > 0)
            {
                Clipboard.Clear();
                Clipboard.SetText(imei);

                MessageBox.Show("IMEI: " + imei, "IMEI скопирован");
            }
        }

        private void ButtonTestCan_Click(object sender, EventArgs e)
        {
            if (testcan == null || testcan.IsDisposed)
            {
                testcan = new TestCan(this);
                testcan.Show();
            }
            else
            {
                testcan.Activate();
            }
        }
    }
}
