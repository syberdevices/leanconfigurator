﻿namespace LEANConfigurator
{
    partial class Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerStatusRequest = new System.Windows.Forms.Timer(this.components);
            this.labelFWVersion = new System.Windows.Forms.Label();
            this.labelHWVersion = new System.Windows.Forms.Label();
            this.labelImei = new System.Windows.Forms.Label();
            this.labelId = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelMode = new System.Windows.Forms.Label();
            this.groupBoxGsm = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelGsmPhyStatus = new System.Windows.Forms.Label();
            this.labelGsmSimStatus = new System.Windows.Forms.Label();
            this.labelGsmGprsStatus = new System.Windows.Forms.Label();
            this.labelGsmServerConStatus = new System.Windows.Forms.Label();
            this.labelGsmServerTxStatus = new System.Windows.Forms.Label();
            this.labelGsmRssiStatus = new System.Windows.Forms.Label();
            this.groupBoxGnss = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelGnssPhyStatus = new System.Windows.Forms.Label();
            this.labelGnssFixStatus = new System.Windows.Forms.Label();
            this.labelGnssHdopStatus = new System.Windows.Forms.Label();
            this.groupBoxPeriph = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelAccelStatus = new System.Windows.Forms.Label();
            this.labelMotionStatus = new System.Windows.Forms.Label();
            this.labelSpiFlashStatus = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labelCaseStatus = new System.Windows.Forms.Label();
            this.labelMcuTempStatus = new System.Windows.Forms.Label();
            this.groupBoxPower = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelPowerVoltage = new System.Windows.Forms.Label();
            this.labelBatteryVoltage = new System.Windows.Forms.Label();
            this.labelBatteryCharging = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBoxDin = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelDin1Status = new System.Windows.Forms.Label();
            this.labelDin2Status = new System.Windows.Forms.Label();
            this.labelDin3Status = new System.Windows.Forms.Label();
            this.labelDin4Status = new System.Windows.Forms.Label();
            this.groupBoxDout = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.labelDout1Status = new System.Windows.Forms.Label();
            this.labelDout2Status = new System.Windows.Forms.Label();
            this.groupBoxAin = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labelAin1Status = new System.Windows.Forms.Label();
            this.labelAin2Status = new System.Windows.Forms.Label();
            this.groupBoxCount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.labelCount1Status = new System.Windows.Forms.Label();
            this.labelCount2Status = new System.Windows.Forms.Label();
            this.labelCount3Status = new System.Windows.Forms.Label();
            this.labelCount4Status = new System.Windows.Forms.Label();
            this.groupBoxLls = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.labelLls1Addr = new System.Windows.Forms.Label();
            this.labelLls2Addr = new System.Windows.Forms.Label();
            this.labelLls3Addr = new System.Windows.Forms.Label();
            this.labelLls4Addr = new System.Windows.Forms.Label();
            this.labelLls5Addr = new System.Windows.Forms.Label();
            this.labelLls6Addr = new System.Windows.Forms.Label();
            this.labelLls7Addr = new System.Windows.Forms.Label();
            this.labelLls8Addr = new System.Windows.Forms.Label();
            this.labelLls1Fuel = new System.Windows.Forms.Label();
            this.labelLls2Fuel = new System.Windows.Forms.Label();
            this.labelLls3Fuel = new System.Windows.Forms.Label();
            this.labelLls4Fuel = new System.Windows.Forms.Label();
            this.labelLls5Fuel = new System.Windows.Forms.Label();
            this.labelLls6Fuel = new System.Windows.Forms.Label();
            this.labelLls7Fuel = new System.Windows.Forms.Label();
            this.labelLls8Fuel = new System.Windows.Forms.Label();
            this.labelLls1Temp = new System.Windows.Forms.Label();
            this.labelLls2Temp = new System.Windows.Forms.Label();
            this.labelLls3Temp = new System.Windows.Forms.Label();
            this.labelLls4Temp = new System.Windows.Forms.Label();
            this.labelLls5Temp = new System.Windows.Forms.Label();
            this.labelLls6Temp = new System.Windows.Forms.Label();
            this.labelLls7Temp = new System.Windows.Forms.Label();
            this.labelLls8Temp = new System.Windows.Forms.Label();
            this.groupBoxTemp = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.labelDs1Id = new System.Windows.Forms.Label();
            this.labelDs2Id = new System.Windows.Forms.Label();
            this.labelDs3Id = new System.Windows.Forms.Label();
            this.labelDs4Id = new System.Windows.Forms.Label();
            this.labelDs5Id = new System.Windows.Forms.Label();
            this.labelDs6Id = new System.Windows.Forms.Label();
            this.labelDs7Id = new System.Windows.Forms.Label();
            this.labelDs8Id = new System.Windows.Forms.Label();
            this.labelDs1Temp = new System.Windows.Forms.Label();
            this.labelDs2Temp = new System.Windows.Forms.Label();
            this.labelDs3Temp = new System.Windows.Forms.Label();
            this.labelDs4Temp = new System.Windows.Forms.Label();
            this.labelDs5Temp = new System.Windows.Forms.Label();
            this.labelDs6Temp = new System.Windows.Forms.Label();
            this.labelDs7Temp = new System.Windows.Forms.Label();
            this.labelDs8Temp = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.groupBoxiButton = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label49 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.labelIbutton1Id = new System.Windows.Forms.Label();
            this.labelIbutton2Id = new System.Windows.Forms.Label();
            this.labelIbutton3Id = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.buttonPrintScreenStatus = new System.Windows.Forms.Button();
            this.labelUnitTime = new System.Windows.Forms.Label();
            this.buttonTestCan = new System.Windows.Forms.Button();
            this.groupBoxGsm.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxGnss.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBoxPeriph.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBoxPower.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBoxDin.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBoxDout.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBoxAin.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBoxCount.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.groupBoxLls.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.groupBoxTemp.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBoxiButton.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerStatusRequest
            // 
            this.timerStatusRequest.Enabled = true;
            this.timerStatusRequest.Interval = 1100;
            this.timerStatusRequest.Tick += new System.EventHandler(this.timerStatusRequestCallback);
            // 
            // labelFWVersion
            // 
            this.labelFWVersion.AutoSize = true;
            this.labelFWVersion.Location = new System.Drawing.Point(13, 13);
            this.labelFWVersion.Name = "labelFWVersion";
            this.labelFWVersion.Size = new System.Drawing.Size(69, 13);
            this.labelFWVersion.TabIndex = 0;
            this.labelFWVersion.Text = "Версия ПО: ";
            // 
            // labelHWVersion
            // 
            this.labelHWVersion.AutoSize = true;
            this.labelHWVersion.Location = new System.Drawing.Point(128, 13);
            this.labelHWVersion.Name = "labelHWVersion";
            this.labelHWVersion.Size = new System.Drawing.Size(124, 13);
            this.labelHWVersion.TabIndex = 0;
            this.labelHWVersion.Text = "Версия оборудования: ";
            // 
            // labelImei
            // 
            this.labelImei.AutoSize = true;
            this.labelImei.Location = new System.Drawing.Point(339, 13);
            this.labelImei.Name = "labelImei";
            this.labelImei.Size = new System.Drawing.Size(32, 13);
            this.labelImei.TabIndex = 0;
            this.labelImei.Text = "IMEI:";
            this.labelImei.Click += new System.EventHandler(this.LabelImei_Click);
            // 
            // labelId
            // 
            this.labelId.AutoSize = true;
            this.labelId.Location = new System.Drawing.Point(521, 13);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(90, 13);
            this.labelId.TabIndex = 0;
            this.labelId.Text = "Идентификатор:";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(16, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(873, 10);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // labelMode
            // 
            this.labelMode.AutoSize = true;
            this.labelMode.Location = new System.Drawing.Point(684, 13);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(45, 13);
            this.labelMode.TabIndex = 0;
            this.labelMode.Text = "Режим:";
            // 
            // groupBoxGsm
            // 
            this.groupBoxGsm.Controls.Add(this.tableLayoutPanel1);
            this.groupBoxGsm.ForeColor = System.Drawing.Color.Red;
            this.groupBoxGsm.Location = new System.Drawing.Point(12, 64);
            this.groupBoxGsm.Name = "groupBoxGsm";
            this.groupBoxGsm.Size = new System.Drawing.Size(184, 174);
            this.groupBoxGsm.TabIndex = 2;
            this.groupBoxGsm.TabStop = false;
            this.groupBoxGsm.Text = "Состояние GSM";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.94253F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.05747F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmPhyStatus, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmSimStatus, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmGprsStatus, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmServerConStatus, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmServerTxStatus, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmRssiStatus, 1, 5);
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(174, 146);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PHY:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Состояние SIM:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Сессия GPRS:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Коннект к серверу:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Передача данных:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Качество сигнала:";
            // 
            // labelGsmPhyStatus
            // 
            this.labelGsmPhyStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmPhyStatus.AutoSize = true;
            this.labelGsmPhyStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmPhyStatus.Location = new System.Drawing.Point(116, 5);
            this.labelGsmPhyStatus.Name = "labelGsmPhyStatus";
            this.labelGsmPhyStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmPhyStatus.TabIndex = 0;
            this.labelGsmPhyStatus.Text = "Err";
            this.labelGsmPhyStatus.TextChanged += new System.EventHandler(this.LabelGsmPhyStatus_TextChanged);
            // 
            // labelGsmSimStatus
            // 
            this.labelGsmSimStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmSimStatus.AutoSize = true;
            this.labelGsmSimStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmSimStatus.Location = new System.Drawing.Point(116, 29);
            this.labelGsmSimStatus.Name = "labelGsmSimStatus";
            this.labelGsmSimStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmSimStatus.TabIndex = 0;
            this.labelGsmSimStatus.Text = "Err";
            this.labelGsmSimStatus.TextChanged += new System.EventHandler(this.LabelGsmSimStatus_TextChanged);
            // 
            // labelGsmGprsStatus
            // 
            this.labelGsmGprsStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmGprsStatus.AutoSize = true;
            this.labelGsmGprsStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmGprsStatus.Location = new System.Drawing.Point(116, 53);
            this.labelGsmGprsStatus.Name = "labelGsmGprsStatus";
            this.labelGsmGprsStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmGprsStatus.TabIndex = 0;
            this.labelGsmGprsStatus.Text = "Err";
            this.labelGsmGprsStatus.TextChanged += new System.EventHandler(this.LabelGsmGprsStatus_TextChanged);
            // 
            // labelGsmServerConStatus
            // 
            this.labelGsmServerConStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmServerConStatus.AutoSize = true;
            this.labelGsmServerConStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmServerConStatus.Location = new System.Drawing.Point(116, 77);
            this.labelGsmServerConStatus.Name = "labelGsmServerConStatus";
            this.labelGsmServerConStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmServerConStatus.TabIndex = 0;
            this.labelGsmServerConStatus.Text = "Err";
            this.labelGsmServerConStatus.TextChanged += new System.EventHandler(this.LabelGsmServerConStatus_TextChanged);
            // 
            // labelGsmServerTxStatus
            // 
            this.labelGsmServerTxStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmServerTxStatus.AutoSize = true;
            this.labelGsmServerTxStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmServerTxStatus.Location = new System.Drawing.Point(116, 101);
            this.labelGsmServerTxStatus.Name = "labelGsmServerTxStatus";
            this.labelGsmServerTxStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmServerTxStatus.TabIndex = 0;
            this.labelGsmServerTxStatus.Text = "Err";
            this.labelGsmServerTxStatus.TextChanged += new System.EventHandler(this.LabelGsmServerTxStatus_TextChanged);
            // 
            // labelGsmRssiStatus
            // 
            this.labelGsmRssiStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGsmRssiStatus.AutoSize = true;
            this.labelGsmRssiStatus.BackColor = System.Drawing.Color.Red;
            this.labelGsmRssiStatus.Location = new System.Drawing.Point(116, 126);
            this.labelGsmRssiStatus.MaximumSize = new System.Drawing.Size(60, 0);
            this.labelGsmRssiStatus.Name = "labelGsmRssiStatus";
            this.labelGsmRssiStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGsmRssiStatus.TabIndex = 0;
            this.labelGsmRssiStatus.Text = "Err";
            this.labelGsmRssiStatus.TextChanged += new System.EventHandler(this.LabelGsmRssiStatus_TextChanged);
            // 
            // groupBoxGnss
            // 
            this.groupBoxGnss.Controls.Add(this.tableLayoutPanel2);
            this.groupBoxGnss.ForeColor = System.Drawing.Color.Red;
            this.groupBoxGnss.Location = new System.Drawing.Point(376, 64);
            this.groupBoxGnss.Name = "groupBoxGnss";
            this.groupBoxGnss.Size = new System.Drawing.Size(118, 174);
            this.groupBoxGnss.TabIndex = 2;
            this.groupBoxGnss.TabStop = false;
            this.groupBoxGnss.Text = "Состояние GNSS";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.94505F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.05495F));
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelGnssPhyStatus, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelGnssFixStatus, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelGnssHdopStatus, 1, 2);
            this.tableLayoutPanel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(91, 74);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "PHY:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Fix:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "HDOP:";
            // 
            // labelGnssPhyStatus
            // 
            this.labelGnssPhyStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGnssPhyStatus.AutoSize = true;
            this.labelGnssPhyStatus.BackColor = System.Drawing.Color.Red;
            this.labelGnssPhyStatus.Location = new System.Drawing.Point(52, 5);
            this.labelGnssPhyStatus.Name = "labelGnssPhyStatus";
            this.labelGnssPhyStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGnssPhyStatus.TabIndex = 0;
            this.labelGnssPhyStatus.Text = "Err";
            this.labelGnssPhyStatus.TextChanged += new System.EventHandler(this.LabelGnssPhyStatus_TextChanged);
            // 
            // labelGnssFixStatus
            // 
            this.labelGnssFixStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGnssFixStatus.AutoSize = true;
            this.labelGnssFixStatus.BackColor = System.Drawing.Color.Red;
            this.labelGnssFixStatus.Location = new System.Drawing.Point(52, 29);
            this.labelGnssFixStatus.Name = "labelGnssFixStatus";
            this.labelGnssFixStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGnssFixStatus.TabIndex = 0;
            this.labelGnssFixStatus.Text = "Err";
            this.labelGnssFixStatus.TextChanged += new System.EventHandler(this.LabelGnssFixStatus_TextChanged);
            // 
            // labelGnssHdopStatus
            // 
            this.labelGnssHdopStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelGnssHdopStatus.AutoSize = true;
            this.labelGnssHdopStatus.BackColor = System.Drawing.Color.Red;
            this.labelGnssHdopStatus.Location = new System.Drawing.Point(52, 54);
            this.labelGnssHdopStatus.Name = "labelGnssHdopStatus";
            this.labelGnssHdopStatus.Size = new System.Drawing.Size(20, 13);
            this.labelGnssHdopStatus.TabIndex = 0;
            this.labelGnssHdopStatus.Text = "Err";
            this.labelGnssHdopStatus.TextChanged += new System.EventHandler(this.LabelGnssHdopStatus_TextChanged);
            // 
            // groupBoxPeriph
            // 
            this.groupBoxPeriph.Controls.Add(this.tableLayoutPanel3);
            this.groupBoxPeriph.ForeColor = System.Drawing.Color.Red;
            this.groupBoxPeriph.Location = new System.Drawing.Point(202, 64);
            this.groupBoxPeriph.Name = "groupBoxPeriph";
            this.groupBoxPeriph.Size = new System.Drawing.Size(168, 174);
            this.groupBoxPeriph.TabIndex = 2;
            this.groupBoxPeriph.TabStop = false;
            this.groupBoxPeriph.Text = "Состояние Периферии";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.79487F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.20513F));
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelAccelStatus, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelMotionStatus, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelSpiFlashStatus, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label17, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.labelCaseStatus, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.labelMcuTempStatus, 1, 4);
            this.tableLayoutPanel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9988F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9988F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(156, 124);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Акселерометр:";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Flash память:";
            // 
            // labelAccelStatus
            // 
            this.labelAccelStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelAccelStatus.AutoSize = true;
            this.labelAccelStatus.BackColor = System.Drawing.Color.Red;
            this.labelAccelStatus.Location = new System.Drawing.Point(114, 5);
            this.labelAccelStatus.Name = "labelAccelStatus";
            this.labelAccelStatus.Size = new System.Drawing.Size(20, 13);
            this.labelAccelStatus.TabIndex = 0;
            this.labelAccelStatus.Text = "Err";
            this.labelAccelStatus.TextChanged += new System.EventHandler(this.LabelAccelStatus_TextChanged);
            // 
            // labelMotionStatus
            // 
            this.labelMotionStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelMotionStatus.AutoSize = true;
            this.labelMotionStatus.BackColor = System.Drawing.Color.Red;
            this.labelMotionStatus.Location = new System.Drawing.Point(114, 29);
            this.labelMotionStatus.Name = "labelMotionStatus";
            this.labelMotionStatus.Size = new System.Drawing.Size(20, 13);
            this.labelMotionStatus.TabIndex = 0;
            this.labelMotionStatus.Text = "Err";
            this.labelMotionStatus.TextChanged += new System.EventHandler(this.LabelMotionStatus_TextChanged);
            // 
            // labelSpiFlashStatus
            // 
            this.labelSpiFlashStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelSpiFlashStatus.AutoSize = true;
            this.labelSpiFlashStatus.BackColor = System.Drawing.Color.Red;
            this.labelSpiFlashStatus.Location = new System.Drawing.Point(114, 53);
            this.labelSpiFlashStatus.Name = "labelSpiFlashStatus";
            this.labelSpiFlashStatus.Size = new System.Drawing.Size(20, 13);
            this.labelSpiFlashStatus.TabIndex = 0;
            this.labelSpiFlashStatus.Text = "Err";
            this.labelSpiFlashStatus.TextChanged += new System.EventHandler(this.LabelSpiFlashStatus_TextChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Движение:";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Корпус:";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 103);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Температура MCU:";
            // 
            // labelCaseStatus
            // 
            this.labelCaseStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCaseStatus.AutoSize = true;
            this.labelCaseStatus.BackColor = System.Drawing.Color.Red;
            this.labelCaseStatus.Location = new System.Drawing.Point(114, 77);
            this.labelCaseStatus.Name = "labelCaseStatus";
            this.labelCaseStatus.Size = new System.Drawing.Size(20, 13);
            this.labelCaseStatus.TabIndex = 0;
            this.labelCaseStatus.Text = "Err";
            this.labelCaseStatus.TextChanged += new System.EventHandler(this.LabelCaseStatus_TextChanged);
            // 
            // labelMcuTempStatus
            // 
            this.labelMcuTempStatus.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelMcuTempStatus.AutoSize = true;
            this.labelMcuTempStatus.BackColor = System.Drawing.Color.Red;
            this.labelMcuTempStatus.Location = new System.Drawing.Point(114, 103);
            this.labelMcuTempStatus.Name = "labelMcuTempStatus";
            this.labelMcuTempStatus.Size = new System.Drawing.Size(20, 13);
            this.labelMcuTempStatus.TabIndex = 0;
            this.labelMcuTempStatus.Text = "Err";
            this.labelMcuTempStatus.TextChanged += new System.EventHandler(this.LabelMcuTempStatus_TextChanged);
            // 
            // groupBoxPower
            // 
            this.groupBoxPower.Controls.Add(this.tableLayoutPanel4);
            this.groupBoxPower.ForeColor = System.Drawing.Color.Red;
            this.groupBoxPower.Location = new System.Drawing.Point(500, 64);
            this.groupBoxPower.Name = "groupBoxPower";
            this.groupBoxPower.Size = new System.Drawing.Size(193, 174);
            this.groupBoxPower.TabIndex = 2;
            this.groupBoxPower.TabStop = false;
            this.groupBoxPower.Text = "Питание и аккумулятор";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.30601F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.69399F));
            this.tableLayoutPanel4.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label14, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.labelPowerVoltage, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelBatteryVoltage, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.labelBatteryCharging, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0008F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(183, 90);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Напряжение питания:";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(118, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Статус заряда:";
            // 
            // labelPowerVoltage
            // 
            this.labelPowerVoltage.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelPowerVoltage.AutoSize = true;
            this.labelPowerVoltage.BackColor = System.Drawing.Color.Red;
            this.labelPowerVoltage.Location = new System.Drawing.Point(127, 8);
            this.labelPowerVoltage.Name = "labelPowerVoltage";
            this.labelPowerVoltage.Size = new System.Drawing.Size(20, 13);
            this.labelPowerVoltage.TabIndex = 0;
            this.labelPowerVoltage.Text = "Err";
            this.labelPowerVoltage.TextChanged += new System.EventHandler(this.LabelPowerVoltage_TextChanged);
            // 
            // labelBatteryVoltage
            // 
            this.labelBatteryVoltage.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelBatteryVoltage.AutoSize = true;
            this.labelBatteryVoltage.BackColor = System.Drawing.Color.Red;
            this.labelBatteryVoltage.Location = new System.Drawing.Point(127, 38);
            this.labelBatteryVoltage.Name = "labelBatteryVoltage";
            this.labelBatteryVoltage.Size = new System.Drawing.Size(20, 13);
            this.labelBatteryVoltage.TabIndex = 0;
            this.labelBatteryVoltage.Text = "Err";
            this.labelBatteryVoltage.TextChanged += new System.EventHandler(this.LabelBatteryVoltage_TextChanged);
            // 
            // labelBatteryCharging
            // 
            this.labelBatteryCharging.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelBatteryCharging.AutoSize = true;
            this.labelBatteryCharging.BackColor = System.Drawing.Color.Red;
            this.labelBatteryCharging.Location = new System.Drawing.Point(127, 68);
            this.labelBatteryCharging.Name = "labelBatteryCharging";
            this.labelBatteryCharging.Size = new System.Drawing.Size(20, 13);
            this.labelBatteryCharging.TabIndex = 0;
            this.labelBatteryCharging.Text = "Err";
            this.labelBatteryCharging.TextChanged += new System.EventHandler(this.LabelBatteryCharging_TextChanged);
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 32);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(118, 26);
            this.label21.TabIndex = 0;
            this.label21.Text = "Напряжение аккумулятора:";
            // 
            // groupBoxDin
            // 
            this.groupBoxDin.Controls.Add(this.tableLayoutPanel5);
            this.groupBoxDin.ForeColor = System.Drawing.Color.LimeGreen;
            this.groupBoxDin.Location = new System.Drawing.Point(12, 244);
            this.groupBoxDin.Name = "groupBoxDin";
            this.groupBoxDin.Size = new System.Drawing.Size(133, 124);
            this.groupBoxDin.TabIndex = 2;
            this.groupBoxDin.TabStop = false;
            this.groupBoxDin.Text = "Дискретные входы";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.21951F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel5.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label25, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.labelDin1Status, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelDin2Status, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.labelDin3Status, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.labelDin4Status, 1, 3);
            this.tableLayoutPanel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(123, 98);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 5);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "DIN1:";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 29);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "DIN2:";
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "DIN3:";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 78);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "DIN4:";
            // 
            // labelDin1Status
            // 
            this.labelDin1Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDin1Status.AutoSize = true;
            this.labelDin1Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDin1Status.Location = new System.Drawing.Point(65, 5);
            this.labelDin1Status.Name = "labelDin1Status";
            this.labelDin1Status.Size = new System.Drawing.Size(13, 13);
            this.labelDin1Status.TabIndex = 0;
            this.labelDin1Status.Text = "0";
            // 
            // labelDin2Status
            // 
            this.labelDin2Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDin2Status.AutoSize = true;
            this.labelDin2Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDin2Status.Location = new System.Drawing.Point(65, 29);
            this.labelDin2Status.Name = "labelDin2Status";
            this.labelDin2Status.Size = new System.Drawing.Size(13, 13);
            this.labelDin2Status.TabIndex = 0;
            this.labelDin2Status.Text = "0";
            // 
            // labelDin3Status
            // 
            this.labelDin3Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDin3Status.AutoSize = true;
            this.labelDin3Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDin3Status.Location = new System.Drawing.Point(65, 53);
            this.labelDin3Status.Name = "labelDin3Status";
            this.labelDin3Status.Size = new System.Drawing.Size(13, 13);
            this.labelDin3Status.TabIndex = 0;
            this.labelDin3Status.Text = "0";
            // 
            // labelDin4Status
            // 
            this.labelDin4Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDin4Status.AutoSize = true;
            this.labelDin4Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDin4Status.Location = new System.Drawing.Point(65, 78);
            this.labelDin4Status.Name = "labelDin4Status";
            this.labelDin4Status.Size = new System.Drawing.Size(13, 13);
            this.labelDin4Status.TabIndex = 0;
            this.labelDin4Status.Text = "0";
            // 
            // groupBoxDout
            // 
            this.groupBoxDout.Controls.Add(this.tableLayoutPanel6);
            this.groupBoxDout.ForeColor = System.Drawing.Color.LimeGreen;
            this.groupBoxDout.Location = new System.Drawing.Point(12, 373);
            this.groupBoxDout.Name = "groupBoxDout";
            this.groupBoxDout.Size = new System.Drawing.Size(133, 136);
            this.groupBoxDout.TabIndex = 2;
            this.groupBoxDout.TabStop = false;
            this.groupBoxDout.Text = "Дискретные выходы";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.6129F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.3871F));
            this.tableLayoutPanel6.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label27, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.labelDout1Status, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelDout2Status, 1, 1);
            this.tableLayoutPanel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(123, 50);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 6);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "DOUT1:";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 31);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "DOUT2:";
            // 
            // labelDout1Status
            // 
            this.labelDout1Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDout1Status.AutoSize = true;
            this.labelDout1Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDout1Status.Location = new System.Drawing.Point(66, 6);
            this.labelDout1Status.Name = "labelDout1Status";
            this.labelDout1Status.Size = new System.Drawing.Size(13, 13);
            this.labelDout1Status.TabIndex = 0;
            this.labelDout1Status.Text = "0";
            // 
            // labelDout2Status
            // 
            this.labelDout2Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelDout2Status.AutoSize = true;
            this.labelDout2Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelDout2Status.Location = new System.Drawing.Point(66, 31);
            this.labelDout2Status.Name = "labelDout2Status";
            this.labelDout2Status.Size = new System.Drawing.Size(13, 13);
            this.labelDout2Status.TabIndex = 0;
            this.labelDout2Status.Text = "0";
            // 
            // groupBoxAin
            // 
            this.groupBoxAin.Controls.Add(this.tableLayoutPanel7);
            this.groupBoxAin.ForeColor = System.Drawing.Color.LimeGreen;
            this.groupBoxAin.Location = new System.Drawing.Point(151, 373);
            this.groupBoxAin.Name = "groupBoxAin";
            this.groupBoxAin.Size = new System.Drawing.Size(127, 136);
            this.groupBoxAin.TabIndex = 2;
            this.groupBoxAin.TabStop = false;
            this.groupBoxAin.Text = "Аналоговые входы";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.53571F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.46429F));
            this.tableLayoutPanel7.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label29, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.labelAin1Status, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.labelAin2Status, 1, 1);
            this.tableLayoutPanel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(117, 50);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "AIN1:";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 31);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "AIN2:";
            // 
            // labelAin1Status
            // 
            this.labelAin1Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelAin1Status.AutoSize = true;
            this.labelAin1Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelAin1Status.Location = new System.Drawing.Point(56, 6);
            this.labelAin1Status.Name = "labelAin1Status";
            this.labelAin1Status.Size = new System.Drawing.Size(13, 13);
            this.labelAin1Status.TabIndex = 0;
            this.labelAin1Status.Text = "0";
            // 
            // labelAin2Status
            // 
            this.labelAin2Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelAin2Status.AutoSize = true;
            this.labelAin2Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelAin2Status.Location = new System.Drawing.Point(56, 31);
            this.labelAin2Status.Name = "labelAin2Status";
            this.labelAin2Status.Size = new System.Drawing.Size(13, 13);
            this.labelAin2Status.TabIndex = 0;
            this.labelAin2Status.Text = "0";
            // 
            // groupBoxCount
            // 
            this.groupBoxCount.Controls.Add(this.tableLayoutPanel8);
            this.groupBoxCount.ForeColor = System.Drawing.Color.LimeGreen;
            this.groupBoxCount.Location = new System.Drawing.Point(151, 244);
            this.groupBoxCount.Name = "groupBoxCount";
            this.groupBoxCount.Size = new System.Drawing.Size(127, 124);
            this.groupBoxCount.TabIndex = 2;
            this.groupBoxCount.TabStop = false;
            this.groupBoxCount.Text = "Счетные входы";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.55556F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel8.Controls.Add(this.label30, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label31, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label32, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label33, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.labelCount1Status, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.labelCount2Status, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.labelCount3Status, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.labelCount4Status, 1, 3);
            this.tableLayoutPanel8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(117, 98);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 5);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(59, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "COUNT1:";
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(59, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "COUNT2:";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "COUNT3:";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 78);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(59, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "COUNT4:";
            // 
            // labelCount1Status
            // 
            this.labelCount1Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCount1Status.AutoSize = true;
            this.labelCount1Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCount1Status.Location = new System.Drawing.Point(68, 5);
            this.labelCount1Status.Name = "labelCount1Status";
            this.labelCount1Status.Size = new System.Drawing.Size(13, 13);
            this.labelCount1Status.TabIndex = 0;
            this.labelCount1Status.Text = "0";
            // 
            // labelCount2Status
            // 
            this.labelCount2Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCount2Status.AutoSize = true;
            this.labelCount2Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCount2Status.Location = new System.Drawing.Point(68, 29);
            this.labelCount2Status.Name = "labelCount2Status";
            this.labelCount2Status.Size = new System.Drawing.Size(13, 13);
            this.labelCount2Status.TabIndex = 0;
            this.labelCount2Status.Text = "0";
            // 
            // labelCount3Status
            // 
            this.labelCount3Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCount3Status.AutoSize = true;
            this.labelCount3Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCount3Status.Location = new System.Drawing.Point(68, 53);
            this.labelCount3Status.Name = "labelCount3Status";
            this.labelCount3Status.Size = new System.Drawing.Size(13, 13);
            this.labelCount3Status.TabIndex = 0;
            this.labelCount3Status.Text = "0";
            // 
            // labelCount4Status
            // 
            this.labelCount4Status.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelCount4Status.AutoSize = true;
            this.labelCount4Status.BackColor = System.Drawing.SystemColors.Control;
            this.labelCount4Status.Location = new System.Drawing.Point(68, 78);
            this.labelCount4Status.Name = "labelCount4Status";
            this.labelCount4Status.Size = new System.Drawing.Size(13, 13);
            this.labelCount4Status.TabIndex = 0;
            this.labelCount4Status.Text = "0";
            // 
            // groupBoxLls
            // 
            this.groupBoxLls.Controls.Add(this.tableLayoutPanel9);
            this.groupBoxLls.ForeColor = System.Drawing.Color.Red;
            this.groupBoxLls.Location = new System.Drawing.Point(284, 244);
            this.groupBoxLls.Name = "groupBoxLls";
            this.groupBoxLls.Size = new System.Drawing.Size(227, 265);
            this.groupBoxLls.TabIndex = 2;
            this.groupBoxLls.TabStop = false;
            this.groupBoxLls.Text = "Датчики уровня топлива";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.59908F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.19816F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.79724F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.32719F));
            this.tableLayoutPanel9.Controls.Add(this.label35, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label36, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label37, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label38, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label34, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.label42, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.label43, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label61, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label44, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label45, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label46, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label47, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.labelLls1Addr, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.labelLls2Addr, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.labelLls3Addr, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.labelLls4Addr, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.labelLls5Addr, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.labelLls6Addr, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.labelLls7Addr, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.labelLls8Addr, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.labelLls1Fuel, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.labelLls2Fuel, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.labelLls3Fuel, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.labelLls4Fuel, 2, 4);
            this.tableLayoutPanel9.Controls.Add(this.labelLls5Fuel, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.labelLls6Fuel, 2, 6);
            this.tableLayoutPanel9.Controls.Add(this.labelLls7Fuel, 2, 7);
            this.tableLayoutPanel9.Controls.Add(this.labelLls8Fuel, 2, 8);
            this.tableLayoutPanel9.Controls.Add(this.labelLls1Temp, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.labelLls2Temp, 3, 2);
            this.tableLayoutPanel9.Controls.Add(this.labelLls3Temp, 3, 3);
            this.tableLayoutPanel9.Controls.Add(this.labelLls4Temp, 3, 4);
            this.tableLayoutPanel9.Controls.Add(this.labelLls5Temp, 3, 5);
            this.tableLayoutPanel9.Controls.Add(this.labelLls6Temp, 3, 6);
            this.tableLayoutPanel9.Controls.Add(this.labelLls7Temp, 3, 7);
            this.tableLayoutPanel9.Controls.Add(this.labelLls8Temp, 3, 8);
            this.tableLayoutPanel9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 9;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(217, 239);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(3, 32);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(16, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "1:";
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 58);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "2:";
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 84);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(16, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "3:";
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.SystemColors.Control;
            this.label38.Location = new System.Drawing.Point(25, 6);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(39, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Адрес";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.SystemColors.Control;
            this.label34.Location = new System.Drawing.Point(70, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(62, 26);
            this.label34.TabIndex = 0;
            this.label34.Text = "Уровень топлива";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.SystemColors.Control;
            this.label42.Location = new System.Drawing.Point(138, 6);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(76, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Температура";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(3, 110);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(16, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "4:";
            // 
            // label61
            // 
            this.label61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(3, 6);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(16, 13);
            this.label61.TabIndex = 0;
            this.label61.Text = "№";
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(3, 136);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(16, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "5:";
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(3, 162);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(16, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "6:";
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 188);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(16, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "7:";
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 217);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(16, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "8:";
            // 
            // labelLls1Addr
            // 
            this.labelLls1Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls1Addr.AutoSize = true;
            this.labelLls1Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls1Addr.Location = new System.Drawing.Point(25, 32);
            this.labelLls1Addr.Name = "labelLls1Addr";
            this.labelLls1Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls1Addr.TabIndex = 0;
            this.labelLls1Addr.Text = "-";
            this.labelLls1Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls2Addr
            // 
            this.labelLls2Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls2Addr.AutoSize = true;
            this.labelLls2Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls2Addr.Location = new System.Drawing.Point(25, 58);
            this.labelLls2Addr.Name = "labelLls2Addr";
            this.labelLls2Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls2Addr.TabIndex = 0;
            this.labelLls2Addr.Text = "-";
            this.labelLls2Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls3Addr
            // 
            this.labelLls3Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls3Addr.AutoSize = true;
            this.labelLls3Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls3Addr.Location = new System.Drawing.Point(25, 84);
            this.labelLls3Addr.Name = "labelLls3Addr";
            this.labelLls3Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls3Addr.TabIndex = 0;
            this.labelLls3Addr.Text = "-";
            this.labelLls3Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls4Addr
            // 
            this.labelLls4Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls4Addr.AutoSize = true;
            this.labelLls4Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls4Addr.Location = new System.Drawing.Point(25, 110);
            this.labelLls4Addr.Name = "labelLls4Addr";
            this.labelLls4Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls4Addr.TabIndex = 0;
            this.labelLls4Addr.Text = "-";
            this.labelLls4Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls5Addr
            // 
            this.labelLls5Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls5Addr.AutoSize = true;
            this.labelLls5Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls5Addr.Location = new System.Drawing.Point(25, 136);
            this.labelLls5Addr.Name = "labelLls5Addr";
            this.labelLls5Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls5Addr.TabIndex = 0;
            this.labelLls5Addr.Text = "-";
            this.labelLls5Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls6Addr
            // 
            this.labelLls6Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls6Addr.AutoSize = true;
            this.labelLls6Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls6Addr.Location = new System.Drawing.Point(25, 162);
            this.labelLls6Addr.Name = "labelLls6Addr";
            this.labelLls6Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls6Addr.TabIndex = 0;
            this.labelLls6Addr.Text = "-";
            this.labelLls6Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls7Addr
            // 
            this.labelLls7Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls7Addr.AutoSize = true;
            this.labelLls7Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls7Addr.Location = new System.Drawing.Point(25, 188);
            this.labelLls7Addr.Name = "labelLls7Addr";
            this.labelLls7Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls7Addr.TabIndex = 0;
            this.labelLls7Addr.Text = "-";
            this.labelLls7Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls8Addr
            // 
            this.labelLls8Addr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls8Addr.AutoSize = true;
            this.labelLls8Addr.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls8Addr.Location = new System.Drawing.Point(25, 217);
            this.labelLls8Addr.Name = "labelLls8Addr";
            this.labelLls8Addr.Size = new System.Drawing.Size(39, 13);
            this.labelLls8Addr.TabIndex = 0;
            this.labelLls8Addr.Text = "-";
            this.labelLls8Addr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls1Fuel
            // 
            this.labelLls1Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls1Fuel.AutoSize = true;
            this.labelLls1Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls1Fuel.Location = new System.Drawing.Point(70, 32);
            this.labelLls1Fuel.Name = "labelLls1Fuel";
            this.labelLls1Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls1Fuel.TabIndex = 0;
            this.labelLls1Fuel.Text = "-";
            this.labelLls1Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls2Fuel
            // 
            this.labelLls2Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls2Fuel.AutoSize = true;
            this.labelLls2Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls2Fuel.Location = new System.Drawing.Point(70, 58);
            this.labelLls2Fuel.Name = "labelLls2Fuel";
            this.labelLls2Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls2Fuel.TabIndex = 0;
            this.labelLls2Fuel.Text = "-";
            this.labelLls2Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls3Fuel
            // 
            this.labelLls3Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls3Fuel.AutoSize = true;
            this.labelLls3Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls3Fuel.Location = new System.Drawing.Point(70, 84);
            this.labelLls3Fuel.Name = "labelLls3Fuel";
            this.labelLls3Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls3Fuel.TabIndex = 0;
            this.labelLls3Fuel.Text = "-";
            this.labelLls3Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls4Fuel
            // 
            this.labelLls4Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls4Fuel.AutoSize = true;
            this.labelLls4Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls4Fuel.Location = new System.Drawing.Point(70, 110);
            this.labelLls4Fuel.Name = "labelLls4Fuel";
            this.labelLls4Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls4Fuel.TabIndex = 0;
            this.labelLls4Fuel.Text = "-";
            this.labelLls4Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls5Fuel
            // 
            this.labelLls5Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls5Fuel.AutoSize = true;
            this.labelLls5Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls5Fuel.Location = new System.Drawing.Point(70, 136);
            this.labelLls5Fuel.Name = "labelLls5Fuel";
            this.labelLls5Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls5Fuel.TabIndex = 0;
            this.labelLls5Fuel.Text = "-";
            this.labelLls5Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls6Fuel
            // 
            this.labelLls6Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls6Fuel.AutoSize = true;
            this.labelLls6Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls6Fuel.Location = new System.Drawing.Point(70, 162);
            this.labelLls6Fuel.Name = "labelLls6Fuel";
            this.labelLls6Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls6Fuel.TabIndex = 0;
            this.labelLls6Fuel.Text = "-";
            this.labelLls6Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls7Fuel
            // 
            this.labelLls7Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls7Fuel.AutoSize = true;
            this.labelLls7Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls7Fuel.Location = new System.Drawing.Point(70, 188);
            this.labelLls7Fuel.Name = "labelLls7Fuel";
            this.labelLls7Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls7Fuel.TabIndex = 0;
            this.labelLls7Fuel.Text = "-";
            this.labelLls7Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls8Fuel
            // 
            this.labelLls8Fuel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls8Fuel.AutoSize = true;
            this.labelLls8Fuel.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls8Fuel.Location = new System.Drawing.Point(70, 217);
            this.labelLls8Fuel.Name = "labelLls8Fuel";
            this.labelLls8Fuel.Size = new System.Drawing.Size(62, 13);
            this.labelLls8Fuel.TabIndex = 0;
            this.labelLls8Fuel.Text = "-";
            this.labelLls8Fuel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls1Temp
            // 
            this.labelLls1Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls1Temp.AutoSize = true;
            this.labelLls1Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls1Temp.Location = new System.Drawing.Point(138, 32);
            this.labelLls1Temp.Name = "labelLls1Temp";
            this.labelLls1Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls1Temp.TabIndex = 0;
            this.labelLls1Temp.Text = "-";
            this.labelLls1Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls2Temp
            // 
            this.labelLls2Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls2Temp.AutoSize = true;
            this.labelLls2Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls2Temp.Location = new System.Drawing.Point(138, 58);
            this.labelLls2Temp.Name = "labelLls2Temp";
            this.labelLls2Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls2Temp.TabIndex = 0;
            this.labelLls2Temp.Text = "-";
            this.labelLls2Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls3Temp
            // 
            this.labelLls3Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls3Temp.AutoSize = true;
            this.labelLls3Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls3Temp.Location = new System.Drawing.Point(138, 84);
            this.labelLls3Temp.Name = "labelLls3Temp";
            this.labelLls3Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls3Temp.TabIndex = 0;
            this.labelLls3Temp.Text = "-";
            this.labelLls3Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls4Temp
            // 
            this.labelLls4Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls4Temp.AutoSize = true;
            this.labelLls4Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls4Temp.Location = new System.Drawing.Point(138, 110);
            this.labelLls4Temp.Name = "labelLls4Temp";
            this.labelLls4Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls4Temp.TabIndex = 0;
            this.labelLls4Temp.Text = "-";
            this.labelLls4Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls5Temp
            // 
            this.labelLls5Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls5Temp.AutoSize = true;
            this.labelLls5Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls5Temp.Location = new System.Drawing.Point(138, 136);
            this.labelLls5Temp.Name = "labelLls5Temp";
            this.labelLls5Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls5Temp.TabIndex = 0;
            this.labelLls5Temp.Text = "-";
            this.labelLls5Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls6Temp
            // 
            this.labelLls6Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls6Temp.AutoSize = true;
            this.labelLls6Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls6Temp.Location = new System.Drawing.Point(138, 162);
            this.labelLls6Temp.Name = "labelLls6Temp";
            this.labelLls6Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls6Temp.TabIndex = 0;
            this.labelLls6Temp.Text = "-";
            this.labelLls6Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls7Temp
            // 
            this.labelLls7Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls7Temp.AutoSize = true;
            this.labelLls7Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls7Temp.Location = new System.Drawing.Point(138, 188);
            this.labelLls7Temp.Name = "labelLls7Temp";
            this.labelLls7Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls7Temp.TabIndex = 0;
            this.labelLls7Temp.Text = "-";
            this.labelLls7Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLls8Temp
            // 
            this.labelLls8Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLls8Temp.AutoSize = true;
            this.labelLls8Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelLls8Temp.Location = new System.Drawing.Point(138, 217);
            this.labelLls8Temp.Name = "labelLls8Temp";
            this.labelLls8Temp.Size = new System.Drawing.Size(76, 13);
            this.labelLls8Temp.TabIndex = 0;
            this.labelLls8Temp.Text = "-";
            this.labelLls8Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxTemp
            // 
            this.groupBoxTemp.Controls.Add(this.tableLayoutPanel10);
            this.groupBoxTemp.ForeColor = System.Drawing.Color.Red;
            this.groupBoxTemp.Location = new System.Drawing.Point(517, 244);
            this.groupBoxTemp.Name = "groupBoxTemp";
            this.groupBoxTemp.Size = new System.Drawing.Size(274, 265);
            this.groupBoxTemp.TabIndex = 2;
            this.groupBoxTemp.TabStop = false;
            this.groupBoxTemp.Text = "Датчики DS18B20";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.923664F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.77863F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.91603F));
            this.tableLayoutPanel10.Controls.Add(this.label39, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label40, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label41, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label48, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label51, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label52, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label53, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.label54, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label55, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.labelDs1Id, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.labelDs2Id, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.labelDs3Id, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.labelDs4Id, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.labelDs5Id, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.labelDs6Id, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.labelDs7Id, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.labelDs8Id, 1, 8);
            this.tableLayoutPanel10.Controls.Add(this.labelDs1Temp, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.labelDs2Temp, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.labelDs3Temp, 2, 3);
            this.tableLayoutPanel10.Controls.Add(this.labelDs4Temp, 2, 4);
            this.tableLayoutPanel10.Controls.Add(this.labelDs5Temp, 2, 5);
            this.tableLayoutPanel10.Controls.Add(this.labelDs6Temp, 2, 6);
            this.tableLayoutPanel10.Controls.Add(this.labelDs7Temp, 2, 7);
            this.tableLayoutPanel10.Controls.Add(this.labelDs8Temp, 2, 8);
            this.tableLayoutPanel10.Controls.Add(this.label50, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.label59, 0, 0);
            this.tableLayoutPanel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 9;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.10815F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(262, 239);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 32);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(20, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "1:";
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 58);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(20, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "2:";
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(3, 84);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "3:";
            // 
            // label48
            // 
            this.label48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.SystemColors.Control;
            this.label48.Location = new System.Drawing.Point(29, 6);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(148, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "ID";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(3, 110);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(20, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "4:";
            // 
            // label52
            // 
            this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(3, 136);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(20, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "5:";
            // 
            // label53
            // 
            this.label53.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(3, 162);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(20, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "6:";
            // 
            // label54
            // 
            this.label54.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(3, 188);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(20, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "7:";
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(3, 217);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(20, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "8:";
            // 
            // labelDs1Id
            // 
            this.labelDs1Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs1Id.AutoSize = true;
            this.labelDs1Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs1Id.Location = new System.Drawing.Point(29, 32);
            this.labelDs1Id.Name = "labelDs1Id";
            this.labelDs1Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs1Id.TabIndex = 0;
            this.labelDs1Id.Text = "XX-XX-XX-XX-XX-XX-XX-XX";
            this.labelDs1Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs2Id
            // 
            this.labelDs2Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs2Id.AutoSize = true;
            this.labelDs2Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs2Id.Location = new System.Drawing.Point(29, 58);
            this.labelDs2Id.Name = "labelDs2Id";
            this.labelDs2Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs2Id.TabIndex = 0;
            this.labelDs2Id.Text = "-";
            this.labelDs2Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs3Id
            // 
            this.labelDs3Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs3Id.AutoSize = true;
            this.labelDs3Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs3Id.Location = new System.Drawing.Point(29, 84);
            this.labelDs3Id.Name = "labelDs3Id";
            this.labelDs3Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs3Id.TabIndex = 0;
            this.labelDs3Id.Text = "-";
            this.labelDs3Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs4Id
            // 
            this.labelDs4Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs4Id.AutoSize = true;
            this.labelDs4Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs4Id.Location = new System.Drawing.Point(29, 110);
            this.labelDs4Id.Name = "labelDs4Id";
            this.labelDs4Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs4Id.TabIndex = 0;
            this.labelDs4Id.Text = "-";
            this.labelDs4Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs5Id
            // 
            this.labelDs5Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs5Id.AutoSize = true;
            this.labelDs5Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs5Id.Location = new System.Drawing.Point(29, 136);
            this.labelDs5Id.Name = "labelDs5Id";
            this.labelDs5Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs5Id.TabIndex = 0;
            this.labelDs5Id.Text = "-";
            this.labelDs5Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs6Id
            // 
            this.labelDs6Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs6Id.AutoSize = true;
            this.labelDs6Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs6Id.Location = new System.Drawing.Point(29, 162);
            this.labelDs6Id.Name = "labelDs6Id";
            this.labelDs6Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs6Id.TabIndex = 0;
            this.labelDs6Id.Text = "-";
            this.labelDs6Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs7Id
            // 
            this.labelDs7Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs7Id.AutoSize = true;
            this.labelDs7Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs7Id.Location = new System.Drawing.Point(29, 188);
            this.labelDs7Id.Name = "labelDs7Id";
            this.labelDs7Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs7Id.TabIndex = 0;
            this.labelDs7Id.Text = "-";
            this.labelDs7Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs8Id
            // 
            this.labelDs8Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs8Id.AutoSize = true;
            this.labelDs8Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs8Id.Location = new System.Drawing.Point(29, 217);
            this.labelDs8Id.Name = "labelDs8Id";
            this.labelDs8Id.Size = new System.Drawing.Size(148, 13);
            this.labelDs8Id.TabIndex = 0;
            this.labelDs8Id.Text = "-";
            this.labelDs8Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs1Temp
            // 
            this.labelDs1Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs1Temp.AutoSize = true;
            this.labelDs1Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs1Temp.Location = new System.Drawing.Point(183, 32);
            this.labelDs1Temp.Name = "labelDs1Temp";
            this.labelDs1Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs1Temp.TabIndex = 0;
            this.labelDs1Temp.Text = "-";
            this.labelDs1Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs2Temp
            // 
            this.labelDs2Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs2Temp.AutoSize = true;
            this.labelDs2Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs2Temp.Location = new System.Drawing.Point(183, 58);
            this.labelDs2Temp.Name = "labelDs2Temp";
            this.labelDs2Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs2Temp.TabIndex = 0;
            this.labelDs2Temp.Text = "-";
            this.labelDs2Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs3Temp
            // 
            this.labelDs3Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs3Temp.AutoSize = true;
            this.labelDs3Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs3Temp.Location = new System.Drawing.Point(183, 84);
            this.labelDs3Temp.Name = "labelDs3Temp";
            this.labelDs3Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs3Temp.TabIndex = 0;
            this.labelDs3Temp.Text = "-";
            this.labelDs3Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs4Temp
            // 
            this.labelDs4Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs4Temp.AutoSize = true;
            this.labelDs4Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs4Temp.Location = new System.Drawing.Point(183, 110);
            this.labelDs4Temp.Name = "labelDs4Temp";
            this.labelDs4Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs4Temp.TabIndex = 0;
            this.labelDs4Temp.Text = "-";
            this.labelDs4Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs5Temp
            // 
            this.labelDs5Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs5Temp.AutoSize = true;
            this.labelDs5Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs5Temp.Location = new System.Drawing.Point(183, 136);
            this.labelDs5Temp.Name = "labelDs5Temp";
            this.labelDs5Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs5Temp.TabIndex = 0;
            this.labelDs5Temp.Text = "-";
            this.labelDs5Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs6Temp
            // 
            this.labelDs6Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs6Temp.AutoSize = true;
            this.labelDs6Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs6Temp.Location = new System.Drawing.Point(183, 162);
            this.labelDs6Temp.Name = "labelDs6Temp";
            this.labelDs6Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs6Temp.TabIndex = 0;
            this.labelDs6Temp.Text = "-";
            this.labelDs6Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs7Temp
            // 
            this.labelDs7Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs7Temp.AutoSize = true;
            this.labelDs7Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs7Temp.Location = new System.Drawing.Point(183, 188);
            this.labelDs7Temp.Name = "labelDs7Temp";
            this.labelDs7Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs7Temp.TabIndex = 0;
            this.labelDs7Temp.Text = "-";
            this.labelDs7Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDs8Temp
            // 
            this.labelDs8Temp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDs8Temp.AutoSize = true;
            this.labelDs8Temp.BackColor = System.Drawing.SystemColors.Control;
            this.labelDs8Temp.Location = new System.Drawing.Point(183, 217);
            this.labelDs8Temp.Name = "labelDs8Temp";
            this.labelDs8Temp.Size = new System.Drawing.Size(76, 13);
            this.labelDs8Temp.TabIndex = 0;
            this.labelDs8Temp.Text = "-";
            this.labelDs8Temp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.SystemColors.Control;
            this.label50.Location = new System.Drawing.Point(183, 6);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(76, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Температура";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(3, 6);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(20, 13);
            this.label59.TabIndex = 0;
            this.label59.Text = "№";
            // 
            // groupBoxiButton
            // 
            this.groupBoxiButton.BackColor = System.Drawing.SystemColors.Control;
            this.groupBoxiButton.Controls.Add(this.tableLayoutPanel11);
            this.groupBoxiButton.ForeColor = System.Drawing.Color.Red;
            this.groupBoxiButton.Location = new System.Drawing.Point(699, 64);
            this.groupBoxiButton.Name = "groupBoxiButton";
            this.groupBoxiButton.Size = new System.Drawing.Size(191, 174);
            this.groupBoxiButton.TabIndex = 2;
            this.groupBoxiButton.TabStop = false;
            this.groupBoxiButton.Text = "Метки iButton";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.923664F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.77863F));
            this.tableLayoutPanel11.Controls.Add(this.label49, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label56, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.label57, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.label58, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.labelIbutton1Id, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.labelIbutton2Id, 1, 2);
            this.tableLayoutPanel11.Controls.Add(this.labelIbutton3Id, 1, 3);
            this.tableLayoutPanel11.Controls.Add(this.label60, 0, 0);
            this.tableLayoutPanel11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 4;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11482F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(176, 106);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // label49
            // 
            this.label49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(3, 32);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(19, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "1:";
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(3, 58);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(19, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "2:";
            // 
            // label57
            // 
            this.label57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(3, 85);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(19, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "3:";
            // 
            // label58
            // 
            this.label58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.SystemColors.Control;
            this.label58.Location = new System.Drawing.Point(28, 6);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(145, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "ID";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelIbutton1Id
            // 
            this.labelIbutton1Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIbutton1Id.AutoSize = true;
            this.labelIbutton1Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelIbutton1Id.Location = new System.Drawing.Point(28, 32);
            this.labelIbutton1Id.Name = "labelIbutton1Id";
            this.labelIbutton1Id.Size = new System.Drawing.Size(145, 13);
            this.labelIbutton1Id.TabIndex = 0;
            this.labelIbutton1Id.Text = "XX-XX-XX-XX-XX-XX-XX-XX";
            this.labelIbutton1Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelIbutton2Id
            // 
            this.labelIbutton2Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIbutton2Id.AutoSize = true;
            this.labelIbutton2Id.BackColor = System.Drawing.SystemColors.Control;
            this.labelIbutton2Id.Location = new System.Drawing.Point(28, 58);
            this.labelIbutton2Id.Name = "labelIbutton2Id";
            this.labelIbutton2Id.Size = new System.Drawing.Size(145, 13);
            this.labelIbutton2Id.TabIndex = 0;
            this.labelIbutton2Id.Text = "-";
            this.labelIbutton2Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelIbutton3Id
            // 
            this.labelIbutton3Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIbutton3Id.AutoSize = true;
            this.labelIbutton3Id.BackColor = System.Drawing.Color.Transparent;
            this.labelIbutton3Id.Location = new System.Drawing.Point(28, 85);
            this.labelIbutton3Id.Name = "labelIbutton3Id";
            this.labelIbutton3Id.Size = new System.Drawing.Size(145, 13);
            this.labelIbutton3Id.TabIndex = 0;
            this.labelIbutton3Id.Text = "-";
            this.labelIbutton3Id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label60
            // 
            this.label60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 6);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(19, 13);
            this.label60.TabIndex = 0;
            this.label60.Text = "№";
            // 
            // buttonPrintScreenStatus
            // 
            this.buttonPrintScreenStatus.Location = new System.Drawing.Point(805, 470);
            this.buttonPrintScreenStatus.Name = "buttonPrintScreenStatus";
            this.buttonPrintScreenStatus.Size = new System.Drawing.Size(89, 40);
            this.buttonPrintScreenStatus.TabIndex = 3;
            this.buttonPrintScreenStatus.Text = "Сделать снимок";
            this.buttonPrintScreenStatus.UseVisualStyleBackColor = true;
            this.buttonPrintScreenStatus.Click += new System.EventHandler(this.ButtonPrintScreenStatus_Click);
            // 
            // labelUnitTime
            // 
            this.labelUnitTime.AutoSize = true;
            this.labelUnitTime.Location = new System.Drawing.Point(13, 35);
            this.labelUnitTime.Name = "labelUnitTime";
            this.labelUnitTime.Size = new System.Drawing.Size(91, 13);
            this.labelUnitTime.TabIndex = 0;
            this.labelUnitTime.Text = "Время прибора: ";
            // 
            // buttonTestCan
            // 
            this.buttonTestCan.Location = new System.Drawing.Point(805, 411);
            this.buttonTestCan.Name = "buttonTestCan";
            this.buttonTestCan.Size = new System.Drawing.Size(85, 40);
            this.buttonTestCan.TabIndex = 4;
            this.buttonTestCan.Text = "Тест CAN";
            this.buttonTestCan.UseVisualStyleBackColor = true;
            this.buttonTestCan.Click += new System.EventHandler(this.ButtonTestCan_Click);
            // 
            // Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 514);
            this.Controls.Add(this.buttonTestCan);
            this.Controls.Add(this.groupBoxGnss);
            this.Controls.Add(this.groupBoxLls);
            this.Controls.Add(this.buttonPrintScreenStatus);
            this.Controls.Add(this.groupBoxPower);
            this.Controls.Add(this.groupBoxiButton);
            this.Controls.Add(this.groupBoxTemp);
            this.Controls.Add(this.groupBoxAin);
            this.Controls.Add(this.groupBoxDout);
            this.Controls.Add(this.groupBoxCount);
            this.Controls.Add(this.groupBoxDin);
            this.Controls.Add(this.groupBoxPeriph);
            this.Controls.Add(this.groupBoxGsm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelMode);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.labelImei);
            this.Controls.Add(this.labelHWVersion);
            this.Controls.Add(this.labelUnitTime);
            this.Controls.Add(this.labelFWVersion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Status";
            this.Text = "Статус устройства";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnStatusFromClosed);
            this.Load += new System.EventHandler(this.Status_Load);
            this.groupBoxGsm.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBoxGnss.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBoxPeriph.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBoxPower.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBoxDin.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBoxDout.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBoxAin.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.groupBoxCount.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.groupBoxLls.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.groupBoxTemp.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBoxiButton.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerStatusRequest;
        private System.Windows.Forms.Label labelFWVersion;
        private System.Windows.Forms.Label labelHWVersion;
        private System.Windows.Forms.Label labelImei;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.GroupBox groupBoxGsm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelGsmPhyStatus;
        private System.Windows.Forms.Label labelGsmSimStatus;
        private System.Windows.Forms.Label labelGsmGprsStatus;
        private System.Windows.Forms.Label labelGsmServerConStatus;
        private System.Windows.Forms.Label labelGsmServerTxStatus;
        private System.Windows.Forms.Label labelGsmRssiStatus;
        private System.Windows.Forms.GroupBox groupBoxGnss;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelGnssPhyStatus;
        private System.Windows.Forms.Label labelGnssFixStatus;
        private System.Windows.Forms.Label labelGnssHdopStatus;
        private System.Windows.Forms.GroupBox groupBoxPeriph;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelAccelStatus;
        private System.Windows.Forms.Label labelMotionStatus;
        private System.Windows.Forms.Label labelSpiFlashStatus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelCaseStatus;
        private System.Windows.Forms.Label labelMcuTempStatus;
        private System.Windows.Forms.GroupBox groupBoxPower;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelPowerVoltage;
        private System.Windows.Forms.Label labelBatteryVoltage;
        private System.Windows.Forms.Label labelBatteryCharging;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBoxDin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label labelDin1Status;
        private System.Windows.Forms.Label labelDin2Status;
        private System.Windows.Forms.Label labelDin3Status;
        private System.Windows.Forms.Label labelDin4Status;
        private System.Windows.Forms.GroupBox groupBoxDout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelDout1Status;
        private System.Windows.Forms.Label labelDout2Status;
        private System.Windows.Forms.GroupBox groupBoxAin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label labelAin1Status;
        private System.Windows.Forms.Label labelAin2Status;
        private System.Windows.Forms.GroupBox groupBoxCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label labelCount1Status;
        private System.Windows.Forms.Label labelCount2Status;
        private System.Windows.Forms.Label labelCount3Status;
        private System.Windows.Forms.Label labelCount4Status;
        private System.Windows.Forms.GroupBox groupBoxLls;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label labelLls1Addr;
        private System.Windows.Forms.Label labelLls2Addr;
        private System.Windows.Forms.Label labelLls3Addr;
        private System.Windows.Forms.Label labelLls4Addr;
        private System.Windows.Forms.Label labelLls5Addr;
        private System.Windows.Forms.Label labelLls6Addr;
        private System.Windows.Forms.Label labelLls7Addr;
        private System.Windows.Forms.Label labelLls8Addr;
        private System.Windows.Forms.Label labelLls1Fuel;
        private System.Windows.Forms.Label labelLls2Fuel;
        private System.Windows.Forms.Label labelLls3Fuel;
        private System.Windows.Forms.Label labelLls4Fuel;
        private System.Windows.Forms.Label labelLls5Fuel;
        private System.Windows.Forms.Label labelLls6Fuel;
        private System.Windows.Forms.Label labelLls7Fuel;
        private System.Windows.Forms.Label labelLls8Fuel;
        private System.Windows.Forms.Label labelLls1Temp;
        private System.Windows.Forms.Label labelLls2Temp;
        private System.Windows.Forms.Label labelLls3Temp;
        private System.Windows.Forms.Label labelLls4Temp;
        private System.Windows.Forms.Label labelLls5Temp;
        private System.Windows.Forms.Label labelLls6Temp;
        private System.Windows.Forms.Label labelLls7Temp;
        private System.Windows.Forms.Label labelLls8Temp;
        private System.Windows.Forms.GroupBox groupBoxTemp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label labelDs1Id;
        private System.Windows.Forms.Label labelDs2Id;
        private System.Windows.Forms.Label labelDs3Id;
        private System.Windows.Forms.Label labelDs4Id;
        private System.Windows.Forms.Label labelDs5Id;
        private System.Windows.Forms.Label labelDs6Id;
        private System.Windows.Forms.Label labelDs7Id;
        private System.Windows.Forms.Label labelDs8Id;
        private System.Windows.Forms.Label labelDs1Temp;
        private System.Windows.Forms.Label labelDs2Temp;
        private System.Windows.Forms.Label labelDs3Temp;
        private System.Windows.Forms.Label labelDs4Temp;
        private System.Windows.Forms.Label labelDs5Temp;
        private System.Windows.Forms.Label labelDs6Temp;
        private System.Windows.Forms.Label labelDs7Temp;
        private System.Windows.Forms.Label labelDs8Temp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBoxiButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label labelIbutton1Id;
        private System.Windows.Forms.Label labelIbutton2Id;
        private System.Windows.Forms.Label labelIbutton3Id;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button buttonPrintScreenStatus;
        private System.Windows.Forms.Label labelUnitTime;
        private System.Windows.Forms.Button buttonTestCan;
    }
}