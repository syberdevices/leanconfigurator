﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Security;
using Newtonsoft.Json;
using RJCP.IO.Ports;
using System.Security.Cryptography;

namespace LEANConfigurator
{
    public partial class Configurator : Form
    {
        public Configurator()
        {
            InitializeComponent();
            GetAvailablePorts();
        }

        public Settings settings = new Settings()
        {
            password = "0000",
            id = "",
            protocol = "Wialon",
            period = new Period()
            {
                data_send = new SubPeriod()
                {
                    in_active_mode = "20",
                    in_standby_mode = "60"
                },
                data_write = new SubPeriod()
                {
                    in_active_mode = "10",
                    in_standby_mode = "20"
                },
                fls_read = new SubPeriod()
                {
                    in_active_mode = "15",
                    in_standby_mode = "40"
                }
            },
            fls_sensor = new FlsSensor[8]
            {
                new FlsSensor() { address = "1" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
                new FlsSensor() { address = "" },
            },
            ds18b20 = new DS18B20[8]
            {
                new DS18B20() { id = "11-22-33-44-55-66-77-88" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
                new DS18B20() { id = "" },
            },
            gnss = new GNSS()
            {
                filter = "on",
                freeze_speed = "5",
                freeze_time = "60",
                hdop_max = "0.6",
                satellites_min = "4",
                max_speed = "180",
                speed_avg = "20"
            },
            min_rssi = "15",
            untracked_course = "10",
            untracked_distance = "100",
            acceleration_threshold = "2",
            apn = new APN()
            {
                address = "internet.emt.ee",
                user = "",
                password = ""
            },
            main_host = new MainHost()
            {
                address = "193.193.165.165",
                port = "20332",
                wait_response = "true"
            },
            /*            ftp_host = new FtpHost()
                        {
                            address = "c1-ftp1.netpoint-dc.com",
                            login = "virusaga3878",
                            password = "RxmPC8iO"
                        },*/
            log_host = new LogHost()
            {
                address = "88.204.74.21",
                port = "12345"
            },
            dins = new DIN[4]
            {
                new DIN() { type = "no", mode = "2" },
                new DIN() { type = "no", mode = "4" },
                new DIN() { type = "no", mode = "4" },
                new DIN() { type = "no", mode = "4" }
            },
            douts = new DOUT[2]
            {
                new DOUT() { type = "no" },
                new DOUT() { type = "no" }
            },
            ains = new AIN[2]
            {
                new AIN() { threshold = "10.0" },
                new AIN() { threshold = "10.0" }
            },
            white_phone_numbers = new string[4]
            {
                "79528025667",
                "79234154072",
                "79095434413",
                "79138506007"
            },
            bluetooth = new Bluetooth()
            {
                used = "false",
                auto_answer = "true",
                mac = "12-34-56-78-90-01"
            },
            can = new Can()
            {
                used = "false",
                used_param = "0",
                period = new SubPeriod()
                {
                    in_active_mode = "10",
                    in_standby_mode = "20"
                },
                alert = new CanAlert()
                {
                    temp_trh = new AlertThreshold()
                    {
                        max = "110",
                        duration = "30"
                    },
                    rpm_trh = new AlertThreshold()
                    {
                        max = "5000",
                        duration = "30"
                    }
                }
            },
        };

        private bool terminalConnected = false;
        const int writeTimeoutDefault = 500;
        const int readTimeoutDefault = 1000;
        const string leanConfiguratorVersion = "2.3";
        public SerialPortStream serialPort = new SerialPortStream();


        private void MoveFieldsToSettings()
        {
            settings.password = textBoxPasswordSett.Text;
            settings.id = textBoxID.Text;
            settings.protocol = comboBoxProtocolType.Text;

            settings.period.data_send.in_active_mode = textBoxTriggerPointActive.Text;
            settings.period.data_send.in_standby_mode = textBoxTriggerPointStandby.Text;
            settings.period.data_write.in_active_mode = textBoxTriggerPointActive.Text;
            settings.period.data_write.in_standby_mode = textBoxTriggerPointStandby.Text;
            settings.period.fls_read.in_active_mode = textBoxTriggerLLSActive.Text;
            settings.period.fls_read.in_standby_mode = textBoxTriggerLLSStandby.Text;

            settings.can.used = checkBoxCanUse.Checked ? "true" : "false";
            int can_used_param = (checkBoxCanRpmUse.Checked ? 0x1 : 0) |
                                    (checkBoxCanFuelLevelPercentUse.Checked ? 0x2 : 0) |
                                    (checkBoxCanFuelLevelLiterUse.Checked ? 0x4 : 0) |
                                    (checkBoxCanOdometerUse.Checked ? 0x8 : 0) |
                                    (checkBoxCanIgnitionUse.Checked ? 0x10 : 0) |
                                    (checkBoxCanEngineTempUse.Checked ? 0x20 : 0) |
                                    (checkBoxCanCheckOilUse.Checked ? 0x40 : 0);
            settings.can.used_param = can_used_param.ToString();
            settings.can.period.in_active_mode = textBoxTriggerCanActive.Text;
            settings.can.period.in_standby_mode = textBoxTriggerCanStandby.Text;
            settings.can.alert.rpm_trh.max = textBoxCanAlertRpmMax.Text;
            settings.can.alert.rpm_trh.duration = textBoxCanAlertRpmDuration.Text;
            settings.can.alert.temp_trh.max = textBoxCanAlertTempMax.Text;
            settings.can.alert.temp_trh.duration = textBoxCanAlertTempDuration.Text;
            settings.can.alert.on_check_oil = checkBoxCanAlertCheckOil.Checked ? "true" : "false";
            settings.can.gen_pack_on_ign = checkBoxCanIgnitionGenPacketOnChange.Checked ? "true" : "false";

            settings.fls_sensor[0].address = checkBoxLLS1.Checked ? textBoxLLS1.Text : "";
            settings.fls_sensor[1].address = checkBoxLLS2.Checked ? textBoxLLS2.Text : "";
            settings.fls_sensor[2].address = checkBoxLLS3.Checked ? textBoxLLS3.Text : "";
            settings.fls_sensor[3].address = checkBoxLLS4.Checked ? textBoxLLS4.Text : "";
            settings.fls_sensor[4].address = checkBoxLLS5.Checked ? textBoxLLS5.Text : "";
            settings.fls_sensor[5].address = checkBoxLLS6.Checked ? textBoxLLS6.Text : "";
            settings.fls_sensor[6].address = checkBoxLLS7.Checked ? textBoxLLS7.Text : "";
            settings.fls_sensor[7].address = checkBoxLLS8.Checked ? textBoxLLS8.Text : "";

            settings.ds18b20[0].id = checkBoxTemp1Enable.Checked ? textBoxTempID1.Text : "";
            settings.ds18b20[1].id = checkBoxTemp2Enable.Checked ? textBoxTempID2.Text : "";
            settings.ds18b20[2].id = checkBoxTemp3Enable.Checked ? textBoxTempID3.Text : "";
            settings.ds18b20[3].id = checkBoxTemp4Enable.Checked ? textBoxTempID4.Text : "";
            settings.ds18b20[4].id = checkBoxTemp5Enable.Checked ? textBoxTempID5.Text : "";
            settings.ds18b20[5].id = checkBoxTemp6Enable.Checked ? textBoxTempID6.Text : "";
            settings.ds18b20[6].id = checkBoxTemp7Enable.Checked ? textBoxTempID7.Text : "";
            settings.ds18b20[7].id = checkBoxTemp8Enable.Checked ? textBoxTempID8.Text : "";

            settings.gnss.filter = checkBoxFilterEnable.Checked ? "on" : "off";
            settings.gnss.freeze_speed = checkBoxFilterEnable.Checked ? textBoxFilterSpeedMin.Text : "5";
            settings.gnss.freeze_time = checkBoxFilterEnable.Checked ? textBoxFilterFreezeTime.Text : "60";
            settings.gnss.hdop_max = checkBoxFilterEnable.Checked ? textBoxFilterHDOP.Text : "0.6";
            settings.gnss.satellites_min = checkBoxFilterEnable.Checked ? textBoxFilterSatellitesMin.Text : "4";
            settings.gnss.max_speed = checkBoxFilterEnable.Checked ? textBoxFilterSpeedMax.Text : "180";
            settings.gnss.speed_avg = checkBoxFilterEnable.Checked ? textBoxFilterSpeedAvg.Text : "20";

            settings.min_rssi = textBoxRssiMin.Text;
            settings.untracked_course = textBoxTriggerPointDegree.Text;
            settings.untracked_distance = textBoxTriggerPointMeter.Text;
            settings.acceleration_threshold = textBoxAccelThreshold.Text;

            settings.apn.address = radioButtonAPNUser.Checked ? textBoxAPNAddress.Text : "";
            settings.apn.user = radioButtonAPNUser.Checked ? textBoxAPNLogin.Text : "";
            settings.apn.password = radioButtonAPNUser.Checked ? textBoxAPNPassword.Text : "";

            // main_host не разрешено настраивать из конфигуратора. Пустая строка - настройки не меняются
            settings.main_host.address = "";
            settings.main_host.port = "";
            settings.main_host.wait_response = "";

            // log_host не разрешено настраивать из конфигуратора. Пустая строка - настройки не меняются
            settings.log_host.address = "";
            settings.log_host.port = "";

            settings.dins[0].mode = comboBoxDI1Mode.SelectedIndex.ToString();
            settings.dins[0].type = comboBoxDI1Type.Text;
            settings.dins[1].mode = comboBoxDI2Mode.SelectedIndex.ToString();
            settings.dins[1].type = comboBoxDI2Type.Text;
            settings.dins[2].mode = comboBoxDI3Mode.SelectedIndex.ToString();
            settings.dins[2].type = comboBoxDI3Type.Text;
            settings.dins[3].mode = comboBoxDI4Mode.SelectedIndex.ToString();
            settings.dins[3].type = comboBoxDI4Type.Text;

            settings.douts[0].type = comboBoxDO1Type.Text;
            settings.douts[1].type = comboBoxDO2Type.Text;

            settings.ains[0].threshold = textBoxAI1Trh.Text;
            settings.ains[1].threshold = textBoxAI2Trh.Text;

            settings.white_phone_numbers[0] = checkBoxWhiteNumber1.Checked ? textBoxWhiteNumber1.Text : "";
            settings.white_phone_numbers[1] = checkBoxWhiteNumber2.Checked ? textBoxWhiteNumber2.Text : "";
            settings.white_phone_numbers[2] = checkBoxWhiteNumber3.Checked ? textBoxWhiteNumber3.Text : "";
            settings.white_phone_numbers[3] = checkBoxWhiteNumber4.Checked ? textBoxWhiteNumber4.Text : "";
            settings.white_phone_numbers[4] = checkBoxWhiteNumber5.Checked ? textBoxWhiteNumber5.Text : "";
            settings.white_phone_numbers[5] = checkBoxWhiteNumber6.Checked ? textBoxWhiteNumber6.Text : "";
            settings.white_phone_numbers[6] = checkBoxWhiteNumber7.Checked ? textBoxWhiteNumber7.Text : "";
            settings.white_phone_numbers[7] = checkBoxWhiteNumber8.Checked ? textBoxWhiteNumber8.Text : "";
            settings.white_phone_numbers[8] = checkBoxWhiteNumber9.Checked ? textBoxWhiteNumber9.Text : "";
            settings.white_phone_numbers[9] = checkBoxWhiteNumber10.Checked ? textBoxWhiteNumber10.Text : "";
            settings.white_phone_numbers[10] = checkBoxWhiteNumber11.Checked ? textBoxWhiteNumber11.Text : "";
            settings.white_phone_numbers[11] = checkBoxWhiteNumber12.Checked ? textBoxWhiteNumber12.Text : "";
            settings.white_phone_numbers[12] = checkBoxWhiteNumber13.Checked ? textBoxWhiteNumber13.Text : "";
            settings.white_phone_numbers[13] = checkBoxWhiteNumber14.Checked ? textBoxWhiteNumber14.Text : "";
            settings.white_phone_numbers[14] = checkBoxWhiteNumber15.Checked ? textBoxWhiteNumber15.Text : "";
            settings.white_phone_numbers[15] = checkBoxWhiteNumber16.Checked ? textBoxWhiteNumber16.Text : "";
            settings.white_phone_numbers[16] = checkBoxWhiteNumber17.Checked ? textBoxWhiteNumber17.Text : "";
            settings.white_phone_numbers[17] = checkBoxWhiteNumber18.Checked ? textBoxWhiteNumber18.Text : "";

            settings.master_phone_numbers[0] = checkBoxMasterNumber1.Checked ? textBoxMasterNumber1.Text : "";
            settings.master_phone_numbers[1] = checkBoxMasterNumber2.Checked ? textBoxMasterNumber2.Text : "";
            settings.master_phone_numbers[2] = checkBoxMasterNumber3.Checked ? textBoxMasterNumber3.Text : "";
            settings.master_phone_numbers[3] = checkBoxMasterNumber4.Checked ? textBoxMasterNumber4.Text : "";
            settings.master_phone_numbers[4] = checkBoxMasterNumber5.Checked ? textBoxMasterNumber5.Text : "";

            settings.bluetooth.used = checkBoxBluetoothUse.Checked ? "true" : "false";
            settings.bluetooth.auto_answer = checkBoxBluetoothAutoAnswer.Checked ? "true" : "false";
            settings.bluetooth.mac = checkBoxBluetoothUse.Checked ? textBoxBluetoothMAC.Text : "";

            settings.auto_id = checkBoxIDAuto.Checked ? "true" : "false";

            settings.log_level = comboBoxLogLevel.Text;

            settings.auto_update = checkBoxAutoUpdate.Checked ? "true" : "false";

            // settings.settings_version = "4";     Поле не предназначено для установки
        }
        
        private void MoveSettingsToFields()
        {
            textBoxPasswordSett.Text = settings.password;
            textBoxID.Text = settings.id;
            comboBoxProtocolType.Text = settings.protocol;

            textBoxTriggerPointActive.Text = settings.period.data_send.in_active_mode;
            textBoxTriggerPointStandby.Text = settings.period.data_send.in_standby_mode;
            textBoxTriggerPointActive.Text = settings.period.data_write.in_active_mode;
            textBoxTriggerPointStandby.Text = settings.period.data_write.in_standby_mode;
            textBoxTriggerLLSActive.Text = settings.period.fls_read.in_active_mode;
            textBoxTriggerLLSStandby.Text = settings.period.fls_read.in_standby_mode;

            checkBoxCanUse.Checked = settings.can.used == "true" ? true : false;
            checkBoxCanRpmUse.Checked = (int.Parse(settings.can.used_param) & 0x1) == 0x1;
            checkBoxCanFuelLevelPercentUse.Checked = (int.Parse(settings.can.used_param) & 0x2) == 0x2;
            checkBoxCanFuelLevelLiterUse.Checked = (int.Parse(settings.can.used_param) & 0x4) == 0x4;
            checkBoxCanOdometerUse.Checked = (int.Parse(settings.can.used_param) & 0x8) == 0x8;
            checkBoxCanIgnitionUse.Checked = (int.Parse(settings.can.used_param) & 0x10) == 0x10;
            checkBoxCanEngineTempUse.Checked = (int.Parse(settings.can.used_param) & 0x20) == 0x20;
            checkBoxCanCheckOilUse.Checked = (int.Parse(settings.can.used_param) & 0x40) == 0x40;
            checkBoxCanIgnitionGenPacketOnChange.Checked = settings.can.gen_pack_on_ign == "true" ? true : false;

            textBoxCanAlertRpmMax.Text = settings.can.alert.rpm_trh.max;
            textBoxCanAlertRpmDuration.Text = settings.can.alert.rpm_trh.duration;
            textBoxCanAlertTempMax.Text = settings.can.alert.temp_trh.max;
            textBoxCanAlertTempDuration.Text = settings.can.alert.temp_trh.duration;
            checkBoxCanAlertCheckOil.Checked = settings.can.alert.on_check_oil == "true" ? true : false;

            textBoxTriggerCanActive.Text = settings.can.period.in_active_mode;
            textBoxTriggerCanStandby.Text = settings.can.period.in_standby_mode;

            textBoxLLS1.Text = settings.fls_sensor[0].address;
            checkBoxLLS1.Checked = textBoxLLS1.Text != "";
            textBoxLLS2.Text = settings.fls_sensor[1].address;
            checkBoxLLS2.Checked = textBoxLLS2.Text != "";
            textBoxLLS3.Text = settings.fls_sensor[2].address;
            checkBoxLLS3.Checked = textBoxLLS3.Text != "";
            textBoxLLS4.Text = settings.fls_sensor[3].address;
            checkBoxLLS4.Checked = textBoxLLS4.Text != "";
            textBoxLLS5.Text = settings.fls_sensor[4].address;
            checkBoxLLS5.Checked = textBoxLLS5.Text != "";
            textBoxLLS6.Text = settings.fls_sensor[5].address;
            checkBoxLLS6.Checked = textBoxLLS6.Text != "";
            textBoxLLS7.Text = settings.fls_sensor[6].address;
            checkBoxLLS7.Checked = textBoxLLS7.Text != "";
            textBoxLLS8.Text = settings.fls_sensor[7].address;
            checkBoxLLS8.Checked = textBoxLLS8.Text != "";

            textBoxTempID1.Text = settings.ds18b20[0].id;
            textBoxTempID2.Text = settings.ds18b20[1].id;
            textBoxTempID3.Text = settings.ds18b20[2].id;
            textBoxTempID4.Text = settings.ds18b20[3].id;
            textBoxTempID5.Text = settings.ds18b20[4].id;
            textBoxTempID6.Text = settings.ds18b20[5].id;
            textBoxTempID7.Text = settings.ds18b20[6].id;
            textBoxTempID8.Text = settings.ds18b20[7].id;

            checkBoxFilterEnable.Checked = settings.gnss.filter == "on" ? true : false;
            textBoxFilterSpeedMin.Text = settings.gnss.freeze_speed;
            textBoxFilterFreezeTime.Text = settings.gnss.freeze_time;
            textBoxFilterHDOP.Text = settings.gnss.hdop_max;
            textBoxFilterSatellitesMin.Text = settings.gnss.satellites_min;
            textBoxFilterSpeedMax.Text = settings.gnss.max_speed;
            textBoxFilterSpeedAvg.Text = settings.gnss.speed_avg;

            textBoxRssiMin.Text = settings.min_rssi;
            textBoxTriggerPointDegree.Text = settings.untracked_course;
            textBoxTriggerPointMeter.Text = settings.untracked_distance;
            textBoxAccelThreshold.Text = settings.acceleration_threshold;

            textBoxAPNAddress.Text = settings.apn.address;
            textBoxAPNLogin.Text = settings.apn.user;
            textBoxAPNPassword.Text = settings.apn.password;

            textBoxServerIP.Text = settings.main_host.address;
            textBoxServerPort.Text = settings.main_host.port;
            checkBoxWaitResponse.Checked = settings.main_host.wait_response == "true" ? true : false;

            textBoxLogIP.Text = settings.log_host.address;
            textBoxLogPort.Text = settings.log_host.port;

            comboBoxDI1Mode.SelectedIndex = int.Parse(settings.dins[0].mode);
            comboBoxDI1Type.Text = settings.dins[0].type;
            comboBoxDI2Mode.SelectedIndex = int.Parse(settings.dins[1].mode);
            comboBoxDI2Type.Text = settings.dins[1].type;
            comboBoxDI3Mode.SelectedIndex = int.Parse(settings.dins[2].mode);
            comboBoxDI3Type.Text = settings.dins[2].type;
            comboBoxDI4Mode.SelectedIndex = int.Parse(settings.dins[3].mode);
            comboBoxDI4Type.Text = settings.dins[3].type;

            comboBoxDO1Type.Text = settings.douts[0].type;
            comboBoxDO2Type.Text = settings.douts[1].type;

            textBoxAI1Trh.Text = settings.ains[0].threshold;
            textBoxAI2Trh.Text = settings.ains[1].threshold;

            textBoxWhiteNumber1.Text = settings.white_phone_numbers[0];
            textBoxWhiteNumber2.Text = settings.white_phone_numbers[1];
            textBoxWhiteNumber3.Text = settings.white_phone_numbers[2];
            textBoxWhiteNumber4.Text = settings.white_phone_numbers[3];
            textBoxWhiteNumber5.Text = settings.white_phone_numbers[4];
            textBoxWhiteNumber6.Text = settings.white_phone_numbers[5];
            textBoxWhiteNumber7.Text = settings.white_phone_numbers[6];
            textBoxWhiteNumber8.Text = settings.white_phone_numbers[7];
            textBoxWhiteNumber9.Text = settings.white_phone_numbers[8];
            textBoxWhiteNumber10.Text = settings.white_phone_numbers[9];
            textBoxWhiteNumber11.Text = settings.white_phone_numbers[10];
            textBoxWhiteNumber12.Text = settings.white_phone_numbers[11];
            textBoxWhiteNumber13.Text = settings.white_phone_numbers[12];
            textBoxWhiteNumber14.Text = settings.white_phone_numbers[13];
            textBoxWhiteNumber15.Text = settings.white_phone_numbers[14];
            textBoxWhiteNumber16.Text = settings.white_phone_numbers[15];
            textBoxWhiteNumber17.Text = settings.white_phone_numbers[16];
            textBoxWhiteNumber18.Text = settings.white_phone_numbers[17];

            textBoxMasterNumber1.Text = settings.master_phone_numbers[0];
            textBoxMasterNumber2.Text = settings.master_phone_numbers[1];
            textBoxMasterNumber3.Text = settings.master_phone_numbers[2];
            textBoxMasterNumber4.Text = settings.master_phone_numbers[3];
            textBoxMasterNumber5.Text = settings.master_phone_numbers[4];

            checkBoxBluetoothUse.Checked = settings.bluetooth.used == "true" ? true : false;
            checkBoxBluetoothAutoAnswer.Checked = settings.bluetooth.auto_answer == "true" ? true : false;
            textBoxBluetoothMAC.Text = settings.bluetooth.mac;

            checkBoxIDAuto.Checked = settings.auto_id == "true" ? true : false;

            comboBoxLogLevel.Text = settings.log_level;

            checkBoxAutoUpdate.Checked = settings.auto_update == "true" ? true : false;
        }

        private void OnApplicationExit(object sender, FormClosingEventArgs e)
        {
            if (serialPort.IsOpen && terminalConnected)
            {
                // Go back to log mode
                serialPort.Write("x\r");
                Thread.Sleep(200);
                serialPort.Close();
                portOpened = false;
            }
        }

        private void GetAvailablePorts()
        {
            String[] ports = SerialPortStream.GetPortNames();
            comboBoxPort.Text = "COM Port";
            comboBoxPort.Items.Clear();
            comboBoxPort.Items.AddRange(ports);
        }

        private void ComPortConnect()
        {
            if (!serialPort.IsOpen)
            {
                if (comboBoxPort.Text != "COM Port")
                {
                    try
                    {
                        serialPort.PortName = comboBoxPort.Text;
                        serialPort.Parity = Parity.None;
                        serialPort.DataBits = 8;
                        serialPort.StopBits = StopBits.One;
                        serialPort.RtsEnable = true;
                        serialPort.Handshake = Handshake.None;
                        serialPort.BaudRate = 9600;
                        serialPort.WriteTimeout = writeTimeoutDefault;
                        serialPort.ReadTimeout = readTimeoutDefault;
                        serialPort.ReadBufferSize = 4096;
                        serialPort.OpenDirect();

                        timerSerialPort.Start();

                        buttonComPortConnect.Text = "Отключить";
                        pictureBoxConnected.BackColor = Color.Orange;

                        serialPort.Write("x\r");
                        Thread.Sleep(200);
#if DEBUG == false
                        serialPort.Write(textBoxPassword.Text + "\r");
#else
                        serialPort.Write("15975346" + "\r");
#endif
                        string ans = serialPort.ReadTo("Enter a command");

                        pictureBoxConnected.BackColor = Color.LimeGreen;

#if true
                        terminalConnected = true;
                        UiEnable();
#else
                        if (ReadSettings() == 0)
                        {
                            terminalConnected = true;
                            UiEnable();
                        }
                        else
                        {
                            pictureBoxConnected.BackColor = Color.Red;
                        }
#endif
                    }
                    catch /*(Exception ex)*/
                    {
                        pictureBoxConnected.BackColor = Color.Red;
                        //MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
            else
            {
                try
                {
                    if (terminalConnected)
                    {
                        // Go back to log mode
                        serialPort.Write("x\r");
                        Thread.Sleep(200);
                    }

                    serialPort.Close();
                    portOpened = false;
                    buttonComPortConnect.Text = "Подключить";
                    pictureBoxConnected.BackColor = Color.Silver;
                    terminalConnected = false;

                    UiDisable();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void UiEnable()
        {
            buttonTest.Enabled = true;
            buttonWriteToDevice.Enabled = true;
            buttonReadFromDevice.Enabled = true;
        }

        private void UiDisable()
        {
            buttonTest.Enabled = false;
            buttonWriteToDevice.Enabled = false;
            buttonReadFromDevice.Enabled = false;
        }

        private void ButtonComPortConnect_Click(object sender, EventArgs e)
        {
            ComPortConnect();
        }

        private void ButtonComPortRefresh_Click(object sender, EventArgs e)
        {
            GetAvailablePorts();
        }

        private void ComboBoxPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxPort.Text != "COM Port")
            {
                buttonComPortConnect.Enabled = true;
            }
            else
            {
                buttonComPortConnect.Enabled = false;
            }
        }

        private void RadioButtonAPNAuto_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonAPNAuto.Checked)
            {
                textBoxAPNAddress.Enabled = false;
                textBoxAPNAddress.Text = "";
                textBoxAPNLogin.Enabled = false;
                textBoxAPNLogin.Text = "";
                textBoxAPNPassword.Enabled = false;
                textBoxAPNPassword.Text = "";
            }
            else
            {
                textBoxAPNAddress.Enabled = true;
                textBoxAPNLogin.Enabled = true;
                textBoxAPNPassword.Enabled = true;
            }
        }

        private void CheckBoxFilterEnable_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFilterEnable.Checked)
            {
                textBoxFilterFreezeTime.Enabled = true;
                textBoxFilterHDOP.Enabled = true;
                textBoxFilterSpeedAvg.Enabled = true;
                textBoxFilterSpeedMax.Enabled = true;
                textBoxFilterSpeedMin.Enabled = true;
                textBoxFilterSatellitesMin.Enabled = true;
            }
            else
            {
                textBoxFilterFreezeTime.Enabled = false;
                textBoxFilterHDOP.Enabled = false;
                textBoxFilterSpeedAvg.Enabled = false;
                textBoxFilterSpeedMax.Enabled = false;
                textBoxFilterSpeedMin.Enabled = false;
                textBoxFilterSatellitesMin.Enabled = false;
            }
        }

        private void CheckBoxLLS1_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS1.Enabled = checkBoxLLS1.Checked ? true : false;
        }

        private void CheckBoxLLS2_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS2.Enabled = checkBoxLLS2.Checked ? true : false;
        }

        private void CheckBoxLLS3_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS3.Enabled = checkBoxLLS3.Checked ? true : false;
        }

        private void CheckBoxLLS4_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS4.Enabled = checkBoxLLS4.Checked ? true : false;
        }

        private void CheckBoxLLS5_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS5.Enabled = checkBoxLLS5.Checked ? true : false;
        }

        private void CheckBoxLLS6_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS6.Enabled = checkBoxLLS6.Checked ? true : false;
        }

        private void CheckBoxLLS7_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS7.Enabled = checkBoxLLS7.Checked ? true : false;
        }

        private void CheckBoxLLS8_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLLS8.Enabled = checkBoxLLS8.Checked ? true : false;
        }

        private void ComboBoxDI1Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDI1Type.Enabled = comboBoxDI1Mode.SelectedIndex == 0 ? false : true;
        }

        private void ComboBoxDI2Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDI2Type.Enabled = comboBoxDI2Mode.SelectedIndex == 0 ? false : true;
        }

        private void ComboBoxDI3Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDI3Type.Enabled = comboBoxDI3Mode.SelectedIndex == 0 ? false : true;
        }

        private void ComboBoxDI4Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDI4Type.Enabled = comboBoxDI4Mode.SelectedIndex == 0 ? false : true;
        }

        private void CheckBoxTemp1Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID1.Enabled = checkBoxTemp1Enable.Checked ? true : false;
        }

        private void CheckBoxTemp2Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID2.Enabled = checkBoxTemp2Enable.Checked ? true : false;
        }

        private void CheckBoxTemp3Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID3.Enabled = checkBoxTemp3Enable.Checked ? true : false;
        }

        private void CheckBoxTemp4Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID4.Enabled = checkBoxTemp4Enable.Checked ? true : false;
        }

        private void CheckBoxTemp5Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID5.Enabled = checkBoxTemp5Enable.Checked ? true : false;
        }

        private void CheckBoxTemp6Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID6.Enabled = checkBoxTemp6Enable.Checked ? true : false;
        }

        private void CheckBoxTemp7Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID7.Enabled = checkBoxTemp7Enable.Checked ? true : false;
        }

        private void CheckBoxTemp8Enable_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTempID8.Enabled = checkBoxTemp8Enable.Checked ? true : false;
        }

        private void CheckBoxWhiteNumber1_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber1.Enabled = checkBoxWhiteNumber1.Checked ? true : false;
        }

        private void CheckBoxWhiteNumber2_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber2.Enabled = checkBoxWhiteNumber2.Checked ? true : false;
        }

        private void CheckBoxWhiteNumber3_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber3.Enabled = checkBoxWhiteNumber3.Checked ? true : false;
        }

        private void CheckBoxWhiteNumber4_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber4.Enabled = checkBoxWhiteNumber4.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber5_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber5.Enabled = checkBoxWhiteNumber5.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber6_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber6.Enabled = checkBoxWhiteNumber6.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber7_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber7.Enabled = checkBoxWhiteNumber7.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber8_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber8.Enabled = checkBoxWhiteNumber8.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber9_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber9.Enabled = checkBoxWhiteNumber9.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber10_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber10.Enabled = checkBoxWhiteNumber10.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber11_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber11.Enabled = checkBoxWhiteNumber11.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber12_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber12.Enabled = checkBoxWhiteNumber12.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber13_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber13.Enabled = checkBoxWhiteNumber13.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber14_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber14.Enabled = checkBoxWhiteNumber14.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber15_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber15.Enabled = checkBoxWhiteNumber15.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber16_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber16.Enabled = checkBoxWhiteNumber16.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber17_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber17.Enabled = checkBoxWhiteNumber17.Checked ? true : false;
        }
        private void CheckBoxWhiteNumber18_CheckedChanged(object sender, EventArgs e)
        {
            textBoxWhiteNumber18.Enabled = checkBoxWhiteNumber18.Checked ? true : false;
        }
        private void CheckBoxMasterNumber1_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterNumber1.Enabled = checkBoxMasterNumber1.Checked ? true : false;
        }
        private void CheckBoxMasterNumber2_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterNumber2.Enabled = checkBoxMasterNumber2.Checked ? true : false;
        }
        private void CheckBoxMasterNumber3_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterNumber3.Enabled = checkBoxMasterNumber3.Checked ? true : false;
        }
        private void CheckBoxMasterNumber4_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterNumber4.Enabled = checkBoxMasterNumber4.Checked ? true : false;
        }
        private void CheckBoxMasterNumber5_CheckedChanged(object sender, EventArgs e)
        {
            textBoxMasterNumber5.Enabled = checkBoxMasterNumber5.Checked ? true : false;
        }

        private void CheckBoxIDAuto_CheckedChanged(object sender, EventArgs e)
        {
            textBoxID.Enabled = checkBoxIDAuto.Checked ? false : true;
        }

        private static string key = "<RSAKeyValue><Modulus>uH2f1uyRZinAeC+S0DaUB1MEvTXqA87O/1CicmnZU7KC6J0g/1dj0zar6z0La8PnXtj1a6BT4m3f2WqPll/XOGb0qhI3B4pr/QTdzjgAnTxJdoH7bvgKduXr2gEELs8A5EG1Yz1ZMZCqVlq42Nww43eYCsWCzQBI7PlqTsgGhAE=</Modulus><Exponent>AQAB</Exponent><P>9NAUOwIFWp7zB7T5wXuVyAVhz7gvxrr4JOlTave4HmZmllsOniX2lc910uRYuKw7TKSPam9IoA2iEDos+0sbUw==</P><Q>wOvfa4pWRh2jOggSZr7ZmPQW5B0TuwK1LsBq/xeFu6phwUFGaeyn4dfQEgDJBZAbq7hjGyRXTL8JSSTylwDM2w==</Q><DP>kPSoFexzaR4Wvuh8vgDERmRRbyIbP+uzRH/Xgc3d+jod4kdWV3QZFsl5dX75vZdVoWMK/DWaZhLMdBlOSxlEuw==</DP><DQ>G4qQ3r1lC6V3g/VbCyf70IYoLnCoPG5qhz4ZCEnHYb81LW2wK7cJUIiKv8c064JjMOSl4V4SyHYNUU/hAgWvOQ==</DQ><InverseQ>f3dczYbBrHp6njuRFrE4gUeLVDNIu6/7j4TnTeXFlBdqCTR1s7qcqodJlIF1mHUjC+DDCbF2S3pOUkq7FJk+pA==</InverseQ><D>K1oZvT9Jy1/0QkmrjhHFJxNzAaMxokNukMA56/jDgM9pw49ouZendXJbY6Osk14teDQzN18tVv/KoiRQKV3MDKYIVZVnPY5kERm5EOD0CMKIQXO3tG7QDG256xHF7Pkhku2nhZcvRlGSTsPhRB5d4MXk9QXRBh6WDDW3axiNCSE=</D></RSAKeyValue>";

        private void ButtonOpenFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var sr = new StreamReader(openFileDialog1.FileName);
                    string encString = sr.ReadToEnd();

                    Crypter crypter = new Crypter();
                    string jsonSett = crypter.Decrypt(encString);

                    settings = JsonConvert.DeserializeObject<Settings>(jsonSett);
                    MoveSettingsToFields();

                    sr.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void ButtonSaveToFile_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MoveFieldsToSettings();

                string jsonSett = JsonConvert.SerializeObject(settings);

                Crypter crypter = new Crypter();
                string encString = crypter.Encrypt(jsonSett);

                try
                {
                    var sr = new StreamWriter(saveFileDialog1.FileName);
                    sr.Write(encString);
                    sr.Close();

                    MessageBox.Show("Операция успешна!", "LEANConfigurator");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void TextBoxID_TextChanged(object sender, EventArgs e)
        {
            checkBoxIDAuto.Checked = textBoxID.Text == "" ? true : false;
        }

        private void TextBoxLLS1_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS1.Checked = textBoxLLS1.Text == "" ? false : true;
        }

        private void TextBoxLLS2_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS2.Checked = textBoxLLS2.Text == "" ? false : true;
        }

        private void TextBoxLLS3_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS3.Checked = textBoxLLS3.Text == "" ? false : true;
        }

        private void TextBoxLLS4_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS4.Checked = textBoxLLS4.Text == "" ? false : true;
        }

        private void TextBoxLLS5_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS5.Checked = textBoxLLS5.Text == "" ? false : true;
        }

        private void TextBoxLLS6_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS6.Checked = textBoxLLS6.Text == "" ? false : true;
        }

        private void TextBoxLLS7_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS7.Checked = textBoxLLS7.Text == "" ? false : true;
        }

        private void TextBoxLLS8_TextChanged(object sender, EventArgs e)
        {
            checkBoxLLS8.Checked = textBoxLLS8.Text == "" ? false : true;
        }

        private void TextBoxTempID1_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp1Enable.Checked = textBoxTempID1.Text == "" ? false : true;
        }

        private void TextBoxTempID2_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp2Enable.Checked = textBoxTempID2.Text == "" ? false : true;
        }

        private void TextBoxTempID3_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp3Enable.Checked = textBoxTempID3.Text == "" ? false : true;
        }

        private void TextBoxTempID4_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp4Enable.Checked = textBoxTempID4.Text == "" ? false : true;
        }

        private void TextBoxTempID5_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp5Enable.Checked = textBoxTempID5.Text == "" ? false : true;
        }

        private void TextBoxTempID6_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp6Enable.Checked = textBoxTempID6.Text == "" ? false : true;
        }

        private void TextBoxTempID7_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp7Enable.Checked = textBoxTempID7.Text == "" ? false : true;
        }

        private void TextBoxTempID8_TextChanged(object sender, EventArgs e)
        {
            checkBoxTemp8Enable.Checked = textBoxTempID8.Text == "" ? false : true;
        }

        private void TextBoxAPNAddress_TextChanged(object sender, EventArgs e)
        {
            radioButtonAPNAuto.Checked = textBoxAPNAddress.Text == "" ? true : false;
            radioButtonAPNUser.Checked = textBoxAPNAddress.Text == "" ? false : true;
        }

        private void TextBoxAPNLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxAPNPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private int ReadSettings()
        {
            serialPort.Write("s\r");

            string settJson = "";

            try
            {
                serialPort.ReadTo("--------========= Settings: ========---------\r\n");

                while (true)
                {
                    string inchar = char.ConvertFromUtf32(serialPort.ReadChar());
                    if (inchar == "\r")
                    {
                        break;
                    }

                    settJson += inchar;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return -1;
            }

            if (settJson.Length > 0)
            {
                try
                {
                    settings = JsonConvert.DeserializeObject<Settings>(settJson);
                    MoveSettingsToFields();

                    MessageBox.Show("Конфигурация прочитана!", "LEANConfigurator");
                    return 0;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                    return -1;
                }
            }

            return -1;
        }

        private void ButtonReadFromDevice_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen && terminalConnected)
            {
                ReadSettings();
            }
        }

        private void ButtonWriteToDevice_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen && terminalConnected)
            {
                try
                {
                    serialPort.Write("l\r");
                    serialPort.ReadTo("Please send settings file...\r\n");

                    MoveFieldsToSettings();
                    string settJson = JsonConvert.SerializeObject(settings);

                    serialPort.Write(settJson);

                    serialPort.ReadTimeout = 5000;
                    Cursor.Current = Cursors.WaitCursor;
                    serialPort.ReadTo("Settings saved successfully");
                    Cursor.Current = Cursors.Default;
                    serialPort.ReadTimeout = readTimeoutDefault;

                    MessageBox.Show("Операция успешна!", "LEANConfigurator");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
        }

        private void TextBoxWhiteNumber1_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber1.Checked = textBoxWhiteNumber1.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber2_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber2.Checked = textBoxWhiteNumber2.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber3_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber3.Checked = textBoxWhiteNumber3.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber4_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber4.Checked = textBoxWhiteNumber4.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber5_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber5.Checked = textBoxWhiteNumber5.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber6_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber6.Checked = textBoxWhiteNumber6.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber7_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber7.Checked = textBoxWhiteNumber7.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber8_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber8.Checked = textBoxWhiteNumber8.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber9_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber9.Checked = textBoxWhiteNumber9.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber10_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber10.Checked = textBoxWhiteNumber10.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber11_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber11.Checked = textBoxWhiteNumber11.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber12_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber12.Checked = textBoxWhiteNumber12.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber13_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber13.Checked = textBoxWhiteNumber13.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber14_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber14.Checked = textBoxWhiteNumber14.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber15_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber15.Checked = textBoxWhiteNumber15.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber16_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber16.Checked = textBoxWhiteNumber16.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber17_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber17.Checked = textBoxWhiteNumber17.Text == "" ? false : true;
        }

        private void TextBoxWhiteNumber18_TextChanged(object sender, EventArgs e)
        {
            checkBoxWhiteNumber18.Checked = textBoxWhiteNumber18.Text == "" ? false : true;
        }

        private void TextBoxMasterNumber1_TextChanged(object sender, EventArgs e)
        {
            checkBoxMasterNumber1.Checked = textBoxMasterNumber1.Text == "" ? false : true;
        }

        private void TextBoxMasterNumber2_TextChanged(object sender, EventArgs e)
        {
            checkBoxMasterNumber2.Checked = textBoxMasterNumber2.Text == "" ? false : true;
        }

        private void TextBoxMasterNumber3_TextChanged(object sender, EventArgs e)
        {
            checkBoxMasterNumber3.Checked = textBoxMasterNumber3.Text == "" ? false : true;
        }

        private void TextBoxMasterNumber4_TextChanged(object sender, EventArgs e)
        {
            checkBoxMasterNumber4.Checked = textBoxMasterNumber4.Text == "" ? false : true;
        }

        private void TextBoxMasterNumber5_TextChanged(object sender, EventArgs e)
        {
            checkBoxMasterNumber5.Checked = textBoxMasterNumber5.Text == "" ? false : true;
        }

        private bool portOpened = false;

        private void timerSerialPortCallback(object sender, EventArgs e)
        {
            if (!serialPort.IsOpen)
            {
                if (portOpened)
                {
                    serialPort.Close();
                    portOpened = false;

                    buttonComPortConnect.Text = "Подключить";
                    pictureBoxConnected.BackColor = Color.Silver;
                    terminalConnected = false;

                    UiDisable();

                    MessageBox.Show("Устройство отключено!", "LEANConfigurator");
                }
            }
            else
            {
                portOpened = true;
            }
        }

        private void ButtonTest_Click(object sender, EventArgs e)
        {
            Status status = new Status(this);
            status.Show();
        }

        private void ButtonAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\"Сибирские Инновационные Системы\"\r\n" +
                            "http://www.sibinn.ru/\r\n" +
                            "Адрес: \r\n" +
                            "Тел: +7(909)543-44-13\r\n" +
                            "e-mail: \r\n" +
                            "\r\n" + 
                            "Версия ПО: " + leanConfiguratorVersion, "Информация");
        }

        private void CheckBoxBluetoothUse_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxBluetoothUse.Checked)
            {
                checkBoxBluetoothAutoAnswer.Enabled = true;
                textBoxBluetoothMAC.Enabled = true;
            }
            else
            {
                checkBoxBluetoothAutoAnswer.Enabled = false;
                textBoxBluetoothMAC.Enabled = false;
            }
        }

        private void checkBoxHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = true;
                textBoxPasswordSett.UseSystemPasswordChar = true;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = false;
                textBoxPasswordSett.UseSystemPasswordChar = false;
            }
        }

        private void CheckBoxCanUse_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxCanUse.Checked)
            {
                checkBoxCanEngineTempUse.Enabled = true;
                checkBoxCanFuelLevelLiterUse.Enabled = true;
                checkBoxCanFuelLevelPercentUse.Enabled = true;
                checkBoxCanIgnitionUse.Enabled = true;
                checkBoxCanOdometerUse.Enabled = true;
                checkBoxCanCheckOilUse.Enabled = true;
                checkBoxCanRpmUse.Enabled = true;
                checkBoxCanIgnitionGenPacketOnChange.Enabled = checkBoxCanIgnitionUse.Checked ? true : false;
                textBoxCanAlertRpmMax.Enabled = checkBoxCanRpmUse.Checked ? true : false;
                textBoxCanAlertRpmDuration.Enabled = checkBoxCanRpmUse.Checked ? true : false;
                textBoxCanAlertTempMax.Enabled = checkBoxCanEngineTempUse.Checked ? true : false;
                textBoxCanAlertTempDuration.Enabled = checkBoxCanEngineTempUse.Checked ? true : false;
                textBoxCanAlertTempDuration.Enabled = checkBoxCanEngineTempUse.Checked ? true : false;
                checkBoxCanAlertCheckOil.Enabled = checkBoxCanCheckOilUse.Checked ? true :false;
                textBoxTriggerCanActive.Enabled = true;
                textBoxTriggerCanStandby.Enabled = true;

                checkBoxLLS1.Enabled = false;
                checkBoxLLS2.Enabled = false;
                checkBoxLLS3.Enabled = false;
                checkBoxLLS4.Enabled = false;
                checkBoxLLS5.Enabled = false;
                checkBoxLLS6.Enabled = false;
                checkBoxLLS7.Enabled = false;
                checkBoxLLS8.Enabled = false;
                textBoxLLS1.Enabled = false;
                textBoxLLS2.Enabled = false;
                textBoxLLS3.Enabled = false;
                textBoxLLS4.Enabled = false;
                textBoxLLS5.Enabled = false;
                textBoxLLS6.Enabled = false;
                textBoxLLS7.Enabled = false;
                textBoxLLS8.Enabled = false;
                textBoxTriggerLLSActive.Enabled = false;
                textBoxTriggerLLSStandby.Enabled = false;
            }
            else
            {
                checkBoxCanEngineTempUse.Enabled = false;
                checkBoxCanFuelLevelLiterUse.Enabled = false;
                checkBoxCanFuelLevelPercentUse.Enabled = false;
                checkBoxCanIgnitionUse.Enabled = false;
                checkBoxCanOdometerUse.Enabled = false;
                checkBoxCanCheckOilUse.Enabled = false;
                checkBoxCanRpmUse.Enabled = false;
                checkBoxCanIgnitionGenPacketOnChange.Enabled = false;
                textBoxCanAlertRpmMax.Enabled = false;
                textBoxCanAlertRpmDuration.Enabled = false;
                textBoxCanAlertTempMax.Enabled = false;
                textBoxCanAlertTempDuration.Enabled = false;
                checkBoxCanAlertCheckOil.Enabled = false;
                textBoxTriggerCanActive.Enabled = false;
                textBoxTriggerCanStandby.Enabled = false;

                checkBoxLLS1.Enabled = true;
                checkBoxLLS2.Enabled = true;
                checkBoxLLS3.Enabled = true;
                checkBoxLLS4.Enabled = true;
                checkBoxLLS5.Enabled = true;
                checkBoxLLS6.Enabled = true;
                checkBoxLLS7.Enabled = true;
                checkBoxLLS8.Enabled = true;
                textBoxLLS1.Text = settings.fls_sensor[0].address;
                textBoxLLS2.Text = settings.fls_sensor[1].address;
                textBoxLLS3.Text = settings.fls_sensor[2].address;
                textBoxLLS4.Text = settings.fls_sensor[3].address;
                textBoxLLS5.Text = settings.fls_sensor[4].address;
                textBoxLLS6.Text = settings.fls_sensor[5].address;
                textBoxLLS7.Text = settings.fls_sensor[6].address;
                textBoxLLS8.Text = settings.fls_sensor[7].address;
                textBoxTriggerLLSActive.Enabled = true;
                textBoxTriggerLLSStandby.Enabled = true;
            }
        }

        private void CheckBoxCanRpmUse_CheckedChanged(object sender, EventArgs e)
        {
            textBoxCanAlertRpmMax.Enabled = checkBoxCanRpmUse.Checked ? true : false;
            textBoxCanAlertRpmDuration.Enabled = checkBoxCanRpmUse.Checked ? true : false;
        }

        private void CheckBoxCanEngineTempUse_CheckedChanged(object sender, EventArgs e)
        {
            textBoxCanAlertTempMax.Enabled = checkBoxCanEngineTempUse.Checked ? true : false;
            textBoxCanAlertTempDuration.Enabled = checkBoxCanEngineTempUse.Checked ? true : false;
        }

        private void CheckBoxCanOilLevelUse_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxCanAlertCheckOil.Enabled = checkBoxCanCheckOilUse.Checked ? true : false;
        }

        private void CheckBoxCanIgnitionUse_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxCanIgnitionGenPacketOnChange.Enabled = checkBoxCanIgnitionUse.Checked ? true : false;
        }

        private void textBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode ==Keys.Enter)
            {
                if (buttonComPortConnect.Enabled)
                {
                    ComPortConnect();
                }
            }
        }
    }
}
