﻿using System.IO.Ports;
using System;

namespace LEANConfigurator
{
    partial class Configurator
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxPort = new System.Windows.Forms.ComboBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.comboBoxLogLevel = new System.Windows.Forms.ComboBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBoxIDAuto = new System.Windows.Forms.CheckBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.groupBoxServerLog = new System.Windows.Forms.GroupBox();
            this.textBoxLogPort = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxLogingEnable = new System.Windows.Forms.CheckBox();
            this.textBoxLogIP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBoxServerTelematic = new System.Windows.Forms.GroupBox();
            this.textBoxServerPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxWaitResponse = new System.Windows.Forms.CheckBox();
            this.textBoxServerIP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxProtocolType = new System.Windows.Forms.ComboBox();
            this.groupBoxNetwork = new System.Windows.Forms.GroupBox();
            this.textBoxRssiMin = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.radioButtonAPNUser = new System.Windows.Forms.RadioButton();
            this.radioButtonAPNAuto = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAPNPassword = new System.Windows.Forms.TextBox();
            this.textBoxAPNLogin = new System.Windows.Forms.TextBox();
            this.textBoxAPNAddress = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.textBoxAccelThreshold = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxFilterEnable = new System.Windows.Forms.CheckBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxFilterSatellitesMin = new System.Windows.Forms.TextBox();
            this.textBoxFilterFreezeTime = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxFilterSpeedMin = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBoxFilterHDOP = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxFilterSpeedAvg = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxFilterSpeedMax = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBoxTriggers = new System.Windows.Forms.GroupBox();
            this.textBoxTriggerPointMeter = new System.Windows.Forms.TextBox();
            this.textBoxTriggerPointDegree = new System.Windows.Forms.TextBox();
            this.textBoxTriggerPointStandby = new System.Windows.Forms.TextBox();
            this.textBoxTriggerPointActive = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.checkBoxCanAlertCheckOil = new System.Windows.Forms.CheckBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.textBoxCanAlertTempDuration = new System.Windows.Forms.TextBox();
            this.textBoxCanAlertRpmDuration = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.textBoxCanAlertTempMax = new System.Windows.Forms.TextBox();
            this.textBoxCanAlertRpmMax = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.checkBoxCanUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanCheckOilUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanRpmUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanEngineTempUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanFuelLevelLiterUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanIgnitionGenPacketOnChange = new System.Windows.Forms.CheckBox();
            this.checkBoxCanIgnitionUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanFuelLevelPercentUse = new System.Windows.Forms.CheckBox();
            this.checkBoxCanOdometerUse = new System.Windows.Forms.CheckBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBoxTriggerCanStandby = new System.Windows.Forms.TextBox();
            this.textBoxTriggerCanActive = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxTriggerLLSStandby = new System.Windows.Forms.TextBox();
            this.textBoxTriggerLLSActive = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.checkBoxLLS1 = new System.Windows.Forms.CheckBox();
            this.textBoxLLS1 = new System.Windows.Forms.TextBox();
            this.textBoxLLS2 = new System.Windows.Forms.TextBox();
            this.textBoxLLS3 = new System.Windows.Forms.TextBox();
            this.textBoxLLS4 = new System.Windows.Forms.TextBox();
            this.textBoxLLS5 = new System.Windows.Forms.TextBox();
            this.textBoxLLS6 = new System.Windows.Forms.TextBox();
            this.textBoxLLS7 = new System.Windows.Forms.TextBox();
            this.textBoxLLS8 = new System.Windows.Forms.TextBox();
            this.checkBoxLLS2 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS3 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS4 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS5 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS6 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS7 = new System.Windows.Forms.CheckBox();
            this.checkBoxLLS8 = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxAI1Trh = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxAI2Trh = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.comboBoxDO1Type = new System.Windows.Forms.ComboBox();
            this.comboBoxDO2Type = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.comboBoxDI1Mode = new System.Windows.Forms.ComboBox();
            this.comboBoxDI2Mode = new System.Windows.Forms.ComboBox();
            this.comboBoxDI3Mode = new System.Windows.Forms.ComboBox();
            this.comboBoxDI4Mode = new System.Windows.Forms.ComboBox();
            this.comboBoxDI1Type = new System.Windows.Forms.ComboBox();
            this.comboBoxDI2Type = new System.Windows.Forms.ComboBox();
            this.comboBoxDI3Type = new System.Windows.Forms.ComboBox();
            this.comboBoxDI4Type = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label53 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.checkBoxTemp1Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp4Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp5Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp6Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp7Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp8Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp2Enable = new System.Windows.Forms.CheckBox();
            this.checkBoxTemp3Enable = new System.Windows.Forms.CheckBox();
            this.textBoxTempID1 = new System.Windows.Forms.TextBox();
            this.textBoxTempID2 = new System.Windows.Forms.TextBox();
            this.textBoxTempID3 = new System.Windows.Forms.TextBox();
            this.textBoxTempID4 = new System.Windows.Forms.TextBox();
            this.textBoxTempID5 = new System.Windows.Forms.TextBox();
            this.textBoxTempID6 = new System.Windows.Forms.TextBox();
            this.textBoxTempID7 = new System.Windows.Forms.TextBox();
            this.textBoxTempID8 = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxBluetoothMAC = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxBluetoothAutoAnswer = new System.Windows.Forms.CheckBox();
            this.checkBoxBluetoothUse = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.checkBoxAutoUpdate = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPasswordSett = new System.Windows.Forms.TextBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.textBoxWhiteNumber18 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber18 = new System.Windows.Forms.CheckBox();
            this.textBoxWhiteNumber17 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber17 = new System.Windows.Forms.CheckBox();
            this.textBoxWhiteNumber16 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber16 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber15 = new System.Windows.Forms.CheckBox();
            this.textBoxWhiteNumber15 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber14 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber13 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber13 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber14 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textBoxWhiteNumber12 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber11 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber10 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber9 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber8 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber7 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber12 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber11 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber10 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber9 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber7 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber8 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.checkBoxWhiteNumber1 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber4 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber2 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber3 = new System.Windows.Forms.CheckBox();
            this.textBoxWhiteNumber1 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber3 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber4 = new System.Windows.Forms.TextBox();
            this.checkBoxWhiteNumber5 = new System.Windows.Forms.CheckBox();
            this.checkBoxWhiteNumber6 = new System.Windows.Forms.CheckBox();
            this.textBoxWhiteNumber5 = new System.Windows.Forms.TextBox();
            this.textBoxWhiteNumber6 = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.checkBoxMasterNumber1 = new System.Windows.Forms.CheckBox();
            this.checkBoxMasterNumber4 = new System.Windows.Forms.CheckBox();
            this.checkBoxMasterNumber2 = new System.Windows.Forms.CheckBox();
            this.checkBoxMasterNumber3 = new System.Windows.Forms.CheckBox();
            this.textBoxMasterNumber1 = new System.Windows.Forms.TextBox();
            this.textBoxMasterNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxMasterNumber3 = new System.Windows.Forms.TextBox();
            this.textBoxMasterNumber4 = new System.Windows.Forms.TextBox();
            this.checkBoxMasterNumber5 = new System.Windows.Forms.CheckBox();
            this.textBoxMasterNumber5 = new System.Windows.Forms.TextBox();
            this.buttonComPortConnect = new System.Windows.Forms.Button();
            this.pictureBoxConnected = new System.Windows.Forms.PictureBox();
            this.buttonTest = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonReadFromDevice = new System.Windows.Forms.Button();
            this.buttonWriteToDevice = new System.Windows.Forms.Button();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timerSerialPort = new System.Windows.Forms.Timer(this.components);
            this.buttonComPortRefresh = new System.Windows.Forms.Button();
            this.checkBoxHidePassword = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBoxServerLog.SuspendLayout();
            this.groupBoxServerTelematic.SuspendLayout();
            this.groupBoxNetwork.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxTriggers.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConnected)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxPort
            // 
            this.comboBoxPort.FormattingEnabled = true;
            this.comboBoxPort.Location = new System.Drawing.Point(8, 13);
            this.comboBoxPort.Name = "comboBoxPort";
            this.comboBoxPort.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPort.TabIndex = 2;
            this.comboBoxPort.Text = "COM Port";
            this.comboBoxPort.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPort_SelectedIndexChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage8);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage7);
            this.tabControl.Controls.Add(this.tabPage6);
            this.tabControl.Controls.Add(this.tabPage9);
            this.tabControl.Location = new System.Drawing.Point(8, 72);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(868, 319);
            this.tabControl.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.groupBoxServerLog);
            this.tabPage1.Controls.Add(this.groupBoxServerTelematic);
            this.tabPage1.Controls.Add(this.groupBoxNetwork);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(860, 293);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Система";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.comboBoxLogLevel);
            this.groupBox12.Controls.Add(this.label48);
            this.groupBox12.Location = new System.Drawing.Point(435, 90);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(419, 61);
            this.groupBox12.TabIndex = 15;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Уровень логирования";
            // 
            // comboBoxLogLevel
            // 
            this.comboBoxLogLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxLogLevel.FormattingEnabled = true;
            this.comboBoxLogLevel.Items.AddRange(new object[] {
            "None",
            "Fatal",
            "Error",
            "Warning",
            "Info",
            "Debug",
            "Verbose"});
            this.comboBoxLogLevel.Location = new System.Drawing.Point(138, 23);
            this.comboBoxLogLevel.Name = "comboBoxLogLevel";
            this.comboBoxLogLevel.Size = new System.Drawing.Size(93, 21);
            this.comboBoxLogLevel.TabIndex = 15;
            this.comboBoxLogLevel.Text = "Error";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(10, 26);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(122, 13);
            this.label48.TabIndex = 14;
            this.label48.Text = "Уровень логирования:";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBoxIDAuto);
            this.groupBox10.Controls.Add(this.textBoxID);
            this.groupBox10.Controls.Add(this.label61);
            this.groupBox10.Location = new System.Drawing.Point(435, 7);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(419, 77);
            this.groupBox10.TabIndex = 12;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Идентификатор";
            // 
            // checkBoxIDAuto
            // 
            this.checkBoxIDAuto.AutoSize = true;
            this.checkBoxIDAuto.Checked = true;
            this.checkBoxIDAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIDAuto.Location = new System.Drawing.Point(13, 21);
            this.checkBoxIDAuto.Name = "checkBoxIDAuto";
            this.checkBoxIDAuto.Size = new System.Drawing.Size(173, 17);
            this.checkBoxIDAuto.TabIndex = 10;
            this.checkBoxIDAuto.Text = "Присваивать автоматически";
            this.checkBoxIDAuto.UseVisualStyleBackColor = true;
            this.checkBoxIDAuto.CheckedChanged += new System.EventHandler(this.CheckBoxIDAuto_CheckedChanged);
            // 
            // textBoxID
            // 
            this.textBoxID.Enabled = false;
            this.textBoxID.Location = new System.Drawing.Point(99, 47);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(87, 20);
            this.textBoxID.TabIndex = 13;
            this.textBoxID.TextChanged += new System.EventHandler(this.TextBoxID_TextChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(10, 50);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(90, 13);
            this.label61.TabIndex = 14;
            this.label61.Text = "Идентификатор:";
            // 
            // groupBoxServerLog
            // 
            this.groupBoxServerLog.Controls.Add(this.textBoxLogPort);
            this.groupBoxServerLog.Controls.Add(this.label6);
            this.groupBoxServerLog.Controls.Add(this.checkBoxLogingEnable);
            this.groupBoxServerLog.Controls.Add(this.textBoxLogIP);
            this.groupBoxServerLog.Controls.Add(this.label14);
            this.groupBoxServerLog.Location = new System.Drawing.Point(437, 157);
            this.groupBoxServerLog.Name = "groupBoxServerLog";
            this.groupBoxServerLog.Size = new System.Drawing.Size(417, 107);
            this.groupBoxServerLog.TabIndex = 11;
            this.groupBoxServerLog.TabStop = false;
            this.groupBoxServerLog.Text = "Сервер логирования";
            this.groupBoxServerLog.Visible = false;
            // 
            // textBoxLogPort
            // 
            this.textBoxLogPort.Location = new System.Drawing.Point(98, 72);
            this.textBoxLogPort.Name = "textBoxLogPort";
            this.textBoxLogPort.Size = new System.Drawing.Size(65, 20);
            this.textBoxLogPort.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Порт:";
            // 
            // checkBoxLogingEnable
            // 
            this.checkBoxLogingEnable.AutoSize = true;
            this.checkBoxLogingEnable.Checked = true;
            this.checkBoxLogingEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLogingEnable.Enabled = false;
            this.checkBoxLogingEnable.Location = new System.Drawing.Point(13, 21);
            this.checkBoxLogingEnable.Name = "checkBoxLogingEnable";
            this.checkBoxLogingEnable.Size = new System.Drawing.Size(143, 17);
            this.checkBoxLogingEnable.TabIndex = 10;
            this.checkBoxLogingEnable.Text = "Включить логирование";
            this.checkBoxLogingEnable.UseVisualStyleBackColor = true;
            // 
            // textBoxLogIP
            // 
            this.textBoxLogIP.Location = new System.Drawing.Point(99, 45);
            this.textBoxLogIP.Name = "textBoxLogIP";
            this.textBoxLogIP.Size = new System.Drawing.Size(145, 20);
            this.textBoxLogIP.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Адрес сервера:";
            // 
            // groupBoxServerTelematic
            // 
            this.groupBoxServerTelematic.Controls.Add(this.textBoxServerPort);
            this.groupBoxServerTelematic.Controls.Add(this.label5);
            this.groupBoxServerTelematic.Controls.Add(this.checkBoxWaitResponse);
            this.groupBoxServerTelematic.Controls.Add(this.textBoxServerIP);
            this.groupBoxServerTelematic.Controls.Add(this.label4);
            this.groupBoxServerTelematic.Controls.Add(this.label7);
            this.groupBoxServerTelematic.Controls.Add(this.comboBoxProtocolType);
            this.groupBoxServerTelematic.Enabled = false;
            this.groupBoxServerTelematic.Location = new System.Drawing.Point(6, 157);
            this.groupBoxServerTelematic.Name = "groupBoxServerTelematic";
            this.groupBoxServerTelematic.Size = new System.Drawing.Size(423, 107);
            this.groupBoxServerTelematic.TabIndex = 9;
            this.groupBoxServerTelematic.TabStop = false;
            this.groupBoxServerTelematic.Text = "Сервер телематики";
            this.groupBoxServerTelematic.Visible = false;
            // 
            // textBoxServerPort
            // 
            this.textBoxServerPort.Location = new System.Drawing.Point(99, 78);
            this.textBoxServerPort.Name = "textBoxServerPort";
            this.textBoxServerPort.Size = new System.Drawing.Size(65, 20);
            this.textBoxServerPort.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Порт:";
            // 
            // checkBoxWaitResponse
            // 
            this.checkBoxWaitResponse.AutoSize = true;
            this.checkBoxWaitResponse.Location = new System.Drawing.Point(242, 25);
            this.checkBoxWaitResponse.Name = "checkBoxWaitResponse";
            this.checkBoxWaitResponse.Size = new System.Drawing.Size(91, 17);
            this.checkBoxWaitResponse.TabIndex = 10;
            this.checkBoxWaitResponse.Text = "Ждать ответ";
            this.checkBoxWaitResponse.UseVisualStyleBackColor = true;
            // 
            // textBoxServerIP
            // 
            this.textBoxServerIP.Location = new System.Drawing.Point(99, 52);
            this.textBoxServerIP.Name = "textBoxServerIP";
            this.textBoxServerIP.Size = new System.Drawing.Size(146, 20);
            this.textBoxServerIP.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Адрес сервера:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Протокол:";
            // 
            // comboBoxProtocolType
            // 
            this.comboBoxProtocolType.FormattingEnabled = true;
            this.comboBoxProtocolType.Items.AddRange(new object[] {
            "EGTS",
            "Wialon IPS 2.0"});
            this.comboBoxProtocolType.Location = new System.Drawing.Point(75, 23);
            this.comboBoxProtocolType.Name = "comboBoxProtocolType";
            this.comboBoxProtocolType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxProtocolType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProtocolType.TabIndex = 0;
            this.comboBoxProtocolType.Text = "EGTS";
            // 
            // groupBoxNetwork
            // 
            this.groupBoxNetwork.Controls.Add(this.textBoxRssiMin);
            this.groupBoxNetwork.Controls.Add(this.label63);
            this.groupBoxNetwork.Controls.Add(this.radioButtonAPNUser);
            this.groupBoxNetwork.Controls.Add(this.radioButtonAPNAuto);
            this.groupBoxNetwork.Controls.Add(this.label3);
            this.groupBoxNetwork.Controls.Add(this.label2);
            this.groupBoxNetwork.Controls.Add(this.label1);
            this.groupBoxNetwork.Controls.Add(this.textBoxAPNPassword);
            this.groupBoxNetwork.Controls.Add(this.textBoxAPNLogin);
            this.groupBoxNetwork.Controls.Add(this.textBoxAPNAddress);
            this.groupBoxNetwork.Location = new System.Drawing.Point(7, 7);
            this.groupBoxNetwork.Name = "groupBoxNetwork";
            this.groupBoxNetwork.Size = new System.Drawing.Size(422, 144);
            this.groupBoxNetwork.TabIndex = 0;
            this.groupBoxNetwork.TabStop = false;
            this.groupBoxNetwork.Text = "Настройки оператора";
            // 
            // textBoxRssiMin
            // 
            this.textBoxRssiMin.Location = new System.Drawing.Point(227, 113);
            this.textBoxRssiMin.Name = "textBoxRssiMin";
            this.textBoxRssiMin.Size = new System.Drawing.Size(87, 20);
            this.textBoxRssiMin.TabIndex = 15;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(10, 116);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(214, 13);
            this.label63.TabIndex = 16;
            this.label63.Text = "Минимальный уровень сигнала сети (%):";
            // 
            // radioButtonAPNUser
            // 
            this.radioButtonAPNUser.AutoSize = true;
            this.radioButtonAPNUser.Location = new System.Drawing.Point(13, 42);
            this.radioButtonAPNUser.Name = "radioButtonAPNUser";
            this.radioButtonAPNUser.Size = new System.Drawing.Size(147, 17);
            this.radioButtonAPNUser.TabIndex = 10;
            this.radioButtonAPNUser.Text = "Пользовательский APN";
            this.radioButtonAPNUser.UseVisualStyleBackColor = true;
            // 
            // radioButtonAPNAuto
            // 
            this.radioButtonAPNAuto.AutoSize = true;
            this.radioButtonAPNAuto.Checked = true;
            this.radioButtonAPNAuto.Location = new System.Drawing.Point(13, 19);
            this.radioButtonAPNAuto.Name = "radioButtonAPNAuto";
            this.radioButtonAPNAuto.Size = new System.Drawing.Size(358, 17);
            this.radioButtonAPNAuto.TabIndex = 9;
            this.radioButtonAPNAuto.TabStop = true;
            this.radioButtonAPNAuto.Text = "Автоматическая настройка APN (Мегафон, ТЕЛЕ2, Билайн, МТС)";
            this.radioButtonAPNAuto.UseVisualStyleBackColor = true;
            this.radioButtonAPNAuto.CheckedChanged += new System.EventHandler(this.RadioButtonAPNAuto_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Пароль:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Логин";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Адрес";
            // 
            // textBoxAPNPassword
            // 
            this.textBoxAPNPassword.Enabled = false;
            this.textBoxAPNPassword.Location = new System.Drawing.Point(266, 80);
            this.textBoxAPNPassword.Name = "textBoxAPNPassword";
            this.textBoxAPNPassword.Size = new System.Drawing.Size(91, 20);
            this.textBoxAPNPassword.TabIndex = 7;
            this.textBoxAPNPassword.TextChanged += new System.EventHandler(this.TextBoxAPNPassword_TextChanged);
            // 
            // textBoxAPNLogin
            // 
            this.textBoxAPNLogin.Enabled = false;
            this.textBoxAPNLogin.Location = new System.Drawing.Point(169, 80);
            this.textBoxAPNLogin.Name = "textBoxAPNLogin";
            this.textBoxAPNLogin.Size = new System.Drawing.Size(91, 20);
            this.textBoxAPNLogin.TabIndex = 7;
            this.textBoxAPNLogin.TextChanged += new System.EventHandler(this.TextBoxAPNLogin_TextChanged);
            // 
            // textBoxAPNAddress
            // 
            this.textBoxAPNAddress.Enabled = false;
            this.textBoxAPNAddress.Location = new System.Drawing.Point(12, 80);
            this.textBoxAPNAddress.Name = "textBoxAPNAddress";
            this.textBoxAPNAddress.Size = new System.Drawing.Size(151, 20);
            this.textBoxAPNAddress.TabIndex = 7;
            this.textBoxAPNAddress.TextChanged += new System.EventHandler(this.TextBoxAPNAddress_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.groupBoxTriggers);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(860, 293);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Трек";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label10);
            this.groupBox11.Controls.Add(this.label9);
            this.groupBox11.Controls.Add(this.label8);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.textBoxAccelThreshold);
            this.groupBox11.Location = new System.Drawing.Point(6, 211);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(422, 76);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Чувствительность к вибрации";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(274, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "10 - мало чувствителен";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(276, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "1 - очень чувствителен";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Диапазон от 1 до 10:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(10, 29);
            this.label64.MaximumSize = new System.Drawing.Size(220, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(201, 26);
            this.label64.TabIndex = 0;
            this.label64.Text = "Коэффициент чувствительности к вибрации для определения движения:";
            // 
            // textBoxAccelThreshold
            // 
            this.textBoxAccelThreshold.Location = new System.Drawing.Point(213, 35);
            this.textBoxAccelThreshold.Name = "textBoxAccelThreshold";
            this.textBoxAccelThreshold.Size = new System.Drawing.Size(43, 20);
            this.textBoxAccelThreshold.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxFilterEnable);
            this.groupBox1.Controls.Add(this.label62);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.textBoxFilterSatellitesMin);
            this.groupBox1.Controls.Add(this.textBoxFilterFreezeTime);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.textBoxFilterSpeedMin);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.textBoxFilterHDOP);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.textBoxFilterSpeedAvg);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.textBoxFilterSpeedMax);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Location = new System.Drawing.Point(434, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 199);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Парметры фильтрации точек трека";
            // 
            // checkBoxFilterEnable
            // 
            this.checkBoxFilterEnable.AutoSize = true;
            this.checkBoxFilterEnable.Location = new System.Drawing.Point(13, 19);
            this.checkBoxFilterEnable.Name = "checkBoxFilterEnable";
            this.checkBoxFilterEnable.Size = new System.Drawing.Size(115, 17);
            this.checkBoxFilterEnable.TabIndex = 0;
            this.checkBoxFilterEnable.Text = "Включить фильтр";
            this.checkBoxFilterEnable.UseVisualStyleBackColor = true;
            this.checkBoxFilterEnable.CheckedChanged += new System.EventHandler(this.CheckBoxFilterEnable_CheckedChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(10, 175);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(197, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "Минимальное количество спутников:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(10, 149);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(201, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Окно времени для фиксации стоянки:";
            // 
            // textBoxFilterSatellitesMin
            // 
            this.textBoxFilterSatellitesMin.Enabled = false;
            this.textBoxFilterSatellitesMin.Location = new System.Drawing.Point(213, 172);
            this.textBoxFilterSatellitesMin.Name = "textBoxFilterSatellitesMin";
            this.textBoxFilterSatellitesMin.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterSatellitesMin.TabIndex = 1;
            // 
            // textBoxFilterFreezeTime
            // 
            this.textBoxFilterFreezeTime.Enabled = false;
            this.textBoxFilterFreezeTime.Location = new System.Drawing.Point(213, 146);
            this.textBoxFilterFreezeTime.Name = "textBoxFilterFreezeTime";
            this.textBoxFilterFreezeTime.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterFreezeTime.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(10, 100);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(131, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Минимальная скорость:";
            // 
            // textBoxFilterSpeedMin
            // 
            this.textBoxFilterSpeedMin.Enabled = false;
            this.textBoxFilterSpeedMin.Location = new System.Drawing.Point(213, 97);
            this.textBoxFilterSpeedMin.Name = "textBoxFilterSpeedMin";
            this.textBoxFilterSpeedMin.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterSpeedMin.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(10, 50);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(177, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Максимально допустимый HDOP";
            // 
            // textBoxFilterHDOP
            // 
            this.textBoxFilterHDOP.Enabled = false;
            this.textBoxFilterHDOP.Location = new System.Drawing.Point(213, 47);
            this.textBoxFilterHDOP.Name = "textBoxFilterHDOP";
            this.textBoxFilterHDOP.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterHDOP.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(262, 149);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "сек.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(10, 124);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(122, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Усреднение скорости:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(262, 100);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(31, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "км/ч";
            // 
            // textBoxFilterSpeedAvg
            // 
            this.textBoxFilterSpeedAvg.Enabled = false;
            this.textBoxFilterSpeedAvg.Location = new System.Drawing.Point(213, 121);
            this.textBoxFilterSpeedAvg.Name = "textBoxFilterSpeedAvg";
            this.textBoxFilterSpeedAvg.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterSpeedAvg.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 75);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(137, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Максимальная скорость:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(262, 124);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(28, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "сек.";
            // 
            // textBoxFilterSpeedMax
            // 
            this.textBoxFilterSpeedMax.Enabled = false;
            this.textBoxFilterSpeedMax.Location = new System.Drawing.Point(213, 72);
            this.textBoxFilterSpeedMax.Name = "textBoxFilterSpeedMax";
            this.textBoxFilterSpeedMax.Size = new System.Drawing.Size(43, 20);
            this.textBoxFilterSpeedMax.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(262, 75);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "км/ч";
            // 
            // groupBoxTriggers
            // 
            this.groupBoxTriggers.Controls.Add(this.textBoxTriggerPointMeter);
            this.groupBoxTriggers.Controls.Add(this.textBoxTriggerPointDegree);
            this.groupBoxTriggers.Controls.Add(this.textBoxTriggerPointStandby);
            this.groupBoxTriggers.Controls.Add(this.textBoxTriggerPointActive);
            this.groupBoxTriggers.Controls.Add(this.label17);
            this.groupBoxTriggers.Controls.Add(this.label23);
            this.groupBoxTriggers.Controls.Add(this.label21);
            this.groupBoxTriggers.Controls.Add(this.label19);
            this.groupBoxTriggers.Controls.Add(this.label18);
            this.groupBoxTriggers.Controls.Add(this.label16);
            this.groupBoxTriggers.Controls.Add(this.label22);
            this.groupBoxTriggers.Controls.Add(this.label20);
            this.groupBoxTriggers.Controls.Add(this.label15);
            this.groupBoxTriggers.Location = new System.Drawing.Point(6, 6);
            this.groupBoxTriggers.Name = "groupBoxTriggers";
            this.groupBoxTriggers.Size = new System.Drawing.Size(422, 199);
            this.groupBoxTriggers.TabIndex = 0;
            this.groupBoxTriggers.TabStop = false;
            this.groupBoxTriggers.Text = "Триггеры записи точек";
            // 
            // textBoxTriggerPointMeter
            // 
            this.textBoxTriggerPointMeter.Location = new System.Drawing.Point(149, 125);
            this.textBoxTriggerPointMeter.Name = "textBoxTriggerPointMeter";
            this.textBoxTriggerPointMeter.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerPointMeter.TabIndex = 1;
            // 
            // textBoxTriggerPointDegree
            // 
            this.textBoxTriggerPointDegree.Location = new System.Drawing.Point(149, 95);
            this.textBoxTriggerPointDegree.Name = "textBoxTriggerPointDegree";
            this.textBoxTriggerPointDegree.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerPointDegree.TabIndex = 1;
            // 
            // textBoxTriggerPointStandby
            // 
            this.textBoxTriggerPointStandby.Location = new System.Drawing.Point(149, 64);
            this.textBoxTriggerPointStandby.Name = "textBoxTriggerPointStandby";
            this.textBoxTriggerPointStandby.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerPointStandby.TabIndex = 1;
            // 
            // textBoxTriggerPointActive
            // 
            this.textBoxTriggerPointActive.Location = new System.Drawing.Point(149, 41);
            this.textBoxTriggerPointActive.Name = "textBoxTriggerPointActive";
            this.textBoxTriggerPointActive.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerPointActive.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 67);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "В режиме ожидания:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(198, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "метров";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(198, 98);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "град.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(198, 67);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "сек.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(198, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "сек.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(31, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "В активном режиме:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "По пройденному пути:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(132, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "По изменению курса на:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "По времени";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox15);
            this.tabPage8.Controls.Add(this.groupBox14);
            this.tabPage8.Controls.Add(this.groupBox13);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(860, 293);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "CAN";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.checkBoxCanAlertCheckOil);
            this.groupBox15.Controls.Add(this.label78);
            this.groupBox15.Controls.Add(this.label73);
            this.groupBox15.Controls.Add(this.textBoxCanAlertTempDuration);
            this.groupBox15.Controls.Add(this.textBoxCanAlertRpmDuration);
            this.groupBox15.Controls.Add(this.label77);
            this.groupBox15.Controls.Add(this.label70);
            this.groupBox15.Controls.Add(this.textBoxCanAlertTempMax);
            this.groupBox15.Controls.Add(this.textBoxCanAlertRpmMax);
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this.label75);
            this.groupBox15.Controls.Add(this.label69);
            this.groupBox15.Controls.Add(this.label74);
            this.groupBox15.Controls.Add(this.label72);
            this.groupBox15.Controls.Add(this.label71);
            this.groupBox15.Controls.Add(this.label68);
            this.groupBox15.Location = new System.Drawing.Point(274, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(286, 287);
            this.groupBox15.TabIndex = 10;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Алерты";
            // 
            // checkBoxCanAlertCheckOil
            // 
            this.checkBoxCanAlertCheckOil.AutoSize = true;
            this.checkBoxCanAlertCheckOil.Enabled = false;
            this.checkBoxCanAlertCheckOil.Location = new System.Drawing.Point(30, 211);
            this.checkBoxCanAlertCheckOil.Name = "checkBoxCanAlertCheckOil";
            this.checkBoxCanAlertCheckOil.Size = new System.Drawing.Size(209, 17);
            this.checkBoxCanAlertCheckOil.TabIndex = 3;
            this.checkBoxCanAlertCheckOil.Text = "Сработал индикатор уровеня масла";
            this.checkBoxCanAlertCheckOil.UseVisualStyleBackColor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(43, 176);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(60, 13);
            this.label78.TabIndex = 7;
            this.label78.Text = "В течение:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(43, 97);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(60, 13);
            this.label73.TabIndex = 7;
            this.label73.Text = "В течение:";
            // 
            // textBoxCanAlertTempDuration
            // 
            this.textBoxCanAlertTempDuration.Enabled = false;
            this.textBoxCanAlertTempDuration.Location = new System.Drawing.Point(117, 172);
            this.textBoxCanAlertTempDuration.Name = "textBoxCanAlertTempDuration";
            this.textBoxCanAlertTempDuration.Size = new System.Drawing.Size(43, 20);
            this.textBoxCanAlertTempDuration.TabIndex = 8;
            // 
            // textBoxCanAlertRpmDuration
            // 
            this.textBoxCanAlertRpmDuration.Enabled = false;
            this.textBoxCanAlertRpmDuration.Location = new System.Drawing.Point(117, 93);
            this.textBoxCanAlertRpmDuration.Name = "textBoxCanAlertRpmDuration";
            this.textBoxCanAlertRpmDuration.Size = new System.Drawing.Size(43, 20);
            this.textBoxCanAlertRpmDuration.TabIndex = 8;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(43, 154);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(69, 13);
            this.label77.TabIndex = 7;
            this.label77.Text = "Превышает:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(43, 75);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(71, 13);
            this.label70.TabIndex = 7;
            this.label70.Text = "Превышают:";
            // 
            // textBoxCanAlertTempMax
            // 
            this.textBoxCanAlertTempMax.Enabled = false;
            this.textBoxCanAlertTempMax.Location = new System.Drawing.Point(117, 150);
            this.textBoxCanAlertTempMax.Name = "textBoxCanAlertTempMax";
            this.textBoxCanAlertTempMax.Size = new System.Drawing.Size(43, 20);
            this.textBoxCanAlertTempMax.TabIndex = 8;
            // 
            // textBoxCanAlertRpmMax
            // 
            this.textBoxCanAlertRpmMax.Enabled = false;
            this.textBoxCanAlertRpmMax.Location = new System.Drawing.Point(117, 71);
            this.textBoxCanAlertRpmMax.Name = "textBoxCanAlertRpmMax";
            this.textBoxCanAlertRpmMax.Size = new System.Drawing.Size(43, 20);
            this.textBoxCanAlertRpmMax.TabIndex = 8;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(27, 131);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(129, 13);
            this.label76.TabIndex = 7;
            this.label76.Text = "Температура двигателя";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(166, 175);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(28, 13);
            this.label75.TabIndex = 4;
            this.label75.Text = "сек.";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(27, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(107, 13);
            this.label69.TabIndex = 7;
            this.label69.Text = "Обороты двигателя";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(166, 153);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(21, 13);
            this.label74.TabIndex = 4;
            this.label74.Text = "гр.";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(166, 96);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 13);
            this.label72.TabIndex = 4;
            this.label72.Text = "сек.";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(166, 74);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(50, 13);
            this.label71.TabIndex = 4;
            this.label71.Text = "об./мин.";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 24);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(140, 13);
            this.label68.TabIndex = 6;
            this.label68.Text = "Генерировать алерт, если";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.checkBoxCanUse);
            this.groupBox14.Controls.Add(this.checkBoxCanCheckOilUse);
            this.groupBox14.Controls.Add(this.checkBoxCanRpmUse);
            this.groupBox14.Controls.Add(this.checkBoxCanEngineTempUse);
            this.groupBox14.Controls.Add(this.checkBoxCanFuelLevelLiterUse);
            this.groupBox14.Controls.Add(this.checkBoxCanIgnitionGenPacketOnChange);
            this.groupBox14.Controls.Add(this.checkBoxCanIgnitionUse);
            this.groupBox14.Controls.Add(this.checkBoxCanFuelLevelPercentUse);
            this.groupBox14.Controls.Add(this.checkBoxCanOdometerUse);
            this.groupBox14.Location = new System.Drawing.Point(3, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(265, 287);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Используемые параметры CAN";
            // 
            // checkBoxCanUse
            // 
            this.checkBoxCanUse.Location = new System.Drawing.Point(8, 19);
            this.checkBoxCanUse.Name = "checkBoxCanUse";
            this.checkBoxCanUse.Size = new System.Drawing.Size(175, 30);
            this.checkBoxCanUse.TabIndex = 2;
            this.checkBoxCanUse.Text = "Использовать CAN           (ДУТ будут недоступны)";
            this.checkBoxCanUse.UseVisualStyleBackColor = true;
            this.checkBoxCanUse.CheckedChanged += new System.EventHandler(this.CheckBoxCanUse_CheckedChanged);
            // 
            // checkBoxCanCheckOilUse
            // 
            this.checkBoxCanCheckOilUse.AutoSize = true;
            this.checkBoxCanCheckOilUse.Enabled = false;
            this.checkBoxCanCheckOilUse.Location = new System.Drawing.Point(23, 216);
            this.checkBoxCanCheckOilUse.Name = "checkBoxCanCheckOilUse";
            this.checkBoxCanCheckOilUse.Size = new System.Drawing.Size(105, 17);
            this.checkBoxCanCheckOilUse.TabIndex = 2;
            this.checkBoxCanCheckOilUse.Text = "Уровень масла";
            this.checkBoxCanCheckOilUse.UseVisualStyleBackColor = true;
            this.checkBoxCanCheckOilUse.CheckedChanged += new System.EventHandler(this.CheckBoxCanOilLevelUse_CheckedChanged);
            // 
            // checkBoxCanRpmUse
            // 
            this.checkBoxCanRpmUse.AutoSize = true;
            this.checkBoxCanRpmUse.Enabled = false;
            this.checkBoxCanRpmUse.Location = new System.Drawing.Point(23, 55);
            this.checkBoxCanRpmUse.Name = "checkBoxCanRpmUse";
            this.checkBoxCanRpmUse.Size = new System.Drawing.Size(126, 17);
            this.checkBoxCanRpmUse.TabIndex = 2;
            this.checkBoxCanRpmUse.Text = "Обороты двигателя";
            this.checkBoxCanRpmUse.UseVisualStyleBackColor = true;
            this.checkBoxCanRpmUse.CheckedChanged += new System.EventHandler(this.CheckBoxCanRpmUse_CheckedChanged);
            // 
            // checkBoxCanEngineTempUse
            // 
            this.checkBoxCanEngineTempUse.AutoSize = true;
            this.checkBoxCanEngineTempUse.Enabled = false;
            this.checkBoxCanEngineTempUse.Location = new System.Drawing.Point(23, 193);
            this.checkBoxCanEngineTempUse.Name = "checkBoxCanEngineTempUse";
            this.checkBoxCanEngineTempUse.Size = new System.Drawing.Size(148, 17);
            this.checkBoxCanEngineTempUse.TabIndex = 2;
            this.checkBoxCanEngineTempUse.Text = "Температура двигателя";
            this.checkBoxCanEngineTempUse.UseVisualStyleBackColor = true;
            this.checkBoxCanEngineTempUse.CheckedChanged += new System.EventHandler(this.CheckBoxCanEngineTempUse_CheckedChanged);
            // 
            // checkBoxCanFuelLevelLiterUse
            // 
            this.checkBoxCanFuelLevelLiterUse.AutoSize = true;
            this.checkBoxCanFuelLevelLiterUse.Enabled = false;
            this.checkBoxCanFuelLevelLiterUse.Location = new System.Drawing.Point(23, 78);
            this.checkBoxCanFuelLevelLiterUse.Name = "checkBoxCanFuelLevelLiterUse";
            this.checkBoxCanFuelLevelLiterUse.Size = new System.Drawing.Size(160, 17);
            this.checkBoxCanFuelLevelLiterUse.TabIndex = 2;
            this.checkBoxCanFuelLevelLiterUse.Text = "Уровень топлива в литрах";
            this.checkBoxCanFuelLevelLiterUse.UseVisualStyleBackColor = true;
            // 
            // checkBoxCanIgnitionGenPacketOnChange
            // 
            this.checkBoxCanIgnitionGenPacketOnChange.AutoSize = true;
            this.checkBoxCanIgnitionGenPacketOnChange.Enabled = false;
            this.checkBoxCanIgnitionGenPacketOnChange.Location = new System.Drawing.Point(41, 170);
            this.checkBoxCanIgnitionGenPacketOnChange.Name = "checkBoxCanIgnitionGenPacketOnChange";
            this.checkBoxCanIgnitionGenPacketOnChange.Size = new System.Drawing.Size(209, 17);
            this.checkBoxCanIgnitionGenPacketOnChange.TabIndex = 2;
            this.checkBoxCanIgnitionGenPacketOnChange.Text = "Генерировать пакет при изменении";
            this.checkBoxCanIgnitionGenPacketOnChange.UseVisualStyleBackColor = true;
            // 
            // checkBoxCanIgnitionUse
            // 
            this.checkBoxCanIgnitionUse.AutoSize = true;
            this.checkBoxCanIgnitionUse.Enabled = false;
            this.checkBoxCanIgnitionUse.Location = new System.Drawing.Point(23, 147);
            this.checkBoxCanIgnitionUse.Name = "checkBoxCanIgnitionUse";
            this.checkBoxCanIgnitionUse.Size = new System.Drawing.Size(82, 17);
            this.checkBoxCanIgnitionUse.TabIndex = 2;
            this.checkBoxCanIgnitionUse.Text = "Зажигание";
            this.checkBoxCanIgnitionUse.UseVisualStyleBackColor = true;
            this.checkBoxCanIgnitionUse.CheckedChanged += new System.EventHandler(this.CheckBoxCanIgnitionUse_CheckedChanged);
            // 
            // checkBoxCanFuelLevelPercentUse
            // 
            this.checkBoxCanFuelLevelPercentUse.AutoSize = true;
            this.checkBoxCanFuelLevelPercentUse.Enabled = false;
            this.checkBoxCanFuelLevelPercentUse.Location = new System.Drawing.Point(23, 101);
            this.checkBoxCanFuelLevelPercentUse.Name = "checkBoxCanFuelLevelPercentUse";
            this.checkBoxCanFuelLevelPercentUse.Size = new System.Drawing.Size(178, 17);
            this.checkBoxCanFuelLevelPercentUse.TabIndex = 2;
            this.checkBoxCanFuelLevelPercentUse.Text = "Уровень топлива в процентах";
            this.checkBoxCanFuelLevelPercentUse.UseVisualStyleBackColor = true;
            // 
            // checkBoxCanOdometerUse
            // 
            this.checkBoxCanOdometerUse.AutoSize = true;
            this.checkBoxCanOdometerUse.Enabled = false;
            this.checkBoxCanOdometerUse.Location = new System.Drawing.Point(23, 124);
            this.checkBoxCanOdometerUse.Name = "checkBoxCanOdometerUse";
            this.checkBoxCanOdometerUse.Size = new System.Drawing.Size(63, 17);
            this.checkBoxCanOdometerUse.TabIndex = 2;
            this.checkBoxCanOdometerUse.Text = "Пробег";
            this.checkBoxCanOdometerUse.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBoxTriggerCanStandby);
            this.groupBox13.Controls.Add(this.textBoxTriggerCanActive);
            this.groupBox13.Controls.Add(this.label54);
            this.groupBox13.Controls.Add(this.label55);
            this.groupBox13.Controls.Add(this.label65);
            this.groupBox13.Controls.Add(this.label66);
            this.groupBox13.Controls.Add(this.label67);
            this.groupBox13.Location = new System.Drawing.Point(566, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(291, 287);
            this.groupBox13.TabIndex = 1;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Триггеры записи параметров CAN";
            // 
            // textBoxTriggerCanStandby
            // 
            this.textBoxTriggerCanStandby.Enabled = false;
            this.textBoxTriggerCanStandby.Location = new System.Drawing.Point(149, 68);
            this.textBoxTriggerCanStandby.Name = "textBoxTriggerCanStandby";
            this.textBoxTriggerCanStandby.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerCanStandby.TabIndex = 7;
            // 
            // textBoxTriggerCanActive
            // 
            this.textBoxTriggerCanActive.Enabled = false;
            this.textBoxTriggerCanActive.Location = new System.Drawing.Point(149, 45);
            this.textBoxTriggerCanActive.Name = "textBoxTriggerCanActive";
            this.textBoxTriggerCanActive.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerCanActive.TabIndex = 8;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(31, 71);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(113, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "В режиме ожидания:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(198, 71);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 3;
            this.label55.Text = "сек.";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(198, 48);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(28, 13);
            this.label65.TabIndex = 4;
            this.label65.Text = "сек.";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(31, 48);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(112, 13);
            this.label66.TabIndex = 5;
            this.label66.Text = "В активном режиме:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(7, 24);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(68, 13);
            this.label67.TabIndex = 6;
            this.label67.Text = "По времени";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(860, 293);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ДУТ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxTriggerLLSStandby);
            this.groupBox3.Controls.Add(this.textBoxTriggerLLSActive);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Location = new System.Drawing.Point(431, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(422, 273);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Триггеры записи уровня топлива";
            // 
            // textBoxTriggerLLSStandby
            // 
            this.textBoxTriggerLLSStandby.Location = new System.Drawing.Point(149, 68);
            this.textBoxTriggerLLSStandby.Name = "textBoxTriggerLLSStandby";
            this.textBoxTriggerLLSStandby.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerLLSStandby.TabIndex = 7;
            // 
            // textBoxTriggerLLSActive
            // 
            this.textBoxTriggerLLSActive.Location = new System.Drawing.Point(149, 45);
            this.textBoxTriggerLLSActive.Name = "textBoxTriggerLLSActive";
            this.textBoxTriggerLLSActive.Size = new System.Drawing.Size(43, 20);
            this.textBoxTriggerLLSActive.TabIndex = 8;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(31, 71);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(113, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "В режиме ожидания:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(198, 71);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "сек.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(198, 48);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 4;
            this.label37.Text = "сек.";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(31, 48);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(112, 13);
            this.label38.TabIndex = 5;
            this.label38.Text = "В активном режиме:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 24);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(68, 13);
            this.label39.TabIndex = 6;
            this.label39.Text = "По времени";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(422, 273);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Установленные ДУТ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS7, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.textBoxLLS8, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxLLS8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label34, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(104, 245);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // checkBoxLLS1
            // 
            this.checkBoxLLS1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS1.AutoSize = true;
            this.checkBoxLLS1.Location = new System.Drawing.Point(10, 32);
            this.checkBoxLLS1.Name = "checkBoxLLS1";
            this.checkBoxLLS1.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS1.TabIndex = 0;
            this.checkBoxLLS1.Text = "1";
            this.checkBoxLLS1.UseVisualStyleBackColor = true;
            this.checkBoxLLS1.CheckedChanged += new System.EventHandler(this.CheckBoxLLS1_CheckedChanged);
            // 
            // textBoxLLS1
            // 
            this.textBoxLLS1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS1.Enabled = false;
            this.textBoxLLS1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS1.Location = new System.Drawing.Point(55, 30);
            this.textBoxLLS1.Name = "textBoxLLS1";
            this.textBoxLLS1.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS1.TabIndex = 2;
            this.textBoxLLS1.TextChanged += new System.EventHandler(this.TextBoxLLS1_TextChanged);
            // 
            // textBoxLLS2
            // 
            this.textBoxLLS2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS2.Enabled = false;
            this.textBoxLLS2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS2.Location = new System.Drawing.Point(55, 57);
            this.textBoxLLS2.Name = "textBoxLLS2";
            this.textBoxLLS2.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS2.TabIndex = 2;
            this.textBoxLLS2.TextChanged += new System.EventHandler(this.TextBoxLLS2_TextChanged);
            // 
            // textBoxLLS3
            // 
            this.textBoxLLS3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS3.Enabled = false;
            this.textBoxLLS3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS3.Location = new System.Drawing.Point(55, 84);
            this.textBoxLLS3.Name = "textBoxLLS3";
            this.textBoxLLS3.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS3.TabIndex = 2;
            this.textBoxLLS3.TextChanged += new System.EventHandler(this.TextBoxLLS3_TextChanged);
            // 
            // textBoxLLS4
            // 
            this.textBoxLLS4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS4.Enabled = false;
            this.textBoxLLS4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS4.Location = new System.Drawing.Point(55, 111);
            this.textBoxLLS4.Name = "textBoxLLS4";
            this.textBoxLLS4.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS4.TabIndex = 2;
            this.textBoxLLS4.TextChanged += new System.EventHandler(this.TextBoxLLS4_TextChanged);
            // 
            // textBoxLLS5
            // 
            this.textBoxLLS5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS5.Enabled = false;
            this.textBoxLLS5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS5.Location = new System.Drawing.Point(55, 138);
            this.textBoxLLS5.Name = "textBoxLLS5";
            this.textBoxLLS5.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS5.TabIndex = 2;
            this.textBoxLLS5.TextChanged += new System.EventHandler(this.TextBoxLLS5_TextChanged);
            // 
            // textBoxLLS6
            // 
            this.textBoxLLS6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS6.Enabled = false;
            this.textBoxLLS6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS6.Location = new System.Drawing.Point(55, 165);
            this.textBoxLLS6.Name = "textBoxLLS6";
            this.textBoxLLS6.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS6.TabIndex = 2;
            this.textBoxLLS6.TextChanged += new System.EventHandler(this.TextBoxLLS6_TextChanged);
            // 
            // textBoxLLS7
            // 
            this.textBoxLLS7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS7.Enabled = false;
            this.textBoxLLS7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS7.Location = new System.Drawing.Point(55, 192);
            this.textBoxLLS7.Name = "textBoxLLS7";
            this.textBoxLLS7.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS7.TabIndex = 2;
            this.textBoxLLS7.TextChanged += new System.EventHandler(this.TextBoxLLS7_TextChanged);
            // 
            // textBoxLLS8
            // 
            this.textBoxLLS8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxLLS8.Enabled = false;
            this.textBoxLLS8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxLLS8.Location = new System.Drawing.Point(55, 220);
            this.textBoxLLS8.Name = "textBoxLLS8";
            this.textBoxLLS8.Size = new System.Drawing.Size(46, 20);
            this.textBoxLLS8.TabIndex = 2;
            this.textBoxLLS8.TextChanged += new System.EventHandler(this.TextBoxLLS8_TextChanged);
            // 
            // checkBoxLLS2
            // 
            this.checkBoxLLS2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS2.AutoSize = true;
            this.checkBoxLLS2.Location = new System.Drawing.Point(10, 59);
            this.checkBoxLLS2.Name = "checkBoxLLS2";
            this.checkBoxLLS2.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS2.TabIndex = 0;
            this.checkBoxLLS2.Text = "2";
            this.checkBoxLLS2.UseVisualStyleBackColor = true;
            this.checkBoxLLS2.CheckedChanged += new System.EventHandler(this.CheckBoxLLS2_CheckedChanged);
            // 
            // checkBoxLLS3
            // 
            this.checkBoxLLS3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS3.AutoSize = true;
            this.checkBoxLLS3.Location = new System.Drawing.Point(10, 86);
            this.checkBoxLLS3.Name = "checkBoxLLS3";
            this.checkBoxLLS3.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS3.TabIndex = 0;
            this.checkBoxLLS3.Text = "3";
            this.checkBoxLLS3.UseVisualStyleBackColor = true;
            this.checkBoxLLS3.CheckedChanged += new System.EventHandler(this.CheckBoxLLS3_CheckedChanged);
            // 
            // checkBoxLLS4
            // 
            this.checkBoxLLS4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS4.AutoSize = true;
            this.checkBoxLLS4.Location = new System.Drawing.Point(10, 113);
            this.checkBoxLLS4.Name = "checkBoxLLS4";
            this.checkBoxLLS4.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS4.TabIndex = 0;
            this.checkBoxLLS4.Text = "4";
            this.checkBoxLLS4.UseVisualStyleBackColor = true;
            this.checkBoxLLS4.CheckedChanged += new System.EventHandler(this.CheckBoxLLS4_CheckedChanged);
            // 
            // checkBoxLLS5
            // 
            this.checkBoxLLS5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS5.AutoSize = true;
            this.checkBoxLLS5.Location = new System.Drawing.Point(10, 140);
            this.checkBoxLLS5.Name = "checkBoxLLS5";
            this.checkBoxLLS5.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS5.TabIndex = 0;
            this.checkBoxLLS5.Text = "5";
            this.checkBoxLLS5.UseVisualStyleBackColor = true;
            this.checkBoxLLS5.CheckedChanged += new System.EventHandler(this.CheckBoxLLS5_CheckedChanged);
            // 
            // checkBoxLLS6
            // 
            this.checkBoxLLS6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS6.AutoSize = true;
            this.checkBoxLLS6.Location = new System.Drawing.Point(10, 167);
            this.checkBoxLLS6.Name = "checkBoxLLS6";
            this.checkBoxLLS6.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS6.TabIndex = 0;
            this.checkBoxLLS6.Text = "6";
            this.checkBoxLLS6.UseVisualStyleBackColor = true;
            this.checkBoxLLS6.CheckedChanged += new System.EventHandler(this.CheckBoxLLS6_CheckedChanged);
            // 
            // checkBoxLLS7
            // 
            this.checkBoxLLS7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS7.AutoSize = true;
            this.checkBoxLLS7.Location = new System.Drawing.Point(10, 194);
            this.checkBoxLLS7.Name = "checkBoxLLS7";
            this.checkBoxLLS7.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS7.TabIndex = 0;
            this.checkBoxLLS7.Text = "7";
            this.checkBoxLLS7.UseVisualStyleBackColor = true;
            this.checkBoxLLS7.CheckedChanged += new System.EventHandler(this.CheckBoxLLS7_CheckedChanged);
            // 
            // checkBoxLLS8
            // 
            this.checkBoxLLS8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkBoxLLS8.AutoSize = true;
            this.checkBoxLLS8.Location = new System.Drawing.Point(10, 222);
            this.checkBoxLLS8.Name = "checkBoxLLS8";
            this.checkBoxLLS8.Size = new System.Drawing.Size(32, 17);
            this.checkBoxLLS8.TabIndex = 0;
            this.checkBoxLLS8.Text = "8";
            this.checkBoxLLS8.UseVisualStyleBackColor = true;
            this.checkBoxLLS8.CheckedChanged += new System.EventHandler(this.CheckBoxLLS8_CheckedChanged);
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Номер";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(59, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 3;
            this.label34.Text = "Адрес";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(860, 293);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Входы/Выходы";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.panel1);
            this.groupBox5.Controls.Add(this.tableLayoutPanel3);
            this.groupBox5.Location = new System.Drawing.Point(431, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(423, 202);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Дискретные выходы";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label60);
            this.panel1.Controls.Add(this.label59);
            this.panel1.Location = new System.Drawing.Point(6, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(413, 80);
            this.panel1.TabIndex = 1;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 43);
            this.label60.MaximumSize = new System.Drawing.Size(280, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(278, 26);
            this.label60.TabIndex = 2;
            this.label60.Text = "Дискретный выход переходит в активное состояние при превышении порога на аналогов" +
    "ом входе";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(3, 7);
            this.label59.MaximumSize = new System.Drawing.Size(220, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(216, 26);
            this.label59.TabIndex = 1;
            this.label59.Text = "Состояние дискретного выхода зависит от соответствующего аналогового входа";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.03846F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.96154F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.textBoxAI1Trh, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label47, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label49, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxAI2Trh, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.label52, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label50, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label51, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxDO1Type, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.comboBoxDO2Type, 1, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(215, 91);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // textBoxAI1Trh
            // 
            this.textBoxAI1Trh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAI1Trh.Location = new System.Drawing.Point(124, 35);
            this.textBoxAI1Trh.Name = "textBoxAI1Trh";
            this.textBoxAI1Trh.Size = new System.Drawing.Size(51, 20);
            this.textBoxAI1Trh.TabIndex = 8;
            // 
            // label47
            // 
            this.label47.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 8);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(14, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "№";
            // 
            // label49
            // 
            this.label49.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(30, 2);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(44, 26);
            this.label49.TabIndex = 0;
            this.label49.Text = "Тип выхода";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxAI2Trh
            // 
            this.textBoxAI2Trh.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxAI2Trh.Location = new System.Drawing.Point(124, 65);
            this.textBoxAI2Trh.Name = "textBoxAI2Trh";
            this.textBoxAI2Trh.Size = new System.Drawing.Size(51, 20);
            this.textBoxAI2Trh.TabIndex = 8;
            // 
            // label52
            // 
            this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(87, 2);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(125, 26);
            this.label52.TabIndex = 0;
            this.label52.Text = "Напряжение срабатывания, В";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 38);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "1";
            // 
            // label51
            // 
            this.label51.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(3, 69);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(13, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "2";
            // 
            // comboBoxDO1Type
            // 
            this.comboBoxDO1Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDO1Type.FormattingEnabled = true;
            this.comboBoxDO1Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDO1Type.Location = new System.Drawing.Point(23, 34);
            this.comboBoxDO1Type.Name = "comboBoxDO1Type";
            this.comboBoxDO1Type.Size = new System.Drawing.Size(58, 21);
            this.comboBoxDO1Type.TabIndex = 1;
            this.comboBoxDO1Type.Text = "NO";
            // 
            // comboBoxDO2Type
            // 
            this.comboBoxDO2Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDO2Type.FormattingEnabled = true;
            this.comboBoxDO2Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDO2Type.Location = new System.Drawing.Point(23, 65);
            this.comboBoxDO2Type.Name = "comboBoxDO2Type";
            this.comboBoxDO2Type.Size = new System.Drawing.Size(58, 21);
            this.comboBoxDO2Type.TabIndex = 1;
            this.comboBoxDO2Type.Text = "NO";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel2);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(422, 202);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Дискретные входы";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.30534F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.15789F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.75439F));
            this.tableLayoutPanel2.Controls.Add(this.label40, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label41, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label42, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label43, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label44, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label45, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label46, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI1Mode, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI2Mode, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI3Mode, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI4Mode, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI1Type, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI2Type, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI3Type, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxDI4Type, 2, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(228, 156);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label40
            // 
            this.label40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 9);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "№";
            // 
            // label41
            // 
            this.label41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(68, 9);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Функция";
            // 
            // label42
            // 
            this.label42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(179, 2);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(36, 26);
            this.label42.TabIndex = 0;
            this.label42.Text = "Тип входа";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(5, 40);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(13, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "1";
            // 
            // label44
            // 
            this.label44.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(5, 71);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(13, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "2";
            // 
            // label45
            // 
            this.label45.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(5, 102);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(13, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "3";
            // 
            // label46
            // 
            this.label46.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(5, 133);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(13, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "4";
            // 
            // comboBoxDI1Mode
            // 
            this.comboBoxDI1Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI1Mode.FormattingEnabled = true;
            this.comboBoxDI1Mode.Items.AddRange(new object[] {
            "Выкл",
            "Счетный",
            "Зажигание",
            "SOS",
            "Триггер записи точки"});
            this.comboBoxDI1Mode.Location = new System.Drawing.Point(26, 36);
            this.comboBoxDI1Mode.Name = "comboBoxDI1Mode";
            this.comboBoxDI1Mode.Size = new System.Drawing.Size(137, 21);
            this.comboBoxDI1Mode.TabIndex = 1;
            this.comboBoxDI1Mode.Text = "Выкл";
            this.comboBoxDI1Mode.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDI1Mode_SelectedIndexChanged);
            // 
            // comboBoxDI2Mode
            // 
            this.comboBoxDI2Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI2Mode.FormattingEnabled = true;
            this.comboBoxDI2Mode.Items.AddRange(new object[] {
            "Выкл",
            "Счетный",
            "Зажигание",
            "SOS",
            "Триггер записи точки"});
            this.comboBoxDI2Mode.Location = new System.Drawing.Point(26, 67);
            this.comboBoxDI2Mode.Name = "comboBoxDI2Mode";
            this.comboBoxDI2Mode.Size = new System.Drawing.Size(137, 21);
            this.comboBoxDI2Mode.TabIndex = 1;
            this.comboBoxDI2Mode.Text = "Выкл";
            this.comboBoxDI2Mode.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDI2Mode_SelectedIndexChanged);
            // 
            // comboBoxDI3Mode
            // 
            this.comboBoxDI3Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI3Mode.FormattingEnabled = true;
            this.comboBoxDI3Mode.Items.AddRange(new object[] {
            "Выкл",
            "Счетный",
            "Зажигание",
            "SOS",
            "Триггер записи точки"});
            this.comboBoxDI3Mode.Location = new System.Drawing.Point(26, 98);
            this.comboBoxDI3Mode.Name = "comboBoxDI3Mode";
            this.comboBoxDI3Mode.Size = new System.Drawing.Size(137, 21);
            this.comboBoxDI3Mode.TabIndex = 1;
            this.comboBoxDI3Mode.Text = "Выкл";
            this.comboBoxDI3Mode.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDI3Mode_SelectedIndexChanged);
            // 
            // comboBoxDI4Mode
            // 
            this.comboBoxDI4Mode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI4Mode.FormattingEnabled = true;
            this.comboBoxDI4Mode.Items.AddRange(new object[] {
            "Выкл",
            "Счетный",
            "Зажигание",
            "SOS",
            "Триггер записи точки"});
            this.comboBoxDI4Mode.Location = new System.Drawing.Point(26, 129);
            this.comboBoxDI4Mode.Name = "comboBoxDI4Mode";
            this.comboBoxDI4Mode.Size = new System.Drawing.Size(137, 21);
            this.comboBoxDI4Mode.TabIndex = 1;
            this.comboBoxDI4Mode.Text = "Выкл";
            this.comboBoxDI4Mode.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDI4Mode_SelectedIndexChanged);
            // 
            // comboBoxDI1Type
            // 
            this.comboBoxDI1Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI1Type.Enabled = false;
            this.comboBoxDI1Type.FormattingEnabled = true;
            this.comboBoxDI1Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDI1Type.Location = new System.Drawing.Point(169, 36);
            this.comboBoxDI1Type.Name = "comboBoxDI1Type";
            this.comboBoxDI1Type.Size = new System.Drawing.Size(56, 21);
            this.comboBoxDI1Type.TabIndex = 1;
            this.comboBoxDI1Type.Text = "NO";
            // 
            // comboBoxDI2Type
            // 
            this.comboBoxDI2Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI2Type.Enabled = false;
            this.comboBoxDI2Type.FormattingEnabled = true;
            this.comboBoxDI2Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDI2Type.Location = new System.Drawing.Point(169, 67);
            this.comboBoxDI2Type.Name = "comboBoxDI2Type";
            this.comboBoxDI2Type.Size = new System.Drawing.Size(56, 21);
            this.comboBoxDI2Type.TabIndex = 1;
            this.comboBoxDI2Type.Text = "NO";
            // 
            // comboBoxDI3Type
            // 
            this.comboBoxDI3Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI3Type.Enabled = false;
            this.comboBoxDI3Type.FormattingEnabled = true;
            this.comboBoxDI3Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDI3Type.Location = new System.Drawing.Point(169, 98);
            this.comboBoxDI3Type.Name = "comboBoxDI3Type";
            this.comboBoxDI3Type.Size = new System.Drawing.Size(56, 21);
            this.comboBoxDI3Type.TabIndex = 1;
            this.comboBoxDI3Type.Text = "NO";
            // 
            // comboBoxDI4Type
            // 
            this.comboBoxDI4Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDI4Type.Enabled = false;
            this.comboBoxDI4Type.FormattingEnabled = true;
            this.comboBoxDI4Type.Items.AddRange(new object[] {
            "NO",
            "NC"});
            this.comboBoxDI4Type.Location = new System.Drawing.Point(169, 129);
            this.comboBoxDI4Type.Name = "comboBoxDI4Type";
            this.comboBoxDI4Type.Size = new System.Drawing.Size(56, 21);
            this.comboBoxDI4Type.TabIndex = 1;
            this.comboBoxDI4Type.Text = "NO";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox7);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(860, 293);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "1-wire";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tableLayoutPanel5);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(850, 286);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Датчики температуры DS18B20";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.88304F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.11696F));
            this.tableLayoutPanel5.Controls.Add(this.label53, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label56, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp1Enable, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp4Enable, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp5Enable, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp6Enable, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp7Enable, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp8Enable, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp2Enable, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.checkBoxTemp3Enable, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID1, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID2, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID3, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID4, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID5, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID6, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID7, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.textBoxTempID8, 1, 8);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(11, 19);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 9;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.59484F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.54867F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.54867F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.54867F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.54867F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.54867F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.55394F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.55394F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.55394F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(193, 256);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label53
            // 
            this.label53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(10, 13);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(18, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "№";
            // 
            // label56
            // 
            this.label56.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(42, 6);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(146, 26);
            this.label56.TabIndex = 0;
            this.label56.Text = "Адрес (XX-XX-XX-XX-XX-XX-XX-XX)";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxTemp1Enable
            // 
            this.checkBoxTemp1Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp1Enable.AutoSize = true;
            this.checkBoxTemp1Enable.Location = new System.Drawing.Point(3, 44);
            this.checkBoxTemp1Enable.Name = "checkBoxTemp1Enable";
            this.checkBoxTemp1Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp1Enable.TabIndex = 1;
            this.checkBoxTemp1Enable.Text = "1";
            this.checkBoxTemp1Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp1Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp1Enable_CheckedChanged);
            // 
            // checkBoxTemp4Enable
            // 
            this.checkBoxTemp4Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp4Enable.AutoSize = true;
            this.checkBoxTemp4Enable.Location = new System.Drawing.Point(3, 125);
            this.checkBoxTemp4Enable.Name = "checkBoxTemp4Enable";
            this.checkBoxTemp4Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp4Enable.TabIndex = 1;
            this.checkBoxTemp4Enable.Text = "4";
            this.checkBoxTemp4Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp4Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp4Enable_CheckedChanged);
            // 
            // checkBoxTemp5Enable
            // 
            this.checkBoxTemp5Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp5Enable.AutoSize = true;
            this.checkBoxTemp5Enable.Location = new System.Drawing.Point(3, 152);
            this.checkBoxTemp5Enable.Name = "checkBoxTemp5Enable";
            this.checkBoxTemp5Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp5Enable.TabIndex = 1;
            this.checkBoxTemp5Enable.Text = "5";
            this.checkBoxTemp5Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp5Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp5Enable_CheckedChanged);
            // 
            // checkBoxTemp6Enable
            // 
            this.checkBoxTemp6Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp6Enable.AutoSize = true;
            this.checkBoxTemp6Enable.Location = new System.Drawing.Point(3, 179);
            this.checkBoxTemp6Enable.Name = "checkBoxTemp6Enable";
            this.checkBoxTemp6Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp6Enable.TabIndex = 1;
            this.checkBoxTemp6Enable.Text = "6";
            this.checkBoxTemp6Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp6Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp6Enable_CheckedChanged);
            // 
            // checkBoxTemp7Enable
            // 
            this.checkBoxTemp7Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp7Enable.AutoSize = true;
            this.checkBoxTemp7Enable.Location = new System.Drawing.Point(3, 206);
            this.checkBoxTemp7Enable.Name = "checkBoxTemp7Enable";
            this.checkBoxTemp7Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp7Enable.TabIndex = 1;
            this.checkBoxTemp7Enable.Text = "7";
            this.checkBoxTemp7Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp7Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp7Enable_CheckedChanged);
            // 
            // checkBoxTemp8Enable
            // 
            this.checkBoxTemp8Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp8Enable.AutoSize = true;
            this.checkBoxTemp8Enable.Location = new System.Drawing.Point(3, 233);
            this.checkBoxTemp8Enable.Name = "checkBoxTemp8Enable";
            this.checkBoxTemp8Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp8Enable.TabIndex = 1;
            this.checkBoxTemp8Enable.Text = "8";
            this.checkBoxTemp8Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp8Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp8Enable_CheckedChanged);
            // 
            // checkBoxTemp2Enable
            // 
            this.checkBoxTemp2Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp2Enable.AutoSize = true;
            this.checkBoxTemp2Enable.Location = new System.Drawing.Point(3, 71);
            this.checkBoxTemp2Enable.Name = "checkBoxTemp2Enable";
            this.checkBoxTemp2Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp2Enable.TabIndex = 1;
            this.checkBoxTemp2Enable.Text = "2";
            this.checkBoxTemp2Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp2Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp2Enable_CheckedChanged);
            // 
            // checkBoxTemp3Enable
            // 
            this.checkBoxTemp3Enable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxTemp3Enable.AutoSize = true;
            this.checkBoxTemp3Enable.Location = new System.Drawing.Point(3, 98);
            this.checkBoxTemp3Enable.Name = "checkBoxTemp3Enable";
            this.checkBoxTemp3Enable.Size = new System.Drawing.Size(32, 17);
            this.checkBoxTemp3Enable.TabIndex = 1;
            this.checkBoxTemp3Enable.Text = "3";
            this.checkBoxTemp3Enable.UseVisualStyleBackColor = true;
            this.checkBoxTemp3Enable.CheckedChanged += new System.EventHandler(this.CheckBoxTemp3Enable_CheckedChanged);
            // 
            // textBoxTempID1
            // 
            this.textBoxTempID1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID1.Enabled = false;
            this.textBoxTempID1.Location = new System.Drawing.Point(41, 42);
            this.textBoxTempID1.Name = "textBoxTempID1";
            this.textBoxTempID1.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID1.TabIndex = 2;
            this.textBoxTempID1.TextChanged += new System.EventHandler(this.TextBoxTempID1_TextChanged);
            // 
            // textBoxTempID2
            // 
            this.textBoxTempID2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID2.Enabled = false;
            this.textBoxTempID2.Location = new System.Drawing.Point(41, 69);
            this.textBoxTempID2.Name = "textBoxTempID2";
            this.textBoxTempID2.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID2.TabIndex = 2;
            this.textBoxTempID2.TextChanged += new System.EventHandler(this.TextBoxTempID2_TextChanged);
            // 
            // textBoxTempID3
            // 
            this.textBoxTempID3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID3.Enabled = false;
            this.textBoxTempID3.Location = new System.Drawing.Point(41, 96);
            this.textBoxTempID3.Name = "textBoxTempID3";
            this.textBoxTempID3.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID3.TabIndex = 2;
            this.textBoxTempID3.TextChanged += new System.EventHandler(this.TextBoxTempID3_TextChanged);
            // 
            // textBoxTempID4
            // 
            this.textBoxTempID4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID4.Enabled = false;
            this.textBoxTempID4.Location = new System.Drawing.Point(41, 123);
            this.textBoxTempID4.Name = "textBoxTempID4";
            this.textBoxTempID4.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID4.TabIndex = 2;
            this.textBoxTempID4.TextChanged += new System.EventHandler(this.TextBoxTempID4_TextChanged);
            // 
            // textBoxTempID5
            // 
            this.textBoxTempID5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID5.Enabled = false;
            this.textBoxTempID5.Location = new System.Drawing.Point(41, 150);
            this.textBoxTempID5.Name = "textBoxTempID5";
            this.textBoxTempID5.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID5.TabIndex = 2;
            this.textBoxTempID5.TextChanged += new System.EventHandler(this.TextBoxTempID5_TextChanged);
            // 
            // textBoxTempID6
            // 
            this.textBoxTempID6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID6.Enabled = false;
            this.textBoxTempID6.Location = new System.Drawing.Point(41, 177);
            this.textBoxTempID6.Name = "textBoxTempID6";
            this.textBoxTempID6.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID6.TabIndex = 2;
            this.textBoxTempID6.TextChanged += new System.EventHandler(this.TextBoxTempID6_TextChanged);
            // 
            // textBoxTempID7
            // 
            this.textBoxTempID7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID7.Enabled = false;
            this.textBoxTempID7.Location = new System.Drawing.Point(41, 204);
            this.textBoxTempID7.Name = "textBoxTempID7";
            this.textBoxTempID7.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID7.TabIndex = 2;
            this.textBoxTempID7.TextChanged += new System.EventHandler(this.TextBoxTempID7_TextChanged);
            // 
            // textBoxTempID8
            // 
            this.textBoxTempID8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTempID8.Enabled = false;
            this.textBoxTempID8.Location = new System.Drawing.Point(41, 232);
            this.textBoxTempID8.Name = "textBoxTempID8";
            this.textBoxTempID8.Size = new System.Drawing.Size(149, 20);
            this.textBoxTempID8.TabIndex = 2;
            this.textBoxTempID8.TextChanged += new System.EventHandler(this.TextBoxTempID8_TextChanged);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox6);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(860, 293);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Bluetooth";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxBluetoothMAC);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.checkBoxBluetoothAutoAnswer);
            this.groupBox6.Controls.Add(this.checkBoxBluetoothUse);
            this.groupBox6.Location = new System.Drawing.Point(7, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(846, 104);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Беспроводная гарнитура";
            // 
            // textBoxBluetoothMAC
            // 
            this.textBoxBluetoothMAC.Enabled = false;
            this.textBoxBluetoothMAC.Location = new System.Drawing.Point(151, 53);
            this.textBoxBluetoothMAC.Name = "textBoxBluetoothMAC";
            this.textBoxBluetoothMAC.Size = new System.Drawing.Size(133, 20);
            this.textBoxBluetoothMAC.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 47);
            this.label12.MaximumSize = new System.Drawing.Size(150, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 26);
            this.label12.TabIndex = 5;
            this.label12.Text = "MAC адрес гарнитуры (XX-XX-XX-XX-XX-XX):";
            // 
            // checkBoxBluetoothAutoAnswer
            // 
            this.checkBoxBluetoothAutoAnswer.AutoSize = true;
            this.checkBoxBluetoothAutoAnswer.Enabled = false;
            this.checkBoxBluetoothAutoAnswer.Location = new System.Drawing.Point(26, 81);
            this.checkBoxBluetoothAutoAnswer.Name = "checkBoxBluetoothAutoAnswer";
            this.checkBoxBluetoothAutoAnswer.Size = new System.Drawing.Size(206, 17);
            this.checkBoxBluetoothAutoAnswer.TabIndex = 0;
            this.checkBoxBluetoothAutoAnswer.Text = "Автоматически отвечать на звонок";
            this.checkBoxBluetoothAutoAnswer.UseVisualStyleBackColor = true;
            // 
            // checkBoxBluetoothUse
            // 
            this.checkBoxBluetoothUse.AutoSize = true;
            this.checkBoxBluetoothUse.Location = new System.Drawing.Point(7, 20);
            this.checkBoxBluetoothUse.Name = "checkBoxBluetoothUse";
            this.checkBoxBluetoothUse.Size = new System.Drawing.Size(152, 17);
            this.checkBoxBluetoothUse.TabIndex = 0;
            this.checkBoxBluetoothUse.Text = "Использовать гарнитуру";
            this.checkBoxBluetoothUse.UseVisualStyleBackColor = true;
            this.checkBoxBluetoothUse.CheckedChanged += new System.EventHandler(this.CheckBoxBluetoothUse_CheckedChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox16);
            this.tabPage6.Controls.Add(this.groupBox8);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(860, 293);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Безопасность";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.checkBoxAutoUpdate);
            this.groupBox16.Location = new System.Drawing.Point(243, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(234, 72);
            this.groupBox16.TabIndex = 0;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Обновление прошивки";
            // 
            // checkBoxAutoUpdate
            // 
            this.checkBoxAutoUpdate.AutoSize = true;
            this.checkBoxAutoUpdate.Location = new System.Drawing.Point(9, 19);
            this.checkBoxAutoUpdate.Name = "checkBoxAutoUpdate";
            this.checkBoxAutoUpdate.Size = new System.Drawing.Size(173, 17);
            this.checkBoxAutoUpdate.TabIndex = 1;
            this.checkBoxAutoUpdate.Text = "Автоматическое обновление";
            this.checkBoxAutoUpdate.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.textBoxPasswordSett);
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(234, 72);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Пароль для доступа к настройкам";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(185, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Максимальная длина 15 символов";
            // 
            // textBoxPasswordSett
            // 
            this.textBoxPasswordSett.Location = new System.Drawing.Point(6, 37);
            this.textBoxPasswordSett.Name = "textBoxPasswordSett";
            this.textBoxPasswordSett.Size = new System.Drawing.Size(166, 20);
            this.textBoxPasswordSett.TabIndex = 8;
            this.textBoxPasswordSett.Text = "0000";
            this.textBoxPasswordSett.UseSystemPasswordChar = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.groupBox9);
            this.tabPage9.Controls.Add(this.groupBox17);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(860, 293);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Мастер и Белые номера";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tableLayoutPanel7);
            this.groupBox9.Controls.Add(this.tableLayoutPanel4);
            this.groupBox9.Controls.Add(this.tableLayoutPanel6);
            this.groupBox9.Location = new System.Drawing.Point(234, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(620, 281);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Белые номера";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel7.Controls.Add(this.label81, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label82, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber18, 1, 6);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber18, 0, 6);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber17, 1, 5);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber17, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber16, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber16, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber15, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber15, 1, 3);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber14, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.textBoxWhiteNumber13, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber13, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxWhiteNumber14, 0, 2);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(409, 19);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 7;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(180, 218);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // label81
            // 
            this.label81.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(13, 9);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(18, 13);
            this.label81.TabIndex = 0;
            this.label81.Text = "№";
            // 
            // label82
            // 
            this.label82.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(64, 2);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(96, 26);
            this.label82.TabIndex = 0;
            this.label82.Text = "Номер телефона (7XXXXXXXXXX)";
            // 
            // textBoxWhiteNumber18
            // 
            this.textBoxWhiteNumber18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber18.Enabled = false;
            this.textBoxWhiteNumber18.Location = new System.Drawing.Point(48, 192);
            this.textBoxWhiteNumber18.Name = "textBoxWhiteNumber18";
            this.textBoxWhiteNumber18.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber18.TabIndex = 2;
            this.textBoxWhiteNumber18.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber18_TextChanged);
            // 
            // checkBoxWhiteNumber18
            // 
            this.checkBoxWhiteNumber18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber18.AutoSize = true;
            this.checkBoxWhiteNumber18.Location = new System.Drawing.Point(3, 193);
            this.checkBoxWhiteNumber18.Name = "checkBoxWhiteNumber18";
            this.checkBoxWhiteNumber18.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber18.TabIndex = 1;
            this.checkBoxWhiteNumber18.Text = "18";
            this.checkBoxWhiteNumber18.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber18.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber18_CheckedChanged);
            // 
            // textBoxWhiteNumber17
            // 
            this.textBoxWhiteNumber17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber17.Enabled = false;
            this.textBoxWhiteNumber17.Location = new System.Drawing.Point(48, 160);
            this.textBoxWhiteNumber17.Name = "textBoxWhiteNumber17";
            this.textBoxWhiteNumber17.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber17.TabIndex = 2;
            this.textBoxWhiteNumber17.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber17_TextChanged);
            // 
            // checkBoxWhiteNumber17
            // 
            this.checkBoxWhiteNumber17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber17.AutoSize = true;
            this.checkBoxWhiteNumber17.Location = new System.Drawing.Point(3, 162);
            this.checkBoxWhiteNumber17.Name = "checkBoxWhiteNumber17";
            this.checkBoxWhiteNumber17.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber17.TabIndex = 1;
            this.checkBoxWhiteNumber17.Text = "17";
            this.checkBoxWhiteNumber17.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber17.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber17_CheckedChanged);
            // 
            // textBoxWhiteNumber16
            // 
            this.textBoxWhiteNumber16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber16.Enabled = false;
            this.textBoxWhiteNumber16.Location = new System.Drawing.Point(48, 129);
            this.textBoxWhiteNumber16.Name = "textBoxWhiteNumber16";
            this.textBoxWhiteNumber16.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber16.TabIndex = 2;
            this.textBoxWhiteNumber16.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber16_TextChanged);
            // 
            // checkBoxWhiteNumber16
            // 
            this.checkBoxWhiteNumber16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber16.AutoSize = true;
            this.checkBoxWhiteNumber16.Location = new System.Drawing.Point(3, 131);
            this.checkBoxWhiteNumber16.Name = "checkBoxWhiteNumber16";
            this.checkBoxWhiteNumber16.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber16.TabIndex = 1;
            this.checkBoxWhiteNumber16.Text = "16";
            this.checkBoxWhiteNumber16.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber16.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber16_CheckedChanged);
            // 
            // checkBoxWhiteNumber15
            // 
            this.checkBoxWhiteNumber15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber15.AutoSize = true;
            this.checkBoxWhiteNumber15.Location = new System.Drawing.Point(3, 100);
            this.checkBoxWhiteNumber15.Name = "checkBoxWhiteNumber15";
            this.checkBoxWhiteNumber15.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber15.TabIndex = 1;
            this.checkBoxWhiteNumber15.Text = "15";
            this.checkBoxWhiteNumber15.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber15.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber15_CheckedChanged);
            // 
            // textBoxWhiteNumber15
            // 
            this.textBoxWhiteNumber15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber15.Enabled = false;
            this.textBoxWhiteNumber15.Location = new System.Drawing.Point(48, 98);
            this.textBoxWhiteNumber15.Name = "textBoxWhiteNumber15";
            this.textBoxWhiteNumber15.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber15.TabIndex = 2;
            this.textBoxWhiteNumber15.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber15_TextChanged);
            // 
            // textBoxWhiteNumber14
            // 
            this.textBoxWhiteNumber14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber14.Enabled = false;
            this.textBoxWhiteNumber14.Location = new System.Drawing.Point(48, 67);
            this.textBoxWhiteNumber14.Name = "textBoxWhiteNumber14";
            this.textBoxWhiteNumber14.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber14.TabIndex = 2;
            this.textBoxWhiteNumber14.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber14_TextChanged);
            // 
            // textBoxWhiteNumber13
            // 
            this.textBoxWhiteNumber13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber13.Enabled = false;
            this.textBoxWhiteNumber13.Location = new System.Drawing.Point(48, 36);
            this.textBoxWhiteNumber13.Name = "textBoxWhiteNumber13";
            this.textBoxWhiteNumber13.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber13.TabIndex = 2;
            this.textBoxWhiteNumber13.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber13_TextChanged);
            // 
            // checkBoxWhiteNumber13
            // 
            this.checkBoxWhiteNumber13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber13.AutoSize = true;
            this.checkBoxWhiteNumber13.Location = new System.Drawing.Point(3, 38);
            this.checkBoxWhiteNumber13.Name = "checkBoxWhiteNumber13";
            this.checkBoxWhiteNumber13.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber13.TabIndex = 1;
            this.checkBoxWhiteNumber13.Text = "13";
            this.checkBoxWhiteNumber13.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber13.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber13_CheckedChanged);
            // 
            // checkBoxWhiteNumber14
            // 
            this.checkBoxWhiteNumber14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber14.AutoSize = true;
            this.checkBoxWhiteNumber14.Location = new System.Drawing.Point(3, 69);
            this.checkBoxWhiteNumber14.Name = "checkBoxWhiteNumber14";
            this.checkBoxWhiteNumber14.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber14.TabIndex = 1;
            this.checkBoxWhiteNumber14.Text = "14";
            this.checkBoxWhiteNumber14.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber14.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber14_CheckedChanged);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel4.Controls.Add(this.label79, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label80, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber12, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber11, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber10, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber9, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber8, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.textBoxWhiteNumber7, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber12, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber11, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber10, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber9, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.checkBoxWhiteNumber8, 0, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(211, 19);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 7;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(180, 218);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label79
            // 
            this.label79.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(13, 9);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(18, 13);
            this.label79.TabIndex = 0;
            this.label79.Text = "№";
            // 
            // label80
            // 
            this.label80.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(64, 2);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(96, 26);
            this.label80.TabIndex = 0;
            this.label80.Text = "Номер телефона (7XXXXXXXXXX)";
            // 
            // textBoxWhiteNumber12
            // 
            this.textBoxWhiteNumber12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber12.Enabled = false;
            this.textBoxWhiteNumber12.Location = new System.Drawing.Point(48, 192);
            this.textBoxWhiteNumber12.Name = "textBoxWhiteNumber12";
            this.textBoxWhiteNumber12.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber12.TabIndex = 2;
            this.textBoxWhiteNumber12.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber12_TextChanged);
            // 
            // textBoxWhiteNumber11
            // 
            this.textBoxWhiteNumber11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber11.Enabled = false;
            this.textBoxWhiteNumber11.Location = new System.Drawing.Point(48, 160);
            this.textBoxWhiteNumber11.Name = "textBoxWhiteNumber11";
            this.textBoxWhiteNumber11.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber11.TabIndex = 2;
            this.textBoxWhiteNumber11.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber11_TextChanged);
            // 
            // textBoxWhiteNumber10
            // 
            this.textBoxWhiteNumber10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber10.Enabled = false;
            this.textBoxWhiteNumber10.Location = new System.Drawing.Point(48, 129);
            this.textBoxWhiteNumber10.Name = "textBoxWhiteNumber10";
            this.textBoxWhiteNumber10.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber10.TabIndex = 2;
            this.textBoxWhiteNumber10.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber10_TextChanged);
            // 
            // textBoxWhiteNumber9
            // 
            this.textBoxWhiteNumber9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber9.Enabled = false;
            this.textBoxWhiteNumber9.Location = new System.Drawing.Point(48, 98);
            this.textBoxWhiteNumber9.Name = "textBoxWhiteNumber9";
            this.textBoxWhiteNumber9.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber9.TabIndex = 2;
            this.textBoxWhiteNumber9.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber9_TextChanged);
            // 
            // textBoxWhiteNumber8
            // 
            this.textBoxWhiteNumber8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber8.Enabled = false;
            this.textBoxWhiteNumber8.Location = new System.Drawing.Point(48, 67);
            this.textBoxWhiteNumber8.Name = "textBoxWhiteNumber8";
            this.textBoxWhiteNumber8.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber8.TabIndex = 2;
            this.textBoxWhiteNumber8.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber8_TextChanged);
            // 
            // textBoxWhiteNumber7
            // 
            this.textBoxWhiteNumber7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber7.Enabled = false;
            this.textBoxWhiteNumber7.Location = new System.Drawing.Point(48, 36);
            this.textBoxWhiteNumber7.Name = "textBoxWhiteNumber7";
            this.textBoxWhiteNumber7.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber7.TabIndex = 2;
            this.textBoxWhiteNumber7.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber7_TextChanged);
            // 
            // checkBoxWhiteNumber12
            // 
            this.checkBoxWhiteNumber12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber12.AutoSize = true;
            this.checkBoxWhiteNumber12.Location = new System.Drawing.Point(3, 193);
            this.checkBoxWhiteNumber12.Name = "checkBoxWhiteNumber12";
            this.checkBoxWhiteNumber12.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber12.TabIndex = 1;
            this.checkBoxWhiteNumber12.Text = "12";
            this.checkBoxWhiteNumber12.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber12.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber12_CheckedChanged);
            // 
            // checkBoxWhiteNumber11
            // 
            this.checkBoxWhiteNumber11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber11.AutoSize = true;
            this.checkBoxWhiteNumber11.Location = new System.Drawing.Point(3, 162);
            this.checkBoxWhiteNumber11.Name = "checkBoxWhiteNumber11";
            this.checkBoxWhiteNumber11.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber11.TabIndex = 1;
            this.checkBoxWhiteNumber11.Text = "11";
            this.checkBoxWhiteNumber11.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber11.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber11_CheckedChanged);
            // 
            // checkBoxWhiteNumber10
            // 
            this.checkBoxWhiteNumber10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber10.AutoSize = true;
            this.checkBoxWhiteNumber10.Location = new System.Drawing.Point(3, 131);
            this.checkBoxWhiteNumber10.Name = "checkBoxWhiteNumber10";
            this.checkBoxWhiteNumber10.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber10.TabIndex = 1;
            this.checkBoxWhiteNumber10.Text = "10";
            this.checkBoxWhiteNumber10.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber10.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber10_CheckedChanged);
            // 
            // checkBoxWhiteNumber9
            // 
            this.checkBoxWhiteNumber9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber9.AutoSize = true;
            this.checkBoxWhiteNumber9.Location = new System.Drawing.Point(3, 100);
            this.checkBoxWhiteNumber9.Name = "checkBoxWhiteNumber9";
            this.checkBoxWhiteNumber9.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber9.TabIndex = 1;
            this.checkBoxWhiteNumber9.Text = "9";
            this.checkBoxWhiteNumber9.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber9.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber9_CheckedChanged);
            // 
            // checkBoxWhiteNumber7
            // 
            this.checkBoxWhiteNumber7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber7.AutoSize = true;
            this.checkBoxWhiteNumber7.Location = new System.Drawing.Point(3, 38);
            this.checkBoxWhiteNumber7.Name = "checkBoxWhiteNumber7";
            this.checkBoxWhiteNumber7.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber7.TabIndex = 1;
            this.checkBoxWhiteNumber7.Text = "7";
            this.checkBoxWhiteNumber7.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber7.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber7_CheckedChanged);
            // 
            // checkBoxWhiteNumber8
            // 
            this.checkBoxWhiteNumber8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber8.AutoSize = true;
            this.checkBoxWhiteNumber8.Location = new System.Drawing.Point(3, 69);
            this.checkBoxWhiteNumber8.Name = "checkBoxWhiteNumber8";
            this.checkBoxWhiteNumber8.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber8.TabIndex = 1;
            this.checkBoxWhiteNumber8.Text = "8";
            this.checkBoxWhiteNumber8.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber8.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber8_CheckedChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel6.Controls.Add(this.label57, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label58, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber4, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber2, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber3, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber1, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber2, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber3, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber4, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber5, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxWhiteNumber6, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber5, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.textBoxWhiteNumber6, 1, 6);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(13, 19);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 7;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(180, 218);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label57
            // 
            this.label57.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(13, 9);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(18, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "№";
            // 
            // label58
            // 
            this.label58.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(64, 2);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(96, 26);
            this.label58.TabIndex = 0;
            this.label58.Text = "Номер телефона (7XXXXXXXXXX)";
            // 
            // checkBoxWhiteNumber1
            // 
            this.checkBoxWhiteNumber1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber1.AutoSize = true;
            this.checkBoxWhiteNumber1.Location = new System.Drawing.Point(3, 38);
            this.checkBoxWhiteNumber1.Name = "checkBoxWhiteNumber1";
            this.checkBoxWhiteNumber1.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber1.TabIndex = 1;
            this.checkBoxWhiteNumber1.Text = "1";
            this.checkBoxWhiteNumber1.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber1.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber1_CheckedChanged);
            // 
            // checkBoxWhiteNumber4
            // 
            this.checkBoxWhiteNumber4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber4.AutoSize = true;
            this.checkBoxWhiteNumber4.Location = new System.Drawing.Point(3, 131);
            this.checkBoxWhiteNumber4.Name = "checkBoxWhiteNumber4";
            this.checkBoxWhiteNumber4.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber4.TabIndex = 1;
            this.checkBoxWhiteNumber4.Text = "4";
            this.checkBoxWhiteNumber4.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber4.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber4_CheckedChanged);
            // 
            // checkBoxWhiteNumber2
            // 
            this.checkBoxWhiteNumber2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber2.AutoSize = true;
            this.checkBoxWhiteNumber2.Location = new System.Drawing.Point(3, 69);
            this.checkBoxWhiteNumber2.Name = "checkBoxWhiteNumber2";
            this.checkBoxWhiteNumber2.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber2.TabIndex = 1;
            this.checkBoxWhiteNumber2.Text = "2";
            this.checkBoxWhiteNumber2.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber2.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber2_CheckedChanged);
            // 
            // checkBoxWhiteNumber3
            // 
            this.checkBoxWhiteNumber3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber3.AutoSize = true;
            this.checkBoxWhiteNumber3.Location = new System.Drawing.Point(3, 100);
            this.checkBoxWhiteNumber3.Name = "checkBoxWhiteNumber3";
            this.checkBoxWhiteNumber3.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber3.TabIndex = 1;
            this.checkBoxWhiteNumber3.Text = "3";
            this.checkBoxWhiteNumber3.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber3.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber3_CheckedChanged);
            // 
            // textBoxWhiteNumber1
            // 
            this.textBoxWhiteNumber1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber1.Enabled = false;
            this.textBoxWhiteNumber1.Location = new System.Drawing.Point(48, 36);
            this.textBoxWhiteNumber1.Name = "textBoxWhiteNumber1";
            this.textBoxWhiteNumber1.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber1.TabIndex = 2;
            this.textBoxWhiteNumber1.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber1_TextChanged);
            // 
            // textBoxWhiteNumber2
            // 
            this.textBoxWhiteNumber2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber2.Enabled = false;
            this.textBoxWhiteNumber2.Location = new System.Drawing.Point(48, 67);
            this.textBoxWhiteNumber2.Name = "textBoxWhiteNumber2";
            this.textBoxWhiteNumber2.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber2.TabIndex = 2;
            this.textBoxWhiteNumber2.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber2_TextChanged);
            // 
            // textBoxWhiteNumber3
            // 
            this.textBoxWhiteNumber3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber3.Enabled = false;
            this.textBoxWhiteNumber3.Location = new System.Drawing.Point(48, 98);
            this.textBoxWhiteNumber3.Name = "textBoxWhiteNumber3";
            this.textBoxWhiteNumber3.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber3.TabIndex = 2;
            this.textBoxWhiteNumber3.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber3_TextChanged);
            // 
            // textBoxWhiteNumber4
            // 
            this.textBoxWhiteNumber4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber4.Enabled = false;
            this.textBoxWhiteNumber4.Location = new System.Drawing.Point(48, 129);
            this.textBoxWhiteNumber4.Name = "textBoxWhiteNumber4";
            this.textBoxWhiteNumber4.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber4.TabIndex = 2;
            this.textBoxWhiteNumber4.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber4_TextChanged);
            // 
            // checkBoxWhiteNumber5
            // 
            this.checkBoxWhiteNumber5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber5.AutoSize = true;
            this.checkBoxWhiteNumber5.Location = new System.Drawing.Point(3, 162);
            this.checkBoxWhiteNumber5.Name = "checkBoxWhiteNumber5";
            this.checkBoxWhiteNumber5.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber5.TabIndex = 1;
            this.checkBoxWhiteNumber5.Text = "5";
            this.checkBoxWhiteNumber5.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber5.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber5_CheckedChanged);
            // 
            // checkBoxWhiteNumber6
            // 
            this.checkBoxWhiteNumber6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxWhiteNumber6.AutoSize = true;
            this.checkBoxWhiteNumber6.Location = new System.Drawing.Point(3, 193);
            this.checkBoxWhiteNumber6.Name = "checkBoxWhiteNumber6";
            this.checkBoxWhiteNumber6.Size = new System.Drawing.Size(39, 17);
            this.checkBoxWhiteNumber6.TabIndex = 1;
            this.checkBoxWhiteNumber6.Text = "6";
            this.checkBoxWhiteNumber6.UseVisualStyleBackColor = true;
            this.checkBoxWhiteNumber6.CheckedChanged += new System.EventHandler(this.CheckBoxWhiteNumber6_CheckedChanged);
            // 
            // textBoxWhiteNumber5
            // 
            this.textBoxWhiteNumber5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber5.Enabled = false;
            this.textBoxWhiteNumber5.Location = new System.Drawing.Point(48, 160);
            this.textBoxWhiteNumber5.Name = "textBoxWhiteNumber5";
            this.textBoxWhiteNumber5.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber5.TabIndex = 2;
            this.textBoxWhiteNumber5.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber5_TextChanged);
            // 
            // textBoxWhiteNumber6
            // 
            this.textBoxWhiteNumber6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxWhiteNumber6.Enabled = false;
            this.textBoxWhiteNumber6.Location = new System.Drawing.Point(48, 192);
            this.textBoxWhiteNumber6.Name = "textBoxWhiteNumber6";
            this.textBoxWhiteNumber6.Size = new System.Drawing.Size(129, 20);
            this.textBoxWhiteNumber6.TabIndex = 2;
            this.textBoxWhiteNumber6.TextChanged += new System.EventHandler(this.TextBoxWhiteNumber6_TextChanged);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tableLayoutPanel10);
            this.groupBox17.Location = new System.Drawing.Point(6, 6);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(222, 281);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Мастер номера";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel10.Controls.Add(this.label87, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label88, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.checkBoxMasterNumber1, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.checkBoxMasterNumber4, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.checkBoxMasterNumber2, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.checkBoxMasterNumber3, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.textBoxMasterNumber1, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.textBoxMasterNumber2, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.textBoxMasterNumber3, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.textBoxMasterNumber4, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.checkBoxMasterNumber5, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.textBoxMasterNumber5, 1, 5);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(13, 19);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 6;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(180, 180);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // label87
            // 
            this.label87.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(13, 8);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(18, 13);
            this.label87.TabIndex = 0;
            this.label87.Text = "№";
            // 
            // label88
            // 
            this.label88.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(64, 1);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(96, 26);
            this.label88.TabIndex = 0;
            this.label88.Text = "Номер телефона (7XXXXXXXXXX)";
            // 
            // checkBoxMasterNumber1
            // 
            this.checkBoxMasterNumber1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMasterNumber1.AutoSize = true;
            this.checkBoxMasterNumber1.Location = new System.Drawing.Point(3, 35);
            this.checkBoxMasterNumber1.Name = "checkBoxMasterNumber1";
            this.checkBoxMasterNumber1.Size = new System.Drawing.Size(39, 17);
            this.checkBoxMasterNumber1.TabIndex = 1;
            this.checkBoxMasterNumber1.Text = "1";
            this.checkBoxMasterNumber1.UseVisualStyleBackColor = true;
            this.checkBoxMasterNumber1.CheckedChanged += new System.EventHandler(this.CheckBoxMasterNumber1_CheckedChanged);
            // 
            // checkBoxMasterNumber4
            // 
            this.checkBoxMasterNumber4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMasterNumber4.AutoSize = true;
            this.checkBoxMasterNumber4.Location = new System.Drawing.Point(3, 122);
            this.checkBoxMasterNumber4.Name = "checkBoxMasterNumber4";
            this.checkBoxMasterNumber4.Size = new System.Drawing.Size(39, 17);
            this.checkBoxMasterNumber4.TabIndex = 1;
            this.checkBoxMasterNumber4.Text = "4";
            this.checkBoxMasterNumber4.UseVisualStyleBackColor = true;
            this.checkBoxMasterNumber4.CheckedChanged += new System.EventHandler(this.CheckBoxMasterNumber4_CheckedChanged);
            // 
            // checkBoxMasterNumber2
            // 
            this.checkBoxMasterNumber2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMasterNumber2.AutoSize = true;
            this.checkBoxMasterNumber2.Location = new System.Drawing.Point(3, 64);
            this.checkBoxMasterNumber2.Name = "checkBoxMasterNumber2";
            this.checkBoxMasterNumber2.Size = new System.Drawing.Size(39, 17);
            this.checkBoxMasterNumber2.TabIndex = 1;
            this.checkBoxMasterNumber2.Text = "2";
            this.checkBoxMasterNumber2.UseVisualStyleBackColor = true;
            this.checkBoxMasterNumber2.CheckedChanged += new System.EventHandler(this.CheckBoxMasterNumber2_CheckedChanged);
            // 
            // checkBoxMasterNumber3
            // 
            this.checkBoxMasterNumber3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMasterNumber3.AutoSize = true;
            this.checkBoxMasterNumber3.Location = new System.Drawing.Point(3, 93);
            this.checkBoxMasterNumber3.Name = "checkBoxMasterNumber3";
            this.checkBoxMasterNumber3.Size = new System.Drawing.Size(39, 17);
            this.checkBoxMasterNumber3.TabIndex = 1;
            this.checkBoxMasterNumber3.Text = "3";
            this.checkBoxMasterNumber3.UseVisualStyleBackColor = true;
            this.checkBoxMasterNumber3.CheckedChanged += new System.EventHandler(this.CheckBoxMasterNumber3_CheckedChanged);
            // 
            // textBoxMasterNumber1
            // 
            this.textBoxMasterNumber1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMasterNumber1.Enabled = false;
            this.textBoxMasterNumber1.Location = new System.Drawing.Point(48, 33);
            this.textBoxMasterNumber1.Name = "textBoxMasterNumber1";
            this.textBoxMasterNumber1.Size = new System.Drawing.Size(129, 20);
            this.textBoxMasterNumber1.TabIndex = 2;
            this.textBoxMasterNumber1.TextChanged += new System.EventHandler(this.TextBoxMasterNumber1_TextChanged);
            // 
            // textBoxMasterNumber2
            // 
            this.textBoxMasterNumber2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMasterNumber2.Enabled = false;
            this.textBoxMasterNumber2.Location = new System.Drawing.Point(48, 62);
            this.textBoxMasterNumber2.Name = "textBoxMasterNumber2";
            this.textBoxMasterNumber2.Size = new System.Drawing.Size(129, 20);
            this.textBoxMasterNumber2.TabIndex = 2;
            this.textBoxMasterNumber2.TextChanged += new System.EventHandler(this.TextBoxMasterNumber2_TextChanged);
            // 
            // textBoxMasterNumber3
            // 
            this.textBoxMasterNumber3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMasterNumber3.Enabled = false;
            this.textBoxMasterNumber3.Location = new System.Drawing.Point(48, 91);
            this.textBoxMasterNumber3.Name = "textBoxMasterNumber3";
            this.textBoxMasterNumber3.Size = new System.Drawing.Size(129, 20);
            this.textBoxMasterNumber3.TabIndex = 2;
            this.textBoxMasterNumber3.TextChanged += new System.EventHandler(this.TextBoxMasterNumber3_TextChanged);
            // 
            // textBoxMasterNumber4
            // 
            this.textBoxMasterNumber4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMasterNumber4.Enabled = false;
            this.textBoxMasterNumber4.Location = new System.Drawing.Point(48, 120);
            this.textBoxMasterNumber4.Name = "textBoxMasterNumber4";
            this.textBoxMasterNumber4.Size = new System.Drawing.Size(129, 20);
            this.textBoxMasterNumber4.TabIndex = 2;
            this.textBoxMasterNumber4.TextChanged += new System.EventHandler(this.TextBoxMasterNumber4_TextChanged);
            // 
            // checkBoxMasterNumber5
            // 
            this.checkBoxMasterNumber5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxMasterNumber5.AutoSize = true;
            this.checkBoxMasterNumber5.Location = new System.Drawing.Point(3, 154);
            this.checkBoxMasterNumber5.Name = "checkBoxMasterNumber5";
            this.checkBoxMasterNumber5.Size = new System.Drawing.Size(39, 17);
            this.checkBoxMasterNumber5.TabIndex = 1;
            this.checkBoxMasterNumber5.Text = "5";
            this.checkBoxMasterNumber5.UseVisualStyleBackColor = true;
            this.checkBoxMasterNumber5.CheckedChanged += new System.EventHandler(this.CheckBoxMasterNumber5_CheckedChanged);
            // 
            // textBoxMasterNumber5
            // 
            this.textBoxMasterNumber5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMasterNumber5.Enabled = false;
            this.textBoxMasterNumber5.Location = new System.Drawing.Point(48, 152);
            this.textBoxMasterNumber5.Name = "textBoxMasterNumber5";
            this.textBoxMasterNumber5.Size = new System.Drawing.Size(129, 20);
            this.textBoxMasterNumber5.TabIndex = 2;
            this.textBoxMasterNumber5.TextChanged += new System.EventHandler(this.TextBoxMasterNumber5_TextChanged);
            // 
            // buttonComPortConnect
            // 
            this.buttonComPortConnect.Enabled = false;
            this.buttonComPortConnect.Location = new System.Drawing.Point(97, 40);
            this.buttonComPortConnect.Name = "buttonComPortConnect";
            this.buttonComPortConnect.Size = new System.Drawing.Size(90, 23);
            this.buttonComPortConnect.TabIndex = 4;
            this.buttonComPortConnect.Text = "Подключить";
            this.buttonComPortConnect.UseVisualStyleBackColor = true;
            this.buttonComPortConnect.Click += new System.EventHandler(this.ButtonComPortConnect_Click);
            // 
            // pictureBoxConnected
            // 
            this.pictureBoxConnected.BackColor = System.Drawing.Color.Silver;
            this.pictureBoxConnected.Location = new System.Drawing.Point(0, -3);
            this.pictureBoxConnected.Name = "pictureBoxConnected";
            this.pictureBoxConnected.Size = new System.Drawing.Size(887, 10);
            this.pictureBoxConnected.TabIndex = 5;
            this.pictureBoxConnected.TabStop = false;
            // 
            // buttonTest
            // 
            this.buttonTest.Enabled = false;
            this.buttonTest.Location = new System.Drawing.Point(680, 14);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(91, 23);
            this.buttonTest.TabIndex = 6;
            this.buttonTest.Text = "Тест";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.ButtonTest_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(338, 14);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(163, 20);
            this.textBoxPassword.TabIndex = 4;
            this.textBoxPassword.Text = "0000";
            this.textBoxPassword.UseSystemPasswordChar = true;
            this.textBoxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxPassword_KeyDown);
            // 
            // buttonAbout
            // 
            this.buttonAbout.Location = new System.Drawing.Point(801, 14);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(75, 23);
            this.buttonAbout.TabIndex = 7;
            this.buttonAbout.Text = "Справка";
            this.buttonAbout.UseVisualStyleBackColor = true;
            this.buttonAbout.Click += new System.EventHandler(this.ButtonAbout_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buttonReadFromDevice
            // 
            this.buttonReadFromDevice.Enabled = false;
            this.buttonReadFromDevice.Location = new System.Drawing.Point(466, 397);
            this.buttonReadFromDevice.Name = "buttonReadFromDevice";
            this.buttonReadFromDevice.Size = new System.Drawing.Size(152, 27);
            this.buttonReadFromDevice.TabIndex = 8;
            this.buttonReadFromDevice.Text = "Прочитать из устройства";
            this.buttonReadFromDevice.UseVisualStyleBackColor = true;
            this.buttonReadFromDevice.Click += new System.EventHandler(this.ButtonReadFromDevice_Click);
            // 
            // buttonWriteToDevice
            // 
            this.buttonWriteToDevice.Enabled = false;
            this.buttonWriteToDevice.Location = new System.Drawing.Point(466, 432);
            this.buttonWriteToDevice.Name = "buttonWriteToDevice";
            this.buttonWriteToDevice.Size = new System.Drawing.Size(152, 27);
            this.buttonWriteToDevice.TabIndex = 8;
            this.buttonWriteToDevice.Text = "Записать в устройство";
            this.buttonWriteToDevice.UseVisualStyleBackColor = true;
            this.buttonWriteToDevice.Click += new System.EventHandler(this.ButtonWriteToDevice_Click);
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Location = new System.Drawing.Point(219, 432);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(152, 27);
            this.buttonSaveToFile.TabIndex = 8;
            this.buttonSaveToFile.Text = "Сохранить в файл";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.ButtonSaveToFile_Click);
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.Location = new System.Drawing.Point(219, 397);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(152, 27);
            this.buttonOpenFile.TabIndex = 8;
            this.buttonOpenFile.Text = "Открыть из файла";
            this.buttonOpenFile.UseVisualStyleBackColor = true;
            this.buttonOpenFile.Click += new System.EventHandler(this.ButtonOpenFile_Click);
            // 
            // timerSerialPort
            // 
            this.timerSerialPort.Enabled = true;
            this.timerSerialPort.Interval = 1000;
            this.timerSerialPort.Tick += new System.EventHandler(this.timerSerialPortCallback);
            // 
            // buttonComPortRefresh
            // 
            this.buttonComPortRefresh.Location = new System.Drawing.Point(8, 40);
            this.buttonComPortRefresh.Name = "buttonComPortRefresh";
            this.buttonComPortRefresh.Size = new System.Drawing.Size(83, 23);
            this.buttonComPortRefresh.TabIndex = 9;
            this.buttonComPortRefresh.Text = "Обновить";
            this.buttonComPortRefresh.UseVisualStyleBackColor = true;
            this.buttonComPortRefresh.Click += new System.EventHandler(this.ButtonComPortRefresh_Click);
            // 
            // checkBoxHidePassword
            // 
            this.checkBoxHidePassword.AutoSize = true;
            this.checkBoxHidePassword.Checked = true;
            this.checkBoxHidePassword.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHidePassword.Location = new System.Drawing.Point(338, 40);
            this.checkBoxHidePassword.Name = "checkBoxHidePassword";
            this.checkBoxHidePassword.Size = new System.Drawing.Size(103, 17);
            this.checkBoxHidePassword.TabIndex = 10;
            this.checkBoxHidePassword.Text = "Скрыть пароль";
            this.checkBoxHidePassword.UseVisualStyleBackColor = true;
            this.checkBoxHidePassword.CheckedChanged += new System.EventHandler(this.checkBoxHidePassword_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(283, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Пароль:";
            // 
            // Configurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 464);
            this.Controls.Add(this.pictureBoxConnected);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.checkBoxHidePassword);
            this.Controls.Add(this.buttonComPortRefresh);
            this.Controls.Add(this.buttonOpenFile);
            this.Controls.Add(this.buttonSaveToFile);
            this.Controls.Add(this.buttonWriteToDevice);
            this.Controls.Add(this.buttonReadFromDevice);
            this.Controls.Add(this.buttonAbout);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.buttonComPortConnect);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.comboBoxPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Configurator";
            this.Text = "LEANConfigurator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnApplicationExit);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBoxServerLog.ResumeLayout(false);
            this.groupBoxServerLog.PerformLayout();
            this.groupBoxServerTelematic.ResumeLayout(false);
            this.groupBoxServerTelematic.PerformLayout();
            this.groupBoxNetwork.ResumeLayout(false);
            this.groupBoxNetwork.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxTriggers.ResumeLayout(false);
            this.groupBoxTriggers.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConnected)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxPort;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonComPortConnect;
        private System.Windows.Forms.PictureBox pictureBoxConnected;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.GroupBox groupBoxNetwork;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAPNLogin;
        private System.Windows.Forms.TextBox textBoxAPNAddress;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBoxServerLog;
        private System.Windows.Forms.CheckBox checkBoxLogingEnable;
        private System.Windows.Forms.TextBox textBoxLogIP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBoxServerTelematic;
        private System.Windows.Forms.CheckBox checkBoxWaitResponse;
        private System.Windows.Forms.TextBox textBoxServerIP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxProtocolType;
        private System.Windows.Forms.RadioButton radioButtonAPNUser;
        private System.Windows.Forms.RadioButton radioButtonAPNAuto;
        private System.Windows.Forms.TextBox textBoxAPNPassword;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.GroupBox groupBoxTriggers;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxTriggerPointStandby;
        private System.Windows.Forms.TextBox textBoxTriggerPointActive;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxTriggerPointMeter;
        private System.Windows.Forms.TextBox textBoxTriggerPointDegree;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxFilterEnable;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBoxFilterFreezeTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxFilterSpeedMin;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxFilterHDOP;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxFilterSpeedAvg;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxFilterSpeedMax;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBoxLLS1;
        private System.Windows.Forms.TextBox textBoxLLS1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxTriggerLLSStandby;
        private System.Windows.Forms.TextBox textBoxTriggerLLSActive;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBoxLLS2;
        private System.Windows.Forms.TextBox textBoxLLS3;
        private System.Windows.Forms.TextBox textBoxLLS4;
        private System.Windows.Forms.TextBox textBoxLLS5;
        private System.Windows.Forms.TextBox textBoxLLS6;
        private System.Windows.Forms.TextBox textBoxLLS7;
        private System.Windows.Forms.TextBox textBoxLLS8;
        private System.Windows.Forms.CheckBox checkBoxLLS2;
        private System.Windows.Forms.CheckBox checkBoxLLS3;
        private System.Windows.Forms.CheckBox checkBoxLLS4;
        private System.Windows.Forms.CheckBox checkBoxLLS5;
        private System.Windows.Forms.CheckBox checkBoxLLS6;
        private System.Windows.Forms.CheckBox checkBoxLLS7;
        private System.Windows.Forms.CheckBox checkBoxLLS8;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox comboBoxDI1Mode;
        private System.Windows.Forms.ComboBox comboBoxDI2Mode;
        private System.Windows.Forms.ComboBox comboBoxDI3Mode;
        private System.Windows.Forms.ComboBox comboBoxDI4Mode;
        private System.Windows.Forms.ComboBox comboBoxDI1Type;
        private System.Windows.Forms.ComboBox comboBoxDI2Type;
        private System.Windows.Forms.ComboBox comboBoxDI3Type;
        private System.Windows.Forms.ComboBox comboBoxDI4Type;
        private System.Windows.Forms.TextBox textBoxAI1Trh;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBoxAI2Trh;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox comboBoxDO1Type;
        private System.Windows.Forms.ComboBox comboBoxDO2Type;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.CheckBox checkBoxTemp1Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp4Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp5Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp6Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp7Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp8Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp2Enable;
        private System.Windows.Forms.CheckBox checkBoxTemp3Enable;
        private System.Windows.Forms.TextBox textBoxTempID1;
        private System.Windows.Forms.TextBox textBoxTempID2;
        private System.Windows.Forms.TextBox textBoxTempID3;
        private System.Windows.Forms.TextBox textBoxTempID4;
        private System.Windows.Forms.TextBox textBoxTempID5;
        private System.Windows.Forms.TextBox textBoxTempID6;
        private System.Windows.Forms.TextBox textBoxTempID7;
        private System.Windows.Forms.TextBox textBoxTempID8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBoxPasswordSett;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox textBoxFilterSatellitesMin;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonReadFromDevice;
        private System.Windows.Forms.Button buttonWriteToDevice;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Button buttonOpenFile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBoxIDAuto;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBoxRssiMin;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBoxAccelThreshold;
        private System.Windows.Forms.TextBox textBoxServerPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxLogPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timerSerialPort;
        private System.Windows.Forms.Button buttonComPortRefresh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxBluetoothMAC;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxBluetoothAutoAnswer;
        private System.Windows.Forms.CheckBox checkBoxBluetoothUse;
        private System.Windows.Forms.CheckBox checkBoxHidePassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox comboBoxLogLevel;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBoxTriggerCanStandby;
        private System.Windows.Forms.TextBox textBoxTriggerCanActive;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox checkBoxCanUse;
        private System.Windows.Forms.CheckBox checkBoxCanCheckOilUse;
        private System.Windows.Forms.CheckBox checkBoxCanRpmUse;
        private System.Windows.Forms.CheckBox checkBoxCanEngineTempUse;
        private System.Windows.Forms.CheckBox checkBoxCanFuelLevelLiterUse;
        private System.Windows.Forms.CheckBox checkBoxCanIgnitionUse;
        private System.Windows.Forms.CheckBox checkBoxCanFuelLevelPercentUse;
        private System.Windows.Forms.CheckBox checkBoxCanOdometerUse;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBoxCanAlertTempDuration;
        private System.Windows.Forms.TextBox textBoxCanAlertRpmDuration;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox textBoxCanAlertTempMax;
        private System.Windows.Forms.TextBox textBoxCanAlertRpmMax;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.CheckBox checkBoxCanAlertCheckOil;
        private System.Windows.Forms.CheckBox checkBoxCanIgnitionGenPacketOnChange;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.CheckBox checkBoxAutoUpdate;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber15;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber16;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber17;
        private System.Windows.Forms.TextBox textBoxWhiteNumber15;
        private System.Windows.Forms.TextBox textBoxWhiteNumber16;
        private System.Windows.Forms.TextBox textBoxWhiteNumber17;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber18;
        private System.Windows.Forms.TextBox textBoxWhiteNumber18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber8;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber11;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber9;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber10;
        private System.Windows.Forms.TextBox textBoxWhiteNumber8;
        private System.Windows.Forms.TextBox textBoxWhiteNumber9;
        private System.Windows.Forms.TextBox textBoxWhiteNumber10;
        private System.Windows.Forms.TextBox textBoxWhiteNumber11;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBoxWhiteNumber12;
        private System.Windows.Forms.TextBox textBoxWhiteNumber13;
        private System.Windows.Forms.TextBox textBoxWhiteNumber14;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber12;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber13;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber1;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber4;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber2;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber3;
        private System.Windows.Forms.TextBox textBoxWhiteNumber1;
        private System.Windows.Forms.TextBox textBoxWhiteNumber2;
        private System.Windows.Forms.TextBox textBoxWhiteNumber3;
        private System.Windows.Forms.TextBox textBoxWhiteNumber4;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber5;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber6;
        private System.Windows.Forms.CheckBox checkBoxWhiteNumber7;
        private System.Windows.Forms.TextBox textBoxWhiteNumber5;
        private System.Windows.Forms.TextBox textBoxWhiteNumber6;
        private System.Windows.Forms.TextBox textBoxWhiteNumber7;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox checkBoxMasterNumber1;
        private System.Windows.Forms.CheckBox checkBoxMasterNumber4;
        private System.Windows.Forms.CheckBox checkBoxMasterNumber2;
        private System.Windows.Forms.CheckBox checkBoxMasterNumber3;
        private System.Windows.Forms.TextBox textBoxMasterNumber1;
        private System.Windows.Forms.TextBox textBoxMasterNumber2;
        private System.Windows.Forms.TextBox textBoxMasterNumber3;
        private System.Windows.Forms.TextBox textBoxMasterNumber4;
        private System.Windows.Forms.CheckBox checkBoxMasterNumber5;
        private System.Windows.Forms.TextBox textBoxMasterNumber5;
    }
}

